//
//  ItemTableCell.h
//  R1
//
//  Created by stephen feggeler on 12/26/12.
//  Copyright 2012 self. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ItemCell : UITableViewCell {
	UILabel *labelKey;	 // label on the left side of the cell
	UILabel *labelValue; // label on the right side of the cell
}

@property (nonatomic, retain) UILabel *labelKey;
@property (nonatomic, retain) UILabel *labelValue;

@end
