//
//  SliderRequirementsCell.m
//  R1
//
//  Created by stephen feggeler on 2/8/13.
//  Copyright 2013 self. All rights reserved.
//

#import "SliderRequirementsCell.h"


@implementation SliderRequirementsCell

@synthesize priceSlider;
@synthesize priceName;
@synthesize priceValue;

@synthesize bedsSlider;
@synthesize bedsName;
@synthesize bedsValue;

@synthesize bathsSlider;
@synthesize bathsName;
@synthesize bathsValue;

@synthesize structureSizeSlider;
@synthesize structureSizeName;
@synthesize structureSizeValue;

@synthesize domSlider;
@synthesize domName;
@synthesize domValue;

@synthesize pageSizeSlider;
@synthesize pageName;
@synthesize pageValue;

static NSInteger xposSlider = 14;
static NSInteger yposSlider = 30;
static NSInteger widthSlider = 270;
static NSInteger heightSlider = 25;

static NSInteger rowHeight = 50;

static NSInteger xposName = 15;
static NSInteger yposLabel = 5;
static NSInteger widthName = 100;
static NSInteger heightLabel = 25;

static NSInteger xposValue = 135;
static NSInteger widthValue = 150;

#define cellHeight 44*6

+ (CGFloat)height{
	return cellHeight;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    widthSlider = [[UIScreen mainScreen] bounds].size.width - 30;
    xposValue = [[UIScreen mainScreen] bounds].size.width - 165;
	
    if (self) {
        // Initialization code.
		
		// price
		priceSlider = [[[UISlider alloc] initWithFrame:CGRectMake(xposSlider,yposSlider,widthSlider,heightSlider)] autorelease];		
		priceSlider.maximumValue=10;
		priceSlider.minimumValue=0; 
		
		priceName = [[[UILabel alloc] initWithFrame:CGRectMake(xposName,yposLabel,widthName,heightLabel)] autorelease];	
		priceValue = [[[UILabel alloc] initWithFrame:CGRectMake(xposValue, yposLabel, widthValue, heightLabel)] autorelease];
		priceValue.textAlignment = NSTextAlignmentRight;
		priceValue.textColor = [UIColor	grayColor];
		priceName.text = @"Max Price";
        priceName.backgroundColor = [UIColor clearColor];
        priceValue.backgroundColor = [UIColor clearColor];
		
		[self.contentView addSubview:priceSlider];
		[self.contentView addSubview:priceName];
		[self.contentView addSubview:priceValue];
				
		// beds
		bedsSlider = [[[UISlider alloc] initWithFrame:CGRectMake(xposSlider,yposSlider + rowHeight,widthSlider,heightSlider)] autorelease];		
		bedsSlider.maximumValue=10;
		bedsSlider.minimumValue=0; 
		
		bedsName = [[[UILabel alloc] initWithFrame:CGRectMake(xposName,yposLabel + rowHeight,widthName,heightLabel)] autorelease];
		bedsName.backgroundColor = [UIColor clearColor];
		bedsValue = [[[UILabel alloc] initWithFrame:CGRectMake(xposValue, yposLabel + rowHeight, widthValue, heightLabel)] autorelease];
		bedsValue.textAlignment = NSTextAlignmentRight;
		bedsValue.textColor = [UIColor	grayColor];
        bedsValue.backgroundColor = [UIColor clearColor];
		bedsName.text = @"Min Beds";
		
		[self.contentView addSubview:bedsSlider];
		[self.contentView addSubview:bedsName];
		[self.contentView addSubview:bedsValue];
		
		bedsValue.text = @"Any";
		
		// baths
		bathsSlider = [[[UISlider alloc] initWithFrame:CGRectMake(xposSlider, yposSlider + 2*rowHeight,widthSlider,heightSlider)] autorelease];		
		bathsSlider.maximumValue=10;
		bathsSlider.minimumValue=0; 
		
		bathsName = [[[UILabel alloc] initWithFrame:CGRectMake(xposName,yposLabel + 2*rowHeight,widthName,heightLabel)] autorelease];
		bathsName.backgroundColor = [UIColor clearColor];
		bathsValue = [[[UILabel alloc] initWithFrame:CGRectMake(xposValue, yposLabel + 2*rowHeight, widthValue, heightLabel)] autorelease];
		bathsValue.textAlignment = NSTextAlignmentRight;
		bathsValue.textColor = [UIColor	grayColor];
        bathsValue.backgroundColor = [UIColor clearColor];
		
		[self.contentView addSubview:bathsSlider];
		[self.contentView addSubview:bathsName];
		[self.contentView addSubview:bathsValue];
		
		bathsName.text = @"Min Baths";
		bathsValue.text = @"Any";
		
		// structure size
		structureSizeSlider= [[[UISlider alloc] initWithFrame:CGRectMake(xposSlider, yposSlider + 3*rowHeight,widthSlider,heightSlider)] autorelease];		
		structureSizeSlider.maximumValue=10;
		structureSizeSlider.minimumValue=0; 
		
		structureSizeName = [[[UILabel alloc] initWithFrame:CGRectMake(xposName,yposLabel + 3*rowHeight,widthName + 110,heightLabel)] autorelease];
        structureSizeName.backgroundColor = [UIColor clearColor];
		
		structureSizeValue = [[[UILabel alloc] initWithFrame:CGRectMake(xposValue+20, yposLabel + 3*rowHeight, widthValue-20, heightLabel)] autorelease];
		structureSizeValue.textAlignment = NSTextAlignmentRight;
		structureSizeValue.textColor = [UIColor	grayColor];
        structureSizeValue.backgroundColor = [UIColor clearColor];
		
		[self.contentView addSubview:structureSizeSlider];
		[self.contentView addSubview:structureSizeName];
		[self.contentView addSubview:structureSizeValue];
		
		structureSizeName.text = @"Min Structure Size";
		structureSizeValue.text = @"Any";
        
        // dom slider
		domSlider = [[[UISlider alloc] initWithFrame:CGRectMake(xposSlider, yposSlider + 4*rowHeight,widthSlider,heightSlider)] autorelease];
		domSlider.maximumValue=10;
		domSlider.minimumValue=0;
		
		domName = [[[UILabel alloc] initWithFrame:CGRectMake(xposName,yposLabel + 4*rowHeight,widthName,heightLabel)] autorelease];
		domName.backgroundColor = [UIColor clearColor];
		domValue = [[[UILabel alloc] initWithFrame:CGRectMake(xposValue, yposLabel + 4*rowHeight, widthValue, heightLabel)] autorelease];
		domValue.textAlignment = NSTextAlignmentRight;
		domValue.textColor = [UIColor	grayColor];
        domValue.backgroundColor = [UIColor clearColor];
		
		[self.contentView addSubview:domSlider];
		[self.contentView addSubview:domName];
		[self.contentView addSubview:domValue];
		
		domName.text = @"Max DOM";
		domValue.text = @"Any";
				
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state.
}


- (void)dealloc {
    [super dealloc];
}


@end
