//
//  CheckListController.m
//  R1
//
//  Created by stephen feggeler on 1/24/13.
//  Copyright 2013 self. All rights reserved.
//

#import "CheckListController.h"
#import "MultiCheckListController.h"

@implementation CheckListController

@synthesize delegate;
@synthesize items;
@synthesize itemsArray;

- (IBAction)doneSelecting:sender
{		
	
	[delegate checkListControllerDidFinishSelecting:self];	
	
} // end method doneAdding:


// called when the user touches one of the rows in the table
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{   	
	ListItem *item = [itemsArray objectAtIndex:indexPath.row];
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    BOOL isAnyRow = checkmatch(item.name, @"Any");
    
    if (isAnyRow)
    {
        if (item.isSelected)
        {
            // already selected so don't do anything
            return;
        }
        else
        {
            // user selected 'any' so uncheck everything else
            //
            
            item.isSelected = YES;
            
            [self tableView:tableView forRow:0 inSection:0 isSelected: YES];
            
            // uncheck these rows in UI
            for (NSUInteger i=1; i< itemsArray.count; ++i) {
                
                [self tableView:tableView forRow:i inSection:0 isSelected: NO];
            }
            
            for (NSUInteger i=1; i< itemsArray.count; ++i) {
                
                ListItem *item = (ListItem *)[itemsArray objectAtIndex:i];
                item.isSelected = NO;
            }
            
            return;
        }
    }
    
    // if here selected non 'any' row
    //
    
    // unselect 'any'         
    ListItem *theAnyItem = (ListItem *)[itemsArray objectAtIndex:0];
    
    theAnyItem.isSelected = NO;
    
    // flip selected item
    item.isSelected = !item.isSelected;
    
    // update the UI for selected row
    [self tableView:tableView forRow:indexPath.row inSection:indexPath.section isSelected: item.isSelected];
    
    if (item.isSelected == NO)
    {
        // Something should always be selected.
        // If not, select any
        
        BOOL somethingSelected = NO;
        
        // use the unfilterd list
        //  list = [[checkLists objectAtIndex: selectedList] list];
        
        for (NSUInteger i=0; i< itemsArray.count; ++i) {
			
			ListItem *item = (ListItem *)[itemsArray objectAtIndex:i];
            
            if(item.isSelected){
                
                somethingSelected = YES;
                break;
            }
		}
        
        if(somethingSelected == NO)
        {
            // select 'any'
            
            ListItem *anyItem = (ListItem *)[itemsArray objectAtIndex:0];
            
            anyItem.isSelected = YES;
        }
    }
    
    // update UI for 'any' if in the list
    // update the UI for selected row
    
    ListItem *firstItem = (ListItem *)[itemsArray objectAtIndex:0];
    
    if ([firstItem.name isEqualToString:@"Any"])
    {
        [self tableView:tableView forRow:0 inSection:0 isSelected: firstItem.isSelected];
    }
 
}

- (void)tableView:(UITableView *)tableView
           forRow:(NSInteger)row
        inSection:(NSInteger)section
       isSelected: (BOOL)selected
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section] ;
    
	UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
	
	if(selected)
    {
		cell.accessoryType = UITableViewCellAccessoryCheckmark;
	}
    else
    {
		cell.accessoryType = UITableViewCellAccessoryNone;
        
	}
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1; //one male and other female
}

// determines how many rows are in a given section
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return itemsArray.count;
}

// retrive tableView's cell at the given index path
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	// used to identify cell as a normal cell
	static NSString *MyIdentifier = @"NormalCell";
	
	// get a reusable cell
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
	
	// if there are no cells to be reused, create one
	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier] autorelease];
	} // end if
			
	// get the label for the cell
	UILabel *label = [cell textLabel];
	
	// update the text of the label
	
	ListItem *item = [itemsArray objectAtIndex:indexPath.row];
	
	label.text = item.name;
	
	if(item.isSelected){
		cell.accessoryType = UITableViewCellAccessoryCheckmark;		
	}else {
		cell.accessoryType = UITableViewCellAccessoryNone;	
	}
	
	// return the customized cell
	return cell;
} // end method tableView:cellForRowAtIndexPath:

// determins the title for a given table header
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	return nil; // there are no section headers
} // end method tableView:titleForHeaderInSection:

// determines if a table row can be edited
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	return NO; // none of the rows are editable
}


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7){
        self.automaticallyAdjustsScrollViewInsets = YES;
        self.edgesForExtendedLayout = UIRectEdgeAll;
    }
    
    // done
    UIBarButtonItem *doneButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneSelecting:)];
    self.navigationItem.rightBarButtonItem = doneButtonItem;
    [doneButtonItem release];
    
    // title
    self.navigationItem.title = @"Property Type";
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc {
    [super dealloc];
}


@end
