//
//  SFColorButton.m
//  CustomAnnotation
//
//  Created by stephen feggeler on 10/27/13.
//
//

#import "SFColorButton.h"

@implementation SFColorButton

@synthesize mainColor = _mainColor;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (UIColor*)mainColor{
    
    return _mainColor;
}


- (void)setMainColor:(UIColor *)color {
    
    _mainColor = [color retain];
    
    self.backgroundColor = color;
}

-(void)setHighlighted:(BOOL)highlighted {
    
    if (!_mainColor) {
        [super setHighlighted:highlighted];
        return;
    }
    
    if(highlighted) {
        self.backgroundColor = [self darkerShade];
    } else {
        self.backgroundColor = self.mainColor;
    }
    [super setHighlighted:highlighted];
}

-(void) setSelected:(BOOL)selected {
    
    //[super setSelected:selected];
    //return;
    
    if(selected) {
//        self.backgroundColor = [self darkerShade];
    } else {
        self.backgroundColor = self.mainColor;
    }
    [super setSelected:selected];
}

-(UIColor*) darkerShade
{
    CGFloat red=0, green=0, blue=0, alpha=0;
    [self.mainColor getRed:&red green:&green blue:&blue alpha:&alpha];
    CGFloat multiplier = 0.8f;
    if (red == green == blue) {
        multiplier = 0.9; // if monocolors use different multiplier
    }
    
    return [UIColor colorWithRed:red * multiplier green:green * multiplier blue:blue*multiplier alpha:alpha];
}

-(void)dealloc{
    if (_mainColor) {
        [_mainColor release];
    }
    
    [super dealloc];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
