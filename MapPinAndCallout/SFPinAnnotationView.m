//
//  CustomAnnotationView.m
//  CustomAnnotation
//
//  Created by akshay on 8/17/12.
//  Copyright (c) 2012 raw engineering, inc. All rights reserved.
//

#import <objc/runtime.h>
#import "SFPinAnnotationView.h"
#import "SFCalloutView.h"
#import "Annotation.h"
#import "SFColorButton.h"
#import <QuartzCore/QuartzCore.h>

#define MIN_DISTANCE_FROM_EDGE 5
#define PIN_ARROW_HEIGHT 6
#define PIN_ARROW_WIDTH  10

static BOOL blockSiblings = NO;
static BOOL gCalloutHit = NO;
static BOOL gResetHitTest = YES;

// assiciative reference
static char const * const ObjectTagKey = "CalloutContainerTag";

@interface MKMapView(SFPinAnnotationView)

@property (retain, atomic) SFCalloutView *calloutView;

@end

@implementation MKMapView(SFPinAnnotationView)

@dynamic calloutView;

- (id)calloutView {
    return objc_getAssociatedObject(self, ObjectTagKey);
}

-(void)setCalloutView:(SFCalloutView *)newCalloutContainer
{
    objc_setAssociatedObject(self, ObjectTagKey, newCalloutContainer, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIView *) hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    
    UIView *view = [super hitTest:point withEvent:event];
    //NSLog( @"map touches %@  self description %@", [view description], [self description]);
    
    if(self.calloutView){
        CGPoint p1 = [self.calloutView convertPoint:point fromView:self];
        BOOL touchedCallout = [self.calloutView pointInside:p1 withEvent:event];
        if (touchedCallout == YES) {
            UIView *calloutHitTest = [self.calloutView hitTest:p1 withEvent:event];
            //NSLog(@" hitMeTest %@", [calloutHitTest description]);
            self.zoomEnabled = NO;
            self.scrollEnabled = NO;
//          self.userInteractionEnabled = NO;
            return calloutHitTest;
        }
    }
//  self.userInteractionEnabled = YES;
    self.scrollEnabled = YES;
    self.zoomEnabled = YES;
    
    return view;
}
@end


@interface SFPinAnnotationView()

@property (retain, nonatomic) NSTimer *timer;
@property (assign, nonatomic) CLLocationCoordinate2D prevCenter;
@property (assign, nonatomic) int timerCount;
@property (assign, nonatomic) BOOL calloutHit;
@property (assign, nonatomic) CGPoint touchPt;
@property (retain, nonatomic) UIScrollView *scrollView;
@property (retain, nonatomic) UIPageControl *pageControl;
@property (assign, nonatomic) BOOL wasPickPosition;

- (void)startTimer;
- (void)timerFired;

@end

@implementation SFPinAnnotationView

#pragma mark -
#pragma mark drawing pin

- (void)drawRect:(CGRect)rect
{
    [self drawPin:UIGraphicsGetCurrentContext()];
    
    self.layer.backgroundColor = [[UIColor clearColor] CGColor];

    return;
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowOpacity = 0.5;
    self.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    self.layer.shadowRadius = 1.0;
    
  }

-(void)drawPin:(CGContextRef)context
{
    CGRect frame = self.bounds;
    CGFloat strokeWidth = 1.5;
    CGFloat borderRadius = 6.0;
    CGFloat arrowHeight = PIN_ARROW_HEIGHT;
    CGFloat arrowWidth = PIN_ARROW_WIDTH;
    
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextSetLineWidth(context, strokeWidth);
    //CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:10/256.0 green:70/256.0 blue:140/256.0 alpha:0.9].CGColor);
    CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:250/256.0 green:250/256.0 blue:250/256.0 alpha:0.9].CGColor);
    
   // CGContextSetFillColorWithColor(context, [UIColor colorWithRed:33/256.0 green:125/256.0 blue:234/256.0 alpha:0.9].CGColor);
  //  CGContextSetFillColorWithColor(context, [UIColor colorWithRed:18/256.0 green:73/256.0 blue:210/256.0 alpha:0.9].CGColor);
   // CGContextSetFillColorWithColor(context, [UIColor colorWithRed:237/256.0 green:133/256.0 blue:0/256.0 alpha:0.9].CGColor);
CGContextSetFillColorWithColor(context, [UIColor colorWithRed:185/256.0 green:95/256.0 blue:0/256.0 alpha:0.9].CGColor);
    //CGContextSetFillColorWithColor(context, [UIColor colorWithRed:40/255.0 green:80/255.0 blue:110/255.0 alpha:1.0].CGColor);
   // CGContextSetFillColorWithColor(context, [UIColor colorWithRed:24/255.0 green:88/255.0 blue:154/255.0 alpha:1.0].CGColor);
   // CGContextSetFillColorWithColor(context, [UIColor colorWithRed:24/255.0 green:110/255.0 blue:160/255.0 alpha:1.0].CGColor);
    //CGContextSetFillColorWithColor(context, [UIColor colorWithRed:76/255.0 green:90/255.0 blue:140/255.0 alpha:1.0].CGColor);
//CGContextSetFillColorWithColor(context, [UIColor colorWithRed:66/255.0 green:106/255.0 blue:169/255.0 alpha:1.0].CGColor);
  //  CGContextSetFillColorWithColor(context, [UIColor colorWithRed:22/255.0 green:141/255.0 blue:78/255.0 alpha:1.0].CGColor);

CGContextSetFillColorWithColor(context, [UIColor colorWithRed:63/255.0 green:131/255.0 blue:143/255.0 alpha:1.0].CGColor);

    
    CGFloat xmin =  strokeWidth;
    CGFloat ymin =  strokeWidth;
    CGFloat xmax = frame.size.width - strokeWidth;
    CGFloat ymax = frame.size.height - strokeWidth;
    
    CGFloat box_xmin = xmin;
    CGFloat box_ymin = ymin;
    CGFloat box_xmax = xmax;
    CGFloat box_ymax = ymax - arrowHeight;
    CGFloat arc_radious = borderRadius - strokeWidth;
    
    CGFloat arrow_tip_x = box_xmin + (box_xmax - box_xmin)/2.0;
    CGFloat arrow_tip_y = ymax;
    CGFloat arrow_left_x = arrow_tip_x - arrowWidth / 2.0f;
    CGFloat arrow_left_y = box_ymax;
    CGFloat arrow_right_x = arrow_tip_x + arrowWidth / 2.0f;
    CGFloat arrow_right_y = box_ymax;
    
    CGContextBeginPath(context);
    
    CGContextMoveToPoint(context,
                         arrow_left_x,
                         arrow_left_y);
    
    CGContextAddLineToPoint(context,
                            arrow_tip_x,
                            arrow_tip_y);
    
    CGContextAddLineToPoint(context,
                            arrow_right_x,
                            arrow_right_y);
    
    
    CGContextAddArcToPoint(context,     // right side, lower right corner
                           box_xmax,
                           box_ymax,
                           box_xmax,
                           box_ymin,
                           arc_radious);
    
    CGContextAddArcToPoint(context,
                           box_xmax,
                           box_ymin,
                           box_xmin,
                           box_ymin,
                           arc_radious);
    
    CGContextAddArcToPoint(context,
                           box_xmin,
                           box_ymin,
                           box_xmin,
                           box_ymax,
                           arc_radious);
    
    CGContextAddArcToPoint(context,
                           box_xmin,
                           box_ymax,
                           arrow_left_x,
                           arrow_left_y,
                           arc_radious);
    
    CGContextClosePath(context);
    CGContextDrawPath(context, kCGPathFillStroke);
}

- (id)initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        self.calloutArrowSize = 12.0;
    }
    return self;
}

- (void)setText:(NSString*)text
{
    if (!self.label) {
        self.label = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)] autorelease];
        [self addSubview:self.label];
    }
    UILabel *lbl = self.label;
	lbl.backgroundColor = [UIColor clearColor];
	lbl.textColor = [UIColor whiteColor];
    
    // opacity slows up rendering
    //lbl.layer.shadowColor = [[UIColor blackColor] CGColor];
    //lbl.layer.shadowOpacity = 0.7;
    //lbl.layer.shadowOffset = CGSizeMake(0.3f, 0.3f);
    //lbl.layer.shadowRadius = 1.0;

	lbl.alpha = 1;
	lbl.textAlignment = NSTextAlignmentCenter;
	lbl.text = text;
	//	lbl.adjustsFontSizeToFitWidth = YES;
	lbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:13];
	lbl.font = [UIFont boldSystemFontOfSize:13.5f];
    lbl.font = [UIFont boldSystemFontOfSize:14.0f];
	lbl.layer.cornerRadius = 8;
	
	//
	// adjust size
	//
	CGSize maximumLabelSize = CGSizeMake(296,20);
	

    CGRect textRect = [lbl.text boundingRectWithSize:maximumLabelSize
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{NSFontAttributeName:lbl.font}
                                                 context:nil];
	
	//adjust the label the the new height.
	CGRect frame = lbl.frame;
	CGFloat newWidth = textRect.size.width + 15;
    frame.size.width = newWidth;
    frame.size.height = textRect.size.height + 4;
	
    lbl.frame = frame;
    
    // make pin frame bigger then label frame
    
    CGRect pinframe = CGRectMake(0, 0, 0, 0);
    pinframe.size.width = lbl.frame.size.width + 0;
    pinframe.size.height = lbl.frame.size.height + 8;
  //  frame.origin.x = -frame.size.width/2;
  //  frame.origin.y = -frame.size.height/2;
    self.frame = pinframe;
    
    // center pin
    CGPoint centerOffset = CGPointMake(0, (pinframe.origin.y + PIN_ARROW_HEIGHT/2 - pinframe.size.height/2));
    self.centerOffset = centerOffset;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated{
    
    //[self logNotes1:selected];
    
    NSLog(@" selected=%s blockSiblings=%s calloutHit=%s",
          selected ? "true" : "false",
          blockSiblings ? "true" : "false",
          self.calloutHit ? "true" : "false");
    
    gResetHitTest = YES;
    
    BOOL hasCallout = self.map.calloutView != nil;
    BOOL isCalloutChild = self != [self.map.calloutView superview];
   
    if (hasCallout && isCalloutChild && blockSiblings) {
        self.layer.zPosition = -1;
        return;
    }
    self.layer.zPosition = 0;
    
    [super setSelected:selected animated:animated];

    if(selected)
    {
        // if pin selected under callout, keep the pin below callout
        if(self.map.calloutView != nil && gCalloutHit)
        {
            self.layer.zPosition = 0;
            return;
        }
        [self addCallout];
     }
    else
    {
        // if callout touched, undo default behavior and reselect pin
        if (self.calloutHit) {
            self.layer.zPosition = 10;
            blockSiblings = YES;
            [self performSelector:@selector(reselectPin) withObject:nil afterDelay:0.1];
            return;
        }
        
        // callout not touched, so remove callout
        if (self == self.map.calloutView.superview) {
            [self.map.calloutView removeFromSuperview];
            self.map.calloutView = nil;
            blockSiblings = NO;
        }        
    }
}

- (void)removeCallout{
    if (self == self.map.calloutView.superview) {
        [self.map.calloutView removeFromSuperview];
        self.map.calloutView = nil;
        blockSiblings = NO;
        gCalloutHit = NO;
        gResetHitTest = YES;
    }    
}

- (void)addCallout{
    
    // if already have callout, remove it
    if (self.map.calloutView) {
        [self.map.calloutView removeFromSuperview];
        self.map.calloutView = nil;
    }
    
    // get content view
    UIView* contentView = [self calloutContentView];
    
    if(self.calloutPosition == SFAvoveOrBelowCalloutPosition || self.wasPickPosition){
        self.wasPickPosition = YES;
        self.calloutPosition  = [self pickCalloutPosition:contentView.frame.size.height];
    }
    
    // position contentView above or below pin
    CGRect calloutFrame;
    if (self.calloutPosition == SFAbovePinCalloutPosition)
    {
        calloutFrame = [self abovePinCalloutFrame:contentView.frame];
    }
    else if(self.calloutPosition == SFBelowPinCalloutPosition)
    {
        calloutFrame = [self belowPinCalloutFrame:contentView.frame];
    }
    else{
        calloutFrame = [self abovePinCalloutFrame:contentView.frame];
    }
    
    // create callout view
    self.map.calloutView = [[[SFCalloutView alloc] initWithFrame:calloutFrame calloutPosition:self.calloutPosition] autorelease];
    self.map.calloutView.calloutPosition = self.calloutPosition;
    
    if (self.calloutPosition == SFBelowPinCalloutPosition) {
        CGRect frame = contentView.frame;
        frame.origin.y += self.calloutArrowSize;
        contentView.frame = frame;
    }
    
    // put content in callout
    [self.map.calloutView addSubview:contentView];
    
    // map may need to move
    CGPoint ptsToMove = [self pointsToMoveMap];
    
    // convert from pts to region
    MKCoordinateRegion movedRegion = [self moveMapThesePts:ptsToMove];
    
    // move map and show callout
    if (ptsToMove.x != 0.0 || ptsToMove.y != 0.0) {
        
        // move map, and when finished moving, show callout
        [self startTimer];
        [self.map setRegion:movedRegion animated:NO];
    }
    else{
        
        // don't move map, show callout
        [self showCallout];
    }
}

- (SFCalloutPosition)pickCalloutPosition:(CGFloat)calloutHeight{
    
    CGPoint pinOrigin = [self.map convertPoint:CGPointZero fromView:self];
//  pinOrigin = self.frame.origin;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"77.0")) {
        if ((pinOrigin.y - calloutHeight) > 65.0)
            return SFAbovePinCalloutPosition;
        else
            return SFBelowPinCalloutPosition;
    }
    else{
        if ((pinOrigin.y - calloutHeight) > 0.0)
            return SFAbovePinCalloutPosition;
        else
            return SFBelowPinCalloutPosition;
    }
}

- (CGPoint)pointsToMoveMap{
    
    CGPoint pinOrigin = [self.map convertPoint:CGPointZero fromView:self];
//  pinOrigin = self.frame.origin;
    CGRect pinFrame = self.frame;
    pinFrame.origin = pinOrigin;
    
    CGRect calloutFrame = self.map.calloutView.frame;
    calloutFrame.origin.x += pinOrigin.x;
    calloutFrame.origin.y += pinOrigin.y;
    
    CGRect totalFrame = CGRectUnion (pinFrame, calloutFrame);
    CGPoint distanceToMove = [self moveRect:totalFrame insideContainer:self.map.frame minDistanceFromEdge:MIN_DISTANCE_FROM_EDGE + self.extraCalloutSideDistance];
    
    return distanceToMove;
}

- (UIView*)propertyViewInFrame:(CGRect)frame{
    
    CGFloat y_offset = 15;
    
    // property view on right side of callout
    SFColorButton * propertyView = [SFColorButton buttonWithType:UIButtonTypeCustom];
    propertyView.frame = frame;
    propertyView.mainColor = [UIColor whiteColor];
    
    // title
    UILabel * title = [[[UILabel alloc] init] autorelease];
    [title setFrame:CGRectMake(10, 10 + y_offset, 200, 20)];
    title.text = @"SFR, 4 Bd, 2 Ba, 12 DOM";
    //title.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
    title.font = [UIFont boldSystemFontOfSize:16];
    title.textColor = [UIColor darkGrayColor];
    title.backgroundColor = [UIColor clearColor];
    [propertyView addSubview:title];
    
    // price
    UILabel * price = [[[UILabel alloc] init] autorelease];
    [price setFrame:CGRectMake(120, 35 + y_offset, 200, 20)];
    price.text = @"$1,900,000";
    price.font = [UIFont boldSystemFontOfSize:15];
    price.textColor = [UIColor blackColor];
    price.backgroundColor = [UIColor clearColor];
    [propertyView addSubview:price];
    
    // neighborhood
    UILabel * hood = [[[UILabel alloc] init] autorelease];
    [hood setFrame:CGRectMake(120, 55 + y_offset, 140, 20)];
    hood.text = @"Pacific Heights";
    hood.font = [UIFont boldSystemFontOfSize:13];
    hood.textColor = [UIColor darkGrayColor];
    hood.backgroundColor = [UIColor clearColor];
    [propertyView addSubview:hood];
    
    // street address
    UILabel * address1 = [[[UILabel alloc] init] autorelease];
    [address1 setFrame:CGRectMake(120, 70 + y_offset, 140, 20)];
    address1.text = @"2250 vallejo st, #10";
    address1.font = [UIFont boldSystemFontOfSize:13];
    address1.textColor = [UIColor darkGrayColor];
    address1.backgroundColor = [UIColor clearColor];
    [propertyView addSubview:address1];
    
    // city address
    UILabel * address2 = [[[UILabel alloc] init] autorelease];
    [address2 setFrame:CGRectMake(120, 85 + y_offset, 140, 20)];
    address2.text = @"San Francisco, 94123";
    address2.font = [UIFont boldSystemFontOfSize:13];
    address2.textColor = [UIColor darkGrayColor];
    address2.backgroundColor = [UIColor clearColor];
    [propertyView addSubview:address2];
    
    // picture
    UIImageView *imageView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sf_house_small.jpg"]] autorelease];
    imageView.backgroundColor = [UIColor lightGrayColor];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    CGRect pic_frame = imageView.frame;
    pic_frame.origin.x = 10;
    pic_frame.origin.y = 35 + y_offset;
    pic_frame.size.width = 100;
    pic_frame.size.height = 75;
    imageView.frame = pic_frame;
    [propertyView addSubview:imageView];

    return propertyView;
}

-(UIScrollView*)ScrollViewInFrame:(CGRect)frame withPageCount:(NSInteger)pageCount{
    
    NSInteger viewcount= pageCount;
    UIScrollView * sv = [[UIScrollView alloc] init];
    sv.frame = frame;
    sv.contentSize = CGSizeMake(viewcount * sv.frame.size.width, sv.frame.size.height);
    sv.backgroundColor = [UIColor whiteColor];
    sv.showsVerticalScrollIndicator = YES;
    sv.showsHorizontalScrollIndicator = YES;
    sv.scrollEnabled=YES;
    sv.userInteractionEnabled=YES;
    sv.scrollEnabled = YES;
    sv.exclusiveTouch = NO;
    sv.pagingEnabled = YES;
    sv.bounces = YES;
    sv.delegate = self;
    
    return sv;
}

- (UIPageControl*)pageControlInFrame:(CGRect)frame withPageCount:(NSInteger)pageCount{
    
    UIColor *color1 = [[[UIColor alloc] initWithRed:20/255.f green:156/255.f blue:255/255.f alpha:0.4] autorelease];
    UIColor *color2 = [[[UIColor alloc] initWithRed:20/255.f green:156/255.f blue:255/255.f alpha:1] autorelease];
    
    UIPageControl * pc = [[UIPageControl alloc] initWithFrame:frame];
    pc.currentPage = 0;
    pc.numberOfPages = pageCount;
    pc.pageIndicatorTintColor = color1;
    pc.currentPageIndicatorTintColor = color2;
    pc.backgroundColor = [[[UIColor alloc] initWithRed:34/255.f green:68/255.f blue:98/255.f alpha:1] autorelease];
    
    [pc addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventValueChanged];

    return pc;
}

- (UIView*)calloutContentView
{
    CGRect contentFrame = CGRectMake(0, 0, 300, 120);
    UIView *contentBox = [[[UIView alloc] initWithFrame:contentFrame] autorelease];
    
    // look for pins with same location
    NSInteger pins_with_this_location = 0;
    CLLocationCoordinate2D thisLocation = ((Annotation*)self.annotation).coordinate;
    for (id<MKAnnotation> annotation in self.map.annotations){
        CLLocationCoordinate2D loc = ((Annotation*)annotation).coordinate;
        if(CLCOORDINATES_EQUAL(loc, thisLocation)){
            ++pins_with_this_location;
        }
    }
    
    // use scrollView to display multiple properties
    NSInteger pageCount = pins_with_this_location;
    if (pageCount>1){
        // multiple property views
        CGRect scrollFrame = CGRectMake(40, -15, contentBox.frame.size.width - 40, contentBox.frame.size.height+15 );
        UIScrollView * scrollView = [self ScrollViewInFrame:scrollFrame withPageCount:pageCount];
        self.scrollView = scrollView;
        
        // add pages to scrollview
        for(int i = 0; i< pageCount; i++) {
            CGFloat x = i * scrollView.frame.size.width;
            CGRect pageFrame = CGRectMake(x, 0, scrollView.frame.size.width, scrollView.frame.size.height );
            UIView * pageView = [self propertyViewInFrame:pageFrame];
            [scrollView addSubview:pageView];
            [contentBox addSubview:scrollView];
        }
        
        CGFloat height_for_page_control = 15;
        CGFloat y_for_page_control = scrollFrame.origin.y + scrollFrame.size.height;
        CGRect pageControlFrame = CGRectMake(40, y_for_page_control - 15, scrollView.frame.size.width, height_for_page_control + 30);
        UIPageControl * pageControl = [self pageControlInFrame:pageControlFrame withPageCount:pageCount];
        self.pageControl = pageControl;
        [contentBox addSubview:pageControl];
        scrollView.layer.zPosition = 10;
        
        // make content frame height larger so page control can fit
        contentFrame.size.height += height_for_page_control;
        contentBox.frame = contentFrame;
    }
    else{
        // one property view
        CGRect propertyFrame = CGRectMake(40, -15, contentBox.frame.size.width - 40, contentBox.frame.size.height + 30 );
        UIView * propertyView = [self propertyViewInFrame:propertyFrame];
        [contentBox addSubview:propertyView];
    }
    
    // car button
    SFColorButton *carButton = [SFColorButton buttonWithType:UIButtonTypeCustom];
    [[carButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [carButton setImage:[UIImage imageNamed:@"car_44x44_white_1.png"] forState: UIControlStateNormal];
    [carButton setImage:[UIImage imageNamed:@"car_44x44_white_1.png"] forState: UIControlStateHighlighted];
    //button.frame = CGRectMake(0, 0, 40, contentBox.frame.size.height);
    carButton.frame = CGRectMake(0, -15, 40, contentBox.frame.size.height/2 + 15);
    carButton.imageEdgeInsets = UIEdgeInsetsMake(15, 0, 0, 0);
    carButton.backgroundColor = [UIColor colorWithRed:18/256.0 green:73/256.0 blue:210/256.0 alpha:0.9];
    [contentBox addSubview:carButton];
    
    // pegman button
    SFColorButton *pegman = [SFColorButton buttonWithType:UIButtonTypeCustom];
    [[pegman imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [pegman setImage:[UIImage imageNamed:@"pegman_44X44_white.png"] forState: UIControlStateNormal];
    [pegman setImage:[UIImage imageNamed:@"pegman_44X44_white.png"] forState: UIControlStateHighlighted];
    pegman.frame = CGRectMake(0, contentBox.frame.size.height/2, 40, contentBox.frame.size.height/2 + 15);
    pegman.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 25, 0);
    pegman.backgroundColor = [UIColor colorWithRed:18/256.0 green:73/256.0 blue:210/256.0 alpha:0.9];
    [contentBox addSubview:pegman];
    
    return contentBox;
}

- (UIView*)calloutContentView2{
    
    CGRect contentFrame = CGRectMake(0, 0, 300, 120);
    UIView *contentBox = [[UIView alloc] initWithFrame:contentFrame];
   // contentBox.backgroundColor = [UIColor blackColor];
    //contentBox.alpha = 0.2;
    
    // scrollview
    NSInteger viewcount= 4;
    UIScrollView *myScroll = [[UIScrollView alloc] init];
    myScroll.frame = CGRectMake(10, 10, contentFrame.size.width - 20, contentFrame.size.height - 40);
    myScroll.contentSize = CGSizeMake(viewcount * myScroll.frame.size.width, myScroll.frame.size.height);
    myScroll.backgroundColor = [UIColor whiteColor];
    myScroll.showsVerticalScrollIndicator = YES;
    myScroll.showsHorizontalScrollIndicator = YES;
    myScroll.scrollEnabled=YES;
    myScroll.userInteractionEnabled=YES;
    myScroll.scrollEnabled = YES;
    myScroll.exclusiveTouch = NO;
    myScroll.pagingEnabled = YES;
    myScroll.bounces = YES;
    myScroll.delegate = self;
    
    self.scrollView = myScroll;
    
    // add pages to scrollview
    
    for(int i = 0; i< viewcount; i++) {
        
        CGFloat x = i * myScroll.frame.size.width;
   //     UIView *view = [[UIView alloc] initWithFrame:CGRectMake(x, 0, myScroll.frame.size.width, myScroll.frame.size.height )];
        
        SFColorButton * view = [SFColorButton buttonWithType:UIButtonTypeCustom];
        view.frame = CGRectMake(x, 0, myScroll.frame.size.width, myScroll.frame.size.height );
        if (i % 2 == 0) {
            view.backgroundColor = [UIColor lightGrayColor];
            view.mainColor = [UIColor lightGrayColor];
        }
        else{
            view.backgroundColor = [UIColor yellowColor];
            view.mainColor = [UIColor yellowColor];
        }
        UIButton * button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        button.frame = CGRectMake(15, 15, 100, 20);
        [button setTitle:@"Hello, world!" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(buttonUp:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:button];
        [myScroll addSubview:view];
    }
    [contentBox addSubview: myScroll];
    
    // add page Controller
    UIPageControl * pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(10, 90, myScroll.frame.size.width, 15)];
    pageControl.currentPage = 0;
    pageControl.numberOfPages = myScroll.contentSize.width/myScroll.frame.size.width;
    
    UIColor *color1 = [[UIColor alloc] initWithRed:20/255.f green:156/255.f blue:255/255.f alpha:0.4];
    UIColor *color2 = [[UIColor alloc] initWithRed:20/255.f green:156/255.f blue:255/255.f alpha:1];
    pageControl.pageIndicatorTintColor = color1;
    pageControl.currentPageIndicatorTintColor = color2;
    pageControl.backgroundColor = [UIColor blackColor];
    [pageControl addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventValueChanged];
    self.pageControl = pageControl;
    [contentBox addSubview: pageControl];
    
     return contentBox;
    
    // add test button
    UIButton * button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.frame = CGRectMake(70, 90, 100, 20);
    [button setTitle:@"Button Here" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonUp:) forControlEvents:UIControlEventTouchUpInside];
    //[contentBox addSubview:button];
    
    return contentBox;
}

BOOL static pageControllingScroll = NO;

- (IBAction)changePage:(id)sender {
    CGFloat x = self.pageControl.currentPage * self.scrollView.frame.size.width;
    [self.scrollView setContentOffset:CGPointMake(x, 0) animated:YES];
    pageControllingScroll = YES;
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
     pageControllingScroll = NO;
}

-(void) scrollViewDidScroll:(UIScrollView *)scrollView  {
    CGFloat pageWidth = self.scrollView.frame.size.width;
    float fractionalPage = self.scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    if(!pageControllingScroll)
        self.pageControl.currentPage = page;
}

- (void)buttonUp:(id) sender{
    NSLog(@"buttonUp");
}

- (CGRect)sidePositionCalloutFrame:(CGRect)frame
{
    CGRect calloutFrame = frame;
   
    // slide callout to left or right to fit on map
    
    // since callout is a subview of pin, calculate callouts x pos on map
    
    // the pinOrigin is needed for IOS 6.1
    CGPoint pinOrigin = [self.map convertPoint:CGPointZero fromView:self];
    CGFloat callout_min_x = pinOrigin.x + calloutFrame.origin.x;
    
    // if callout origin less then zero, move to right
    if (callout_min_x < 0 + self.extraPinSideDistance + MIN_DISTANCE_FROM_EDGE) {
        CGFloat move_callout_x = -callout_min_x + self.extraPinSideDistance + MIN_DISTANCE_FROM_EDGE;
        calloutFrame.origin.x += move_callout_x;
    }
    
    // if callout right side greater the map width, slide to left
    CGFloat callout_max_x = callout_min_x + calloutFrame.size.width;
    
    if (callout_max_x + self.extraPinSideDistance + MIN_DISTANCE_FROM_EDGE > self.map.frame.size.width) {
        CGFloat distance_outside_map = callout_max_x + self.extraPinSideDistance + MIN_DISTANCE_FROM_EDGE - self.map.frame.size.width;
        calloutFrame.origin.x -= distance_outside_map;
    }
    
    // check left side
    // callout must be min 15 pts to the left of the pin center.
    // if not, shift callout to left
    callout_min_x = pinOrigin.x + calloutFrame.origin.x;
    callout_max_x = callout_min_x + calloutFrame.size.width;
    
    CGFloat pin_x_center = pinOrigin.x + self.frame.size.width/2 + self.centerOffset.x;
    
    CGFloat left_side_distance = pin_x_center - callout_min_x;
    CGFloat right_side_distance = callout_max_x - pin_x_center;
    
    if (left_side_distance < 30) {
        calloutFrame.origin.x -= (30 - left_side_distance);
    }
    
    if (right_side_distance < 30) {
        calloutFrame.origin.x += (30 - right_side_distance);
    }
    
    // check left side of pin off map
    // and left side pin past left side of callout
    // this only happens for very large pins
    
    callout_min_x = pinOrigin.x + calloutFrame.origin.x;
    if (pinOrigin.x < 0 && (pinOrigin.x < callout_min_x)) {
        calloutFrame.origin.x -= (callout_min_x - pinOrigin.x);
    }
    
    // check if right side of pin past map edge
    // and pin right side past callout right side
    // this only happens for very large pins
    
    callout_max_x = callout_min_x + calloutFrame.size.width;
    BOOL pin_over_right_side_map = pinOrigin.x + self.frame.size.width > self.map.frame.size.width;
    CGFloat pin_max_x = pinOrigin.x + self.frame.size.width;
    if (pin_over_right_side_map && (callout_max_x < pin_max_x)) {
        calloutFrame.origin.x += (pin_max_x - callout_max_x);
    }
    
    return calloutFrame;
}

- (CGRect)belowPinCalloutFrame:(CGRect)frame
{
    CGRect pinFrame = self.frame;
    CGPoint adjust;
    adjust.x = 0;
    adjust.y = 0;
    
    CGFloat x = pinFrame.size.width/2.0 + adjust.x;
    
    CGRect calloutFrame = frame;
    calloutFrame.origin.x = x - calloutFrame.size.width/2.0;
    
    CGFloat yAdjustForPin = -1.0 + self.yAdjustForBelowCallout;
    calloutFrame.origin.y += pinFrame.size.height + yAdjustForPin;
    
    calloutFrame = [self sidePositionCalloutFrame:calloutFrame];
    
    return calloutFrame;
}

- (CGRect)abovePinCalloutFrame:(CGRect)frame
{
    // step 1
    // position callout above pin and centered around pin
    CGRect pinFrame = self.frame;
    CGPoint adjust;
    adjust.x = -8;
    adjust.y = -.3;
    
    adjust.x = 0;
    adjust.y = 0;
    
    CGFloat x = pinFrame.size.width/2.0 + adjust.x;
    CGFloat y = 0 + adjust.y;
    
    CGRect calloutFrame = frame;
    calloutFrame.origin.x = x - calloutFrame.size.width/2.0;
    
    CGFloat yAdjustForPin = self.yAdjustForAboveCallout;

    calloutFrame.origin.y = y - calloutFrame.size.height + yAdjustForPin;
    
    // now callout is positioned above pin and centered
    
    calloutFrame = [self sidePositionCalloutFrame:calloutFrame];
    
    return calloutFrame;
}

- (void)reselectPin
{
    [self.map selectAnnotation:self.annotation animated:NO];
}

- (void)logNotes1:(BOOL)selected{
    BOOL isChild = (self == [self.map.calloutView superview]);
    NSLog(@"callout is child=%s selected=%s blockSiblings=%s zposition=%f",
          isChild ? "true" : "false",
          selected ? "true" : "false",
          blockSiblings ? "true" : "false",
          self.layer.zPosition);
}


- (void)outputLocationType
{
    Annotation *ann = self.annotation;
    NSLog(@" hittest ann type %@",ann.locationType);
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    // reset callout hit test on each touch
    gResetHitTest = YES;
    if (gResetHitTest) {
        self.calloutHit = NO;
        gResetHitTest = NO;
        gCalloutHit = NO;
    }
    self.touchPt = point; // save this for self.map.calloutView touch testing
    
    //NSLog(@" hit point %@", NSStringFromCGPoint(point));
    
    //[self outputLocationType];
    UIView *hitView = [super hitTest:point withEvent:event];
	
    if (self.map.calloutView != nil) {
        CGPoint ptInCallout = [self.map.calloutView convertPoint:point fromView: self];
        self.calloutHit = [self.map.calloutView pointInside:ptInCallout withEvent:event];
        if (self.calloutHit) {
            blockSiblings = YES;
            gCalloutHit = YES;
            NSLog(@"callout hit !!!!!!!!!!!");
        }
        else
        {
            blockSiblings = NO;
        }
    }
    
    return hitView;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{

}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

- (void)doneTouching{
    self.calloutHit = NO;
}

- (MKCoordinateRegion)moveMapThesePts:(CGPoint)pts
{
    MKCoordinateSpan mapSpan = self.map.region.span;
    
    double mapHeight = self.map.frame.size.height;
    double mapWidth = self.map.frame.size.width;
    
    double reduceHeight = 0;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"77.0")) {
        reduceHeight = 64.0f;
    }
    
    double delta_pixels_to_latitude = mapSpan.latitudeDelta / (mapHeight - reduceHeight);
    double delta_pixels_to_longitude = mapSpan.longitudeDelta / mapWidth;

    // calc position to move
    double x_to_move = pts.x;
    double y_to_move = pts.y;
    
    // move center
    CLLocationCoordinate2D center = self.map.region.center;
    center.latitude  += delta_pixels_to_latitude  * y_to_move;
    center.longitude -= delta_pixels_to_longitude * x_to_move;
    MKCoordinateRegion movedRegion = self.map.region;
    movedRegion.center = center;
    
    return movedRegion;
}

- (CGPoint)moveRect:(CGRect)frame insideContainer:(CGRect)container minDistanceFromEdge:(CGFloat)minDistanceFromEdge
{
    CGPoint distanceToMove = CGPointMake(0 , 0);
    
    // these hack margins are needed for different os versions
    CGFloat marginTop = 0, marginBottom = 44, marginLeft = 0, marginRight = 0;
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"77.0")) {
        marginTop = 65, marginBottom = 0, marginLeft = 0, marginRight = 0;
    }
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        marginTop = 0, marginBottom = 0, marginLeft = 0, marginRight = 0;
    }
    
    // x min
    CGFloat minX = CGRectGetMinX(frame);
    if (minX < minDistanceFromEdge) {
        CGFloat dx = minDistanceFromEdge - minX;
        if (dx > distanceToMove.x) {
            distanceToMove.x = dx;
        }
    }
    
    // x max
    CGFloat maxX = CGRectGetMaxX(frame);
    CGFloat maxAllowedX = CGRectGetMaxX(container) - minDistanceFromEdge;
    if (maxX > maxAllowedX) {
        CGFloat dx = maxAllowedX - maxX;
        if (dx <  distanceToMove.x) {
            distanceToMove.x = dx;
        }
    }
    
    // y min
    CGFloat minY = CGRectGetMinY(frame);
    CGFloat minYDistanceFromEdge = minDistanceFromEdge + marginTop;
    if (minY < minYDistanceFromEdge) {
        CGFloat dy = minYDistanceFromEdge - minY;
        if (dy > distanceToMove.y) {
            distanceToMove.y = dy;
        }
    }
    
    // y max
    CGFloat maxY = CGRectGetMaxY(frame);
    minYDistanceFromEdge = minDistanceFromEdge + marginBottom; // 9
    CGFloat maxAllowedY = CGRectGetMaxY(container) - minYDistanceFromEdge;
    if (maxY > maxAllowedY) {
        CGFloat dy = maxAllowedY - maxY;
        if (dy < distanceToMove.y) {
            distanceToMove.y = dy;
        }
    }
    
    return distanceToMove;
}

- (void)showCallout
{
    // set arrow offset
    CGRect frame = self.map.calloutView.frame;
    CGFloat xoff = (frame.size.width / 2.0) + frame.origin.x - self.frame.size.width/2 + self.centerOffset.x;
    
    // this last bit centers callout and pin
    xoff += -0.3 - self.xAdjustForCalloutPointer;
    
    CGPoint offset = self.map.calloutView.offset;
    
    offset.x -= xoff;
    self.map.calloutView.offset = offset;
    
    [self animateCalloutAppearance];
    
    [self addSubview:self.map.calloutView];
}

- (void)startTimer
{
    self.prevCenter = self.map.region.center;
    self.timerCount = 0;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(timerFired) userInfo:nil repeats:YES];
}

- (void)timerFired
{
    if (self.timerCount > 3 && CLCOORDINATES_EQUAL(self.prevCenter, self.map.region.center)) {
        [self stopTimer];
    }
    self.prevCenter = self.map.region.center;
    ++self.timerCount;
    
    if (self.timerCount > 30) {
        [self stopTimer];
    }
}

- (IBAction)stopTimer
{
    [self.timer invalidate];
    self.timer = nil;
    [self showCallout];
}

- (void)animateCalloutAppearance {
    
    // if anchorpoint changes then frame position must change
    
    // match anchorpoint
    CGRect frame = self.map.calloutView.frame;
    
    // pw ranges from 0.0 (0%) to 1.0 (100%)
    CGFloat pw = self.map.calloutView.offset.x/frame.size.width + 0.5;
    
    if (self.calloutPosition == SFAbovePinCalloutPosition) {
        frame.origin.y += frame.size.height/2;
    }
    else{
        frame.origin.y -= frame.size.height/2;
    }
    
    frame.origin.x -= frame.size.width * (0.5 - pw);
    self.map.calloutView.frame = frame;
    
    CGFloat yAnchorPt = 1.0;
    if (self.calloutPosition == SFBelowPinCalloutPosition) {
        yAnchorPt = 0.0;
    }
    self.map.calloutView.layer.anchorPoint = CGPointMake(pw, yAnchorPt); // 0.5 is middle, 1.0 is bottom edge
 
    CAKeyframeAnimation *bounceAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    
    bounceAnimation.values = [NSArray arrayWithObjects:
                              [NSNumber numberWithFloat:0.4], // was 0.05
                              [NSNumber numberWithFloat:1.08],
                          //    [NSNumber numberWithFloat:0.92],
                              [NSNumber numberWithFloat:1.0],
                              nil];
    
    bounceAnimation.duration = 0.3;
    [bounceAnimation setTimingFunctions:[NSArray arrayWithObjects:
                                         [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                         [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                 //      [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                         [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut],
                                         nil]];
   // bounceAnimation.removedOnCompletion = NO;
    
    [self.map.calloutView.layer addAnimation:bounceAnimation forKey:@"bounce"];
    
    // FADE IN
    CAKeyframeAnimation *opacityAnimation = [CAKeyframeAnimation animationWithKeyPath:@"opacity"];
    
    opacityAnimation.values = [NSArray arrayWithObjects:
                              [NSNumber numberWithFloat:0.3],
                              [NSNumber numberWithFloat:0.9],
                              nil];
    
    opacityAnimation.duration = 0.3;
    
    [opacityAnimation setTimingFunctions:[NSArray arrayWithObjects:
                                         [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear],
                                         [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear],
                                        nil]];
    
    [self.map.calloutView.layer addAnimation:opacityAnimation forKey:@"opacityIN"];
    
    return;
    
    // below does not work. Needs adjustment
    // uses uiview transforms.
    
    CGFloat scale = 0.001f;
    
    self.map.calloutView.transform = CGAffineTransformMake(scale, 0.0f, 0.0f, scale, 0, 50); //-50
    
    [UIView animateWithDuration:0.15 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        CGFloat scale = 1.1f;
        self.map.calloutView.transform = CGAffineTransformMake(scale, 0.0f, 0.0f, scale, 0, -3);
        }
    completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            CGFloat scale = 0.95;
            self.map.calloutView.transform = CGAffineTransformMake(scale, 0.0f, 0.0f, scale, 0, 4);
        }
    completion:^(BOOL finished) {
        [UIView animateWithDuration:0.075 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            CGFloat scale = 1.0;
            self.map.calloutView.transform = CGAffineTransformMake(scale, 0.0f, 0.0f, scale, 0, 0);
        }
    completion:nil];
        }];
    }];
}

@end
