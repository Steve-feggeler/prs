//
//  SFPropertiesPinView.h
//  CustomAnnotation
//
//  Created by stephen feggeler on 11/4/13.
//
//

#import "SFPinAnnotationView.h"
#import "Property.h"

@protocol SFPropertyPinDelegate;

@protocol SFPropertyPinSelectedProperty <NSObject>

- (void)mapView:(MKMapView*)mapView selectedProperty:(Property*)property;

@end

@interface SFPropertiesPinView : SFPinAnnotationView <SFPropertyPinSelectedProperty>

@property (nonatomic, assign) id <SFPropertyPinDelegate> delegate;

-(void)updatePrice;

@end

@protocol SFPropertyPinDelegate <NSObject>

- (void)mapView:(MKMapView*)mapView directionsToProperty:(Property*)property;
- (void)mapView:(MKMapView*)mapView streetViewForProperty:(Property*)property;
- (void)mapView:(MKMapView*)mapView detailsForProperty:(Property*)property;

@end
