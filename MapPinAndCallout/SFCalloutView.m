//
//  CalloutContainer.m
//  CustomAnnotation
//
//  Created by stephen feggeler on 10/7/13.
//
//

#import "SFCalloutView.h"
#import <MapKit/MapKit.h>

@implementation SFCalloutView

- (id)initWithFrame:(CGRect)frame calloutPosition:(SFCalloutPosition)calloutPosition
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.calloutPosition = calloutPosition;
        self.arrowSize = 12;
        self.cornerRadius = 12.0;
        self.backgroundColor = [UIColor clearColor];
        CGRect frame = self.frame;
        if (calloutPosition == SFAbovePinCalloutPosition) {
             frame.origin.y  -= self.arrowSize;
        }
        frame.size.height += self.arrowSize;
        self.frame = frame;
    }
    return self;
}


- (void)touchesBeganHIDE:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    // Capturing touches on a subview outside the frame of its superview using hitTest:withEvent:
    UIView *contentView = self.subviews[0];
    if (!self.hidden && self.alpha > 0) {
        for (UIView *subview in contentView.subviews) { //reverseObjectEnumerator
            CGPoint subPoint = [subview convertPoint:point fromView:self];
            UIView *result = [subview hitTest:subPoint withEvent:event];
            if (result != nil) {
                return result;
                break;
            }
        }
    }
    
    // use this to pass the 'touch' onward in case no subviews trigger the touch
    return [super hitTest:point withEvent:event];
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    
    CGPathRef calloutPath = nil;
    if (self.calloutPosition == SFAbovePinCalloutPosition) {
        calloutPath = [self newCGPathAbovePinCallout];
    }
    else{
        calloutPath = [self newCGPathBelowPinCallout];
    }
    
    BOOL pathContainsPoint = CGPathContainsPoint(calloutPath, NULL, point, FALSE);
    
    CGPathRelease(calloutPath);
    
    return pathContainsPoint;
}

-(void)drawInContext:(CGContextRef)context
{
    CGContextSetLineWidth(context, 2.0);
    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    
    if (self.calloutPosition == SFAbovePinCalloutPosition) {
        [self getAbovePinDrawPath:context];
    }
    else{
        [self getBelowPinDrawPath:context];
    }
   
    CGContextFillPath(context);
    
    //    CGContextSetLineWidth(context, 1.0);
    //     CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
    //    [self getAbovePinDrawPath:context];
    //    CGContextStrokePath(context);
    
}

- (void)getBelowPinDrawPath:(CGContextRef)context
{
    CGRect rrect = self.bounds;
	CGFloat radius = self.cornerRadius;
    
	CGFloat minx = CGRectGetMinX(rrect),
    midx = CGRectGetMidX(rrect),
    maxx = CGRectGetMaxX(rrect);
    
	CGFloat miny = CGRectGetMinY(rrect)+self.arrowSize,
    maxy = CGRectGetMaxY(rrect);
    
    CGFloat arrow_x_offset = self.offset.x;
    
    CGContextMoveToPoint(context,    midx+self.arrowSize+arrow_x_offset, miny);
    CGContextAddLineToPoint(context, midx+arrow_x_offset,              miny-self.arrowSize);
    CGContextAddLineToPoint(context, midx-self.arrowSize+arrow_x_offset, miny);
    
    CGContextAddArcToPoint(context, minx, miny, minx, maxy, radius);
    CGContextAddArcToPoint(context, minx, maxy, maxx, maxy, radius);
    CGContextAddArcToPoint(context, maxx, maxy, maxx, miny, radius);
    CGContextAddArcToPoint(context, maxx, miny, midx, miny, radius);
    CGContextClosePath(context);
}

- (CGPathRef)newCGPathBelowPinCallout
{
    CGRect rrect = self.bounds;
	CGFloat radius = self.cornerRadius;
	CGFloat minx = CGRectGetMinX(rrect),
    midx = CGRectGetMidX(rrect),
    maxx = CGRectGetMaxX(rrect);
	CGFloat miny = CGRectGetMinY(rrect)+self.arrowSize,
    maxy = CGRectGetMaxY(rrect);
    CGFloat arrow_x_offset = self.offset.x;
    
    CGMutablePathRef p = CGPathCreateMutable();
    CGPathMoveToPoint( p, NULL, midx+self.arrowSize+arrow_x_offset, miny );
    CGPathAddLineToPoint( p, NULL, midx+arrow_x_offset, miny-self.arrowSize);
    CGPathAddLineToPoint(p, NULL, midx-self.arrowSize+arrow_x_offset, miny);
    CGPathAddArcToPoint(p, NULL, minx, miny, minx, maxy, radius);
    CGPathAddArcToPoint(p, NULL, minx, maxy, maxx, maxy, radius);
    CGPathAddArcToPoint(p, NULL, maxx, maxy, maxx, miny, radius);
    CGPathAddArcToPoint(p, NULL, maxx, miny, midx, miny, radius);
    CGPathCloseSubpath(p);
    
    return p;
}

- (CGPathRef)newCGPathAbovePinCallout
{
    CGRect rrect = self.bounds;
	CGFloat radius = self.cornerRadius;
	CGFloat minx = CGRectGetMinX(rrect),
    midx = CGRectGetMidX(rrect),
    maxx = CGRectGetMaxX(rrect);
	CGFloat miny = CGRectGetMinY(rrect),
    maxy = CGRectGetMaxY(rrect)-self.arrowSize;
    CGFloat arrow_x_offset = self.offset.x;
    
    CGMutablePathRef p = CGPathCreateMutable();
    CGPathMoveToPoint( p, NULL, midx+self.arrowSize+arrow_x_offset, maxy );
    CGPathAddLineToPoint( p, NULL, midx+arrow_x_offset, maxy+self.arrowSize);
    CGPathAddLineToPoint(p, NULL, midx-self.arrowSize+arrow_x_offset, maxy);
    CGPathAddArcToPoint(p, NULL, minx, maxy, minx, miny, radius);
    CGPathAddArcToPoint(p, NULL, minx, minx, maxx, miny, radius);
    CGPathAddArcToPoint(p, NULL, maxx, miny, maxx, maxx, radius);
    CGPathAddArcToPoint(p, NULL, maxx, maxy, midx, maxy, radius);
    CGPathCloseSubpath(p);
    
    return p;
}

- (void)getAbovePinDrawPath:(CGContextRef)context
{
    CGRect rrect = self.bounds;
	CGFloat radius = self.cornerRadius;
    
	CGFloat minx = CGRectGetMinX(rrect),
    midx = CGRectGetMidX(rrect),
    maxx = CGRectGetMaxX(rrect);
    
	CGFloat miny = CGRectGetMinY(rrect),
    // midy = CGRectGetMidY(rrect),
    maxy = CGRectGetMaxY(rrect)-self.arrowSize;
    
    CGFloat arrow_x_offset = self.offset.x;
    
    CGContextMoveToPoint(context,    midx+self.arrowSize+arrow_x_offset, maxy);
    CGContextAddLineToPoint(context, midx+arrow_x_offset,              maxy+self.arrowSize);
    CGContextAddLineToPoint(context, midx-self.arrowSize+arrow_x_offset, maxy);
    
    CGContextAddArcToPoint(context, minx, maxy, minx, miny, radius);
    CGContextAddArcToPoint(context, minx, minx, maxx, miny, radius);
    CGContextAddArcToPoint(context, maxx, miny, maxx, maxx, radius);
    CGContextAddArcToPoint(context, maxx, maxy, midx, maxy, radius);
    CGContextClosePath(context);
}

- (void)drawRect:(CGRect)rect
{
//	[self drawInContext:UIGraphicsGetCurrentContext()];
    
    CGPathRef calloutPath = nil;
    if (self.calloutPosition == SFAbovePinCalloutPosition) {
        calloutPath = [self newCGPathAbovePinCallout];
    }
    else{
        calloutPath = [self newCGPathBelowPinCallout];
    }
    
    CAShapeLayer *calloutShape = [CAShapeLayer layer];
    calloutShape.path = calloutPath;
    calloutShape.fillColor = [UIColor whiteColor].CGColor;
    
    self.layer.mask = calloutShape;
    self.layer.masksToBounds = YES;
    self.layer.backgroundColor = [UIColor whiteColor].CGColor;
    self.layer.opacity = 0.97;
    
    // shadow
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowOpacity = 1.0;
    self.layer.shadowOffset = CGSizeMake(0.0f, 0.0f); // -5,5
    self.layer.shadowRadius = 2.0;
    
    CGPathRelease(calloutPath);
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
