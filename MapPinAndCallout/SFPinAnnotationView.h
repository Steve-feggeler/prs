//
//  CustomAnnotationView.h
//  CustomAnnotation
//
//  Created by akshay on 8/17/12.
//  Copyright (c) 2012 raw engineering, inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

#define CLCOORDINATES_EQUAL( coord1, coord2 ) ((coord1.latitude == coord2.latitude && coord1.longitude == coord2.longitude))
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


typedef NS_OPTIONS(NSUInteger, SFCalloutPosition) {
    SFAbovePinCalloutPosition           = 0,
    SFBelowPinCalloutPosition           = 1 <<  0,
    SFAvoveOrBelowCalloutPosition       = 1 <<  1   // positions callout to minimise map moving, above or below
};

@interface SFPinAnnotationView : MKAnnotationView
<UIScrollViewDelegate>

@property (assign, nonatomic) MKMapView *map;
@property (assign, nonatomic) SFCalloutPosition calloutPosition;
@property (assign, nonatomic) CGFloat calloutArrowSize;
@property (assign, atomic) CGFloat extraPinSideDistance;
@property (assign, atomic) CGFloat extraCalloutSideDistance;
@property (assign, atomic) CGFloat xAdjustForCalloutPointer;
@property (assign, atomic) CGFloat yAdjustForAboveCallout;
@property (assign, atomic) CGFloat yAdjustForBelowCallout;
@property (retain, atomic) UILabel *label;

- (void)setText:(NSString*)text;
- (void)setSelected:(BOOL)selected animated:(BOOL)animated;
- (void)animateCalloutAppearance;
- (UIView*)calloutContentView;
- (void)doneTouching;
- (void)removeCallout;

@end