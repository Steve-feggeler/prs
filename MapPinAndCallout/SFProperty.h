//
//  SFProperty.h
//  CustomAnnotation
//
//  Created by stephen feggeler on 11/4/13.
//
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface SFProperty : NSObject

//@property (strong, nonatomic) NSString * description;
@property (strong, nonatomic) NSString * price;
@property (strong, nonatomic) NSString * neighborhood;
@property (strong, nonatomic) NSString * addressStreet;
@property (strong, nonatomic) NSString * addressCity;
@property (strong, nonatomic) NSString * picture;
@property (assign, nonatomic) CLLocationCoordinate2D coordinate;

@end
