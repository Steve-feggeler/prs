//
//  CalloutContainer.h
//  CustomAnnotation
//
//  Created by stephen feggeler on 10/7/13.
//
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "SFPinAnnotationView.h"

@interface SFCalloutView : UIView

- (id)initWithFrame:(CGRect)frame calloutPosition:(SFCalloutPosition)calloutPosition;

@property (assign, nonatomic) CGPoint offset;
@property (assign, nonatomic) SFCalloutPosition calloutPosition;
@property (retain, nonatomic) UIColor *calloutColor;
@property (assign, nonatomic) CGFloat arrowSize;
@property (assign, nonatomic) CGFloat cornerRadius;

@end
