//
//  SFPropertiesPinView.m
//  CustomAnnotation
//
//  Created by stephen feggeler on 11/4/13.
//
//

#import <objc/runtime.h>
#import "UIImageView+WebCache.h"
#import "SFPropertiesPinView.h"
#import "SFCalloutView.h"
#import "Annotation.h"
#import "SFColorButton.h"
#import <QuartzCore/QuartzCore.h>

static NSInteger SCROLLVIEW_TAG = 111;
static NSInteger PAGE_CONTROL_TAG = 222;

@interface SFPropertiesPinView()

@property (retain, nonatomic) UIScrollView *scrollView;
@property (retain, nonatomic) UIPageControl *pageControl;

@end


@implementation SFPropertiesPinView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)updateCallout{
    
    UIScrollView * old_sv = (UIScrollView*)[self viewWithTag:SCROLLVIEW_TAG];
    
    // if can't find scrollview, nothing to update
    if (!old_sv) {
        return;
    }
    
    CGRect sv_frame = old_sv.frame;
    
    // use scrollView to display multiple properties
    NSMutableArray * properties = ((Annotation*)self.annotation).properties;
    NSInteger pageCount = properties.count;
    if (pageCount>1){
        
        // multiple property views
        UIScrollView * new_sv = [self ScrollViewInFrame:sv_frame withPageCount:pageCount];
        new_sv.tag = SCROLLVIEW_TAG;
        new_sv.layer.zPosition = 10;
        self.scrollView = new_sv;
        
        // add pages to scrollview
        for(int i = 0; i< pageCount; i++) {
            CGFloat x = i * new_sv.frame.size.width;
            CGRect page_frame = CGRectMake(x, 0, sv_frame.size.width, sv_frame.size.height );
            UIView * pageView = [self propertyViewInFrame:page_frame  withProperty:properties[i]];
            [new_sv addSubview:pageView];
        }
        
        // switch old_sv with new_sv
        UIView * sv_parent = old_sv.superview;
        [old_sv removeFromSuperview];
        [sv_parent addSubview:new_sv];
                
        // update page control
        UIPageControl * old_pc = (UIPageControl*)[self viewWithTag:PAGE_CONTROL_TAG];
        CGRect pc_frame = old_pc.frame;
        
        // new page control
        UIPageControl * new_pc = [self pageControlInFrame:pc_frame withPageCount:pageCount];
        new_pc.tag = PAGE_CONTROL_TAG;
        
        // switch old and new page control
        UIView * pc_parent = old_pc.superview;
        self.pageControl = nil;
        [old_pc removeFromSuperview];
        
        [pc_parent addSubview:new_pc];
        self.pageControl = new_pc;
        
    }
}

-(void)updatePrice
{
    NSMutableArray * properties = ((Annotation*)self.annotation).properties;
    
    // find the lowest price
    CGFloat min_price = CGFLOAT_MAX;
    NSString * min_price_string = @"";
    for (Property *p in properties) {
        NSString *pureNumbers = [[[p price] componentsSeparatedByCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]] componentsJoinedByString:@""];
        CGFloat price = (CGFloat)[pureNumbers floatValue];
        if(price <= min_price){
            min_price = price;
            min_price_string = [p priceMini];
        }
    }
    
    // single property, set price as text
    if (properties.count <= 1) {
        NSString *cost = [((Property*)properties[0]) priceMini];
        [self setText:cost];
    }
    
    // multiple properties at address
    else{
        int cnt = (int)properties.count;
        NSString *priceLabel = [NSString stringWithFormat:@"≥ %@ [%d]", min_price_string, cnt];
        [self setText:priceLabel];
        [self updateCallout];
    }
}

- (void)mapView:(MKMapView*)mapView selectedProperty:(Property*)property{
    NSMutableArray * properties = ((Annotation*)self.annotation).properties;
    NSInteger pageCount = properties.count;
    if (pageCount>1){
        NSInteger index = [properties indexOfObject:property];
        // scroll to property page
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * index;
        frame.origin.y = 0;
        [self.scrollView scrollRectToVisible:frame animated:YES];
    }
}

- (NSInteger)pinsAtThisLocation:(CLLocationCoordinate2D)thisLocation{
    NSInteger pins_with_this_location = 0;
    for (id<MKAnnotation> annotation in self.map.annotations){
        CLLocationCoordinate2D loc = ((Annotation*)annotation).coordinate;
        if(CLCOORDINATES_EQUAL(loc, thisLocation)){
            ++pins_with_this_location;
        }
    }
    return pins_with_this_location;
}

-(void)sortProperties{
    NSMutableArray * properties = ((Annotation*)self.annotation).properties;
    
    [properties sortUsingComparator:
     ^NSComparisonResult(id obj1, id obj2){
         
         Property *p1 = (Property*)obj1;
         Property *p2 = (Property*)obj2;
         NSInteger n1 = [p1 priceNumber];
         NSInteger n2 = [p2 priceNumber];
         if (n1 > n2) {
             return (NSComparisonResult)NSOrderedAscending;
         }
         
         if (n1 < n2) {
             return (NSComparisonResult)NSOrderedDescending;
         }
         
         NSInteger mln1 = [p1.mln integerValue];
         NSInteger mln2 = [p2.mln integerValue];
         
         // if here price is same so use mln
         
         if (mln1 > mln2) {
             return (NSComparisonResult)NSOrderedAscending;
         }
         
         if (mln1 < mln2) {
             return (NSComparisonResult)NSOrderedDescending;
         }
         
         return (NSComparisonResult)NSOrderedSame;
     }
     ];
}

- (UIView*)calloutContentView
{
    CGRect contentFrame = CGRectMake(0, 0, 300, 130);
    UIView *contentBox = [[[UIView alloc] initWithFrame:contentFrame] autorelease];
    
    // use scrollView to display multiple properties
    NSMutableArray * properties = ((Annotation*)self.annotation).properties;
    NSInteger pageCount = properties.count;
    if (pageCount>1){
        
        [self sortProperties];
        
        // multiple property views
        CGRect scrollFrame = CGRectMake(40, -15, contentBox.frame.size.width - 40, contentBox.frame.size.height+15 );
        UIScrollView * scrollView = [self ScrollViewInFrame:scrollFrame withPageCount:pageCount];
        scrollView.tag = SCROLLVIEW_TAG;
        self.scrollView = scrollView;
        
        // add pages to scrollview
        for(int i = 0; i< pageCount; i++) {
            CGFloat x = i * scrollView.frame.size.width;
            CGRect pageFrame = CGRectMake(x, 0, scrollView.frame.size.width, scrollView.frame.size.height );
            UIView * pageView = [self propertyViewInFrame:pageFrame  withProperty:properties[i]];
            [scrollView addSubview:pageView];
        }
        
        // add scrollview
        [contentBox addSubview:scrollView];
        
        // add page control
        CGFloat height_for_page_control = 15;
        CGFloat y_for_page_control = scrollFrame.origin.y + scrollFrame.size.height;
        CGRect pageControlFrame = CGRectMake(40, y_for_page_control - 15, scrollView.frame.size.width, height_for_page_control + 30);
        UIPageControl * pageControl = [self pageControlInFrame:pageControlFrame withPageCount:pageCount];
        pageControl.tag = PAGE_CONTROL_TAG;
        self.pageControl = pageControl;
        [contentBox addSubview:pageControl];
        scrollView.layer.zPosition = 10;
        
        // make content frame height larger so page control can fit
        contentFrame.size.height += height_for_page_control;
        contentBox.frame = contentFrame;
    }
    else{
        // one property view
        CGRect propertyFrame = CGRectMake(40, -15, contentBox.frame.size.width - 40, contentBox.frame.size.height + 30 );
        UIView * propertyView = [self propertyViewInFrame:propertyFrame withProperty:properties[0]];
        [contentBox addSubview:propertyView];
    }
    
    // car button
    SFColorButton *carButton = [SFColorButton buttonWithType:UIButtonTypeCustom];
    [carButton addTarget:self action:@selector(showDirectionsToProperty) forControlEvents:UIControlEventTouchUpInside];
    [[carButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [carButton setImage:[UIImage imageNamed:@"car_44x44_white_1.png"] forState: UIControlStateNormal];
    [carButton setImage:[UIImage imageNamed:@"car_44x44_white_1.png"] forState: UIControlStateHighlighted];
    //button.frame = CGRectMake(0, 0, 40, contentBox.frame.size.height);
    carButton.frame = CGRectMake(0, -15, 40, contentBox.frame.size.height/2 + 15);
    carButton.imageEdgeInsets = UIEdgeInsetsMake(15, 0, 0, 0);
    //carButton.mainColor = [UIColor colorWithRed:18/256.0 green:73/256.0 blue:210/256.0 alpha:1.0];
    carButton.mainColor = [UIColor colorWithRed:59/256.0 green:63/256.0 blue:133/256.0 alpha:1.0];
    [contentBox addSubview:carButton];
    
    // pegman button
    SFColorButton *pegman = [SFColorButton buttonWithType:UIButtonTypeCustom];
    [pegman addTarget:self action:@selector(showStreetViewForProperty) forControlEvents:UIControlEventTouchUpInside];
    [[pegman imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [pegman setImage:[UIImage imageNamed:@"pegman_44X44_white.png"] forState: UIControlStateNormal];
    [pegman setImage:[UIImage imageNamed:@"pegman_44X44_white.png"] forState: UIControlStateHighlighted];
    pegman.frame = CGRectMake(0, contentBox.frame.size.height/2, 40, contentBox.frame.size.height/2 + 15);
    pegman.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 25, 0);
    pegman.mainColor = [UIColor colorWithRed:59/256.0 green:63/256.0 blue:133/256.0 alpha:1.0];
       
    [contentBox addSubview:pegman];
    
    return contentBox;
}

- (UIPageControl*)pageControlInFrame:(CGRect)frame withPageCount:(NSInteger)pageCount{
    UIColor *color1 = [[[UIColor alloc] initWithRed:20/255.f green:156/255.f blue:255/255.f alpha:0.4] autorelease];
    UIColor *color2 = [[[UIColor alloc] initWithRed:20/255.f green:156/255.f blue:255/255.f alpha:1] autorelease];
    
    UIPageControl * pc = [[[UIPageControl alloc] initWithFrame:frame] autorelease];
    pc.currentPage = 0;
    pc.numberOfPages = pageCount;
    pc.pageIndicatorTintColor = color1;
    pc.currentPageIndicatorTintColor = color2;
    pc.backgroundColor = [[[UIColor alloc] initWithRed:34/255.f green:68/255.f blue:98/255.f alpha:1] autorelease];
    
    [pc addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventValueChanged];
    
    return pc;
}

- (Property*)getCurrentProperty
{
    Property * property = nil;
    NSMutableArray * properties = ((Annotation*)self.annotation).properties;
    
    if(properties.count==1){
        property = properties[0];
    }
    else if(properties.count>1){
        NSInteger currentPage = self.pageControl.currentPage;
        property = properties[currentPage];
    }
    return property;
}

- (void)showPropertyDetails{
    Property * property = [self getCurrentProperty];
    [self.delegate mapView:self.map detailsForProperty:property];
    [self doneTouching];
}

- (void)showStreetViewForProperty{
    Property * property = [self getCurrentProperty];
    [self.delegate mapView:self.map streetViewForProperty:property];
    [self doneTouching];
}

- (void)showDirectionsToProperty{
    Property * property = [self getCurrentProperty];
    [self.delegate mapView:self.map directionsToProperty:property];
    [self doneTouching];
}

- (UIView*)propertyViewInFrame:(CGRect)frame withProperty:(Property*)property
{
    CGFloat y_offset = 15;
    
    // property view on right side of callout
    SFColorButton * propertyView = [SFColorButton buttonWithType:UIButtonTypeCustom];
    [propertyView addTarget:self action:@selector(showPropertyDetails) forControlEvents:UIControlEventTouchUpInside];
    propertyView.frame = frame;
    propertyView.mainColor = [UIColor whiteColor];
    
    // title
    UILabel * title = [[[UILabel alloc] init] autorelease];
    [title setFrame:CGRectMake(10, 10 + y_offset, 240, 20)];
    title.text = [property detials1];
    //title.font = [UIFont fontWithName:@"HelveticaNeue" size:15];
    title.font = [UIFont boldSystemFontOfSize:16];
    title.textColor = [UIColor darkGrayColor];
    title.backgroundColor = [UIColor clearColor];
    [propertyView addSubview:title];
    
    // price
    UILabel * price = [[[UILabel alloc] init] autorelease];
    [price setFrame:CGRectMake(120, 35 + y_offset, 200, 20)];
    price.text = [property price];
    price.font = [UIFont boldSystemFontOfSize:15];
    price.textColor = [UIColor blackColor];
    price.backgroundColor = [UIColor clearColor];
    [propertyView addSubview:price];
    
    // neighborhood
    UILabel * hood = [[[UILabel alloc] init] autorelease];
    [hood setFrame:CGRectMake(120, 55 + y_offset, 140, 20)];
    hood.text = [property neighborhood];
    hood.font = [UIFont boldSystemFontOfSize:13];
    hood.textColor = [UIColor darkGrayColor];
    hood.backgroundColor = [UIColor clearColor];
    [propertyView addSubview:hood];
    
    // street address
    UILabel * address1 = [[[UILabel alloc] init] autorelease];
    [address1 setFrame:CGRectMake(120, 70 + y_offset, 140, 20)];
    address1.text = [property address1];
    address1.font = [UIFont boldSystemFontOfSize:13];
    address1.textColor = [UIColor darkGrayColor];
    address1.backgroundColor = [UIColor clearColor];
    [propertyView addSubview:address1];
    
    // city address
    UILabel * address2 = [[[UILabel alloc] init] autorelease];
    [address2 setFrame:CGRectMake(120, 85 + y_offset, 140, 20)];
    //address2.text = [property address2];
    address2.text = [NSString stringWithFormat:@"%@, %@", [property city], [property zip]];
    address2.font = [UIFont boldSystemFontOfSize:13];
    address2.textColor = [UIColor darkGrayColor];
    address2.backgroundColor = [UIColor clearColor];
    [propertyView addSubview:address2];
    
    // mls and listing office
    UILabel * mls = [[[UILabel alloc] init] autorelease];
    [mls setFrame:CGRectMake(10, 110 + y_offset, 240, 20)];
    mls.text = [NSString stringWithFormat:@"San Francisco MLS, %@", [property listingOfficeName]];
    mls.font = [UIFont systemFontOfSize:8];
    mls.textColor = [UIColor colorWithWhite: 0.2 alpha:1];
    mls.backgroundColor = [UIColor clearColor];
    [propertyView addSubview:mls];
    
    // picture
    UIImageView *imageView = [[[UIImageView alloc] init] autorelease];
    
    NSString *path = [property mainThumbImage];

   // [imageView setImageWithURL:[NSURL URLWithString:path]
   //           placeholderImage:[UIImage imageNamed:@"placeholder"]];
    
    [imageView setImageWithURL:[NSURL URLWithString:path]
     success:^(UIImage *image, BOOL cached)
     {
         
     }
     failure:^(NSError *error)
     {
         imageView.image = [UIImage imageNamed:@"No-Image-available.png"];
     }];

    
   // imageView.backgroundColor = [UIColor colorWithRed:234/255.0 green:234/255.0 blue:234/255.0 alpha:1.0];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    CGRect pic_frame = imageView.frame;
    pic_frame.origin.x = 10;
    pic_frame.origin.y = 35 + y_offset;
    pic_frame.size.width = 100;
    pic_frame.size.height = 75;
    imageView.frame = pic_frame;
    [propertyView addSubview:imageView];
    
    return propertyView;
}

-(UIScrollView*)ScrollViewInFrame:(CGRect)frame withPageCount:(NSInteger)pageCount{
    
    NSInteger viewcount= pageCount;
    UIScrollView * sv = [[UIScrollView alloc] init];
    sv.frame = frame;
    sv.contentSize = CGSizeMake(viewcount * sv.frame.size.width, sv.frame.size.height);
    sv.backgroundColor = [UIColor whiteColor];
    sv.showsVerticalScrollIndicator = YES;
    sv.showsHorizontalScrollIndicator = YES;
    sv.scrollEnabled=YES;
    sv.userInteractionEnabled=YES;
    sv.scrollEnabled = YES;
    sv.exclusiveTouch = NO;
    sv.pagingEnabled = YES;
    sv.bounces = YES;
    sv.delegate = self;
    
    return sv;
}

BOOL static pageControllingScroll = NO;

- (IBAction)changePage:(id)sender {
    CGFloat x = self.pageControl.currentPage * self.scrollView.frame.size.width;
    [self.scrollView setContentOffset:CGPointMake(x, 0) animated:YES];
    pageControllingScroll = YES;
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    pageControllingScroll = NO;
}

-(void) scrollViewDidScroll:(UIScrollView *)scrollView  {
    CGFloat pageWidth = self.scrollView.frame.size.width;
    float fractionalPage = self.scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    if(!pageControllingScroll)
        self.pageControl.currentPage = page;
}




/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
