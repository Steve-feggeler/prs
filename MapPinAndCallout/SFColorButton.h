//
//  SFColorButton.h
//  CustomAnnotation
//
//  Created by stephen feggeler on 10/27/13.
//
//

#import <UIKit/UIKit.h>

@interface SFColorButton : UIButton

@property (retain, atomic) UIColor * mainColor;

@end
