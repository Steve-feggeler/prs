//
//  SliderCell.m
//  R1
//
//  Created by stephen feggeler on 1/12/13.
//  Copyright 2013 self. All rights reserved.
//

#import "SliderCell.h"


@implementation SliderCell

@synthesize slider;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	
    if (self) {
        // Initialization code.
				
		slider =  [[[UISlider alloc] initWithFrame:CGRectMake(65,15,170,25)] autorelease];		
		slider.maximumValue=10;
		slider.minimumValue=0; 
		
		UILabel *leftLabel = [[[UILabel alloc] initWithFrame:CGRectMake(15,15,50,25)] autorelease];	
		UILabel *rightLabel = [[[UILabel alloc] initWithFrame:CGRectMake(235, 15, 50, 25)] autorelease];
		rightLabel.textAlignment = NSTextAlignmentRight;
		//rightLabel.backgroundColor = [UIColor grayColor];

		
		leftLabel.text = @"beds";
		rightLabel.text = @"0+";
				
		[self.contentView addSubview:slider];
		[self.contentView addSubview:leftLabel];
		[self.contentView addSubview:rightLabel];
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state.
}


- (void)dealloc {
    [super dealloc];
}


@end
