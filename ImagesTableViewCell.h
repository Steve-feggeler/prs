//
//  ImagesTableViewCell.h
//  R2
//
//  Created by stephen feggeler on 3/16/13.
//  Copyright 2013 self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScrollingImageDelegate.h"


@interface ImagesTableViewCell : UITableViewCell {

}

@property (nonatomic, retain) IBOutlet UILabel *imageLabel;
@property (nonatomic, retain) IBOutlet UIScrollView *imageScroller;
@property (nonatomic, retain) ScrollingImageDelegate *scrollDelegate1;
@property (nonatomic, retain) NSArray *_objects;
@property (nonatomic, retain) NSArray *imagePaths;

- (void)loadImages;
- (void)setupScrollView1;

@end
