//
//  SliderCell.h
//  R1
//
//  Created by stephen feggeler on 1/12/13.
//  Copyright 2013 self. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SliderCell : UITableViewCell {
	
	UISlider *slider;	 // slider on the left side of the cell
}

@property (nonatomic, retain) UISlider *slider;

@end
