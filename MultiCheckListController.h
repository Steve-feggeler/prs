//
//  MultiCheckListController.h
//  R1
//
//  Created by stephen feggeler on 2/11/13.
//  Copyright 2013 self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckListItem.h"
#import "ListItem.h"
#import "ItemCell.h"

extern BOOL checkmatch(NSString *item, NSString *matchphrase);

@protocol MultiCheckListControllerDelegate; 

@interface MultiCheckListController : UIViewController
<
    UITableViewDataSource,
    UISearchDisplayDelegate,
    UISearchBarDelegate
>
{	}

@property (nonatomic, assign) id<MultiCheckListControllerDelegate> delegate;
@property (nonatomic, retain) NSMutableArray *checkLists;
@property (nonatomic, assign) IBOutlet UITableView *table;
@property (nonatomic, assign) NSInteger selectedList;

- (void)done;
- (void)reset;
- (void)createAlphabetArray;

@end
	
// notifies ViewController that Done Button was touched
@protocol MultiCheckListControllerDelegate
						   
- (void) MultiCheckListController:(MultiCheckListController *)controller doneWithCheckList:(NSMutableArray*)list selectedListIndex:(NSInteger)index;
						   
@end // end protocol AddViewControllerDelegate
						   
						   
						   
						   
						   
