//
//  DataController.m
//  R1
//
//  Created by stephen feggeler on 1/29/13.
//  Copyright 2013 self. All rights reserved.
//

#import "DataController.h"


@implementation DataController

@synthesize beds;
@synthesize baths;
@synthesize dom;
@synthesize structureSize;
@synthesize propertyTypes;
@synthesize price;
@synthesize resultsPerPage;
@synthesize zipcodes;
@synthesize neighborhoods;
@synthesize area;
@synthesize selectedLocationList;
@synthesize openHousesOnly;
@synthesize shortSaleOnly;
@synthesize reoSaleOnly;
@synthesize priceReducedOnly;

//@"price=Any&dom=Any&pageSize=50&sqfeet=Any&beds=Any&baths=Any&property_subtype=RESI&county=San+Francisco&city=Any&area=4-A+Balboa+Terrace&currentPage=0&lastPage=-1"

// price=Any&dom=Any&pageSize=10&sqfeet=Any&beds=Any&baths=Any&property_subtype=RESI&county=San+Francisco&city=Any&area=Any&currentPage=0&lastPage=23&page=2

// price=Any&dom=Any&pageSize=10&sqfeet=Any&beds=Any&baths=Any&property_subtype=RESI&county=San+Francisco&city=Any&area=Any&currentPage=0

// &lat_center=37.751851&lon_center=-122.427177&lat_delta=0.01&lon_delta=0.01

- (NSString*)getPostCMDForPage:(NSInteger)page
{
	//NSString *dom = [[NSString alloc] initWithString:@"Any"];
		
	NSMutableString *cmd = [[NSMutableString alloc] initWithString:@""];
	
	NSString *priceString = [[DataController pricesArray] objectAtIndex:price];	
		
	NSString *encodedPrice = (NSString *)CFURLCreateStringByAddingPercentEscapes(
							     NULL,
								(CFStringRef)priceString,
								 NULL,
								(CFStringRef)@"!*'();:@&=+$,/?%#[]",
								kCFStringEncodingUTF8 );
	
	[cmd appendFormat:@"price=%@&", encodedPrice];
	[cmd appendFormat:@"dom=%@&",[[DataController domArray] objectAtIndex:dom]];
//	[cmd appendFormat:@"pageSize=%@&",[[DataController resultsPerPageArray] objectAtIndex:resultsPerPage]];
	[cmd appendFormat:@"pageSize=%@&",@"50"];
	[cmd appendFormat:@"sqfeet=%@&",[[DataController structureSizeArray] objectAtIndex:structureSize]];
	[cmd appendFormat:@"beds=%@&",[[DataController bedsArray] objectAtIndex:beds]];
	[cmd appendFormat:@"baths=%@&",[[DataController bathsArray] objectAtIndex:baths]];
    
    // only send openHousesOnly if yes
    NSString * yesOrNo = @"NO";
    if (self.openHousesOnly) {
        yesOrNo = @"YES";
        [cmd appendFormat:@"openHousesOnly=%@&", yesOrNo];
    }
    
    // only send shortSaleOnly if yes
    if (self.shortSaleOnly) {
        yesOrNo = @"YES";
        [cmd appendFormat:@"shortSaleOnly=%@&", yesOrNo];
    }
    
    // only send reoSaleOnly if yes
    if (self.reoSaleOnly) {
        yesOrNo = @"YES";
        [cmd appendFormat:@"reoSaleOnly=%@&", yesOrNo];
    }
    
    // only send priceReducedOnly if yes
    if (self.priceReducedOnly) {
        yesOrNo = @"YES";
        [cmd appendFormat:@"priceReducedOnly=%@&", yesOrNo];
    }
	
	[cmd appendString:[self CMDFromListItemArray:propertyTypes named:@"property_subtype"]];
	
	switch (selectedLocationList) {
		case 0:
			[cmd appendString:[self CMDFromListItemArray:neighborhoods named:@"area"]];
			break;
			
		case 1:
			[cmd appendString:[self CMDFromListItemArray:zipcodes named:@"area"]];
			break;
			
		case 2:
			[cmd appendString:[self CMDFromListItemArray:area named:@"area"]];
			break;
			
		default:
			break;
	}	
							
	[cmd appendString:@"county=San+Francisco&"];
	[cmd appendString:@"city=Any&"];
	[cmd appendString:[NSString stringWithFormat:@"page=%li", (long)page]];
	
	NSString *result = [[cmd copy] autorelease];
	
	[cmd release];
	
	result = [result stringByReplacingOccurrencesOfString:@" "
											   withString:@"+"];
	
	[encodedPrice release];
	
	NSLog(@"%@", result);
	
	return result;	
}

- (NSString*)getPostCMDForPage:(NSInteger)page region:(MKCoordinateRegion)region
{
	NSString *cmd = [self getPostCMDForPage:page];
	
	NSString *regionCmd = [NSString stringWithFormat:@"%@&lat_center=%f&lon_center=%f&lat_delta=%f&lon_delta=%f", 
						   cmd, 
						   region.center.latitude, 
						   region.center.longitude,
						   region.span.latitudeDelta,
						   region.span.longitudeDelta];	
	
	return regionCmd;
}


- (NSString*)CMDFromListItemArray:(NSMutableArray*)items named:(NSString*)name
{
	NSMutableString *cmd = [[[NSMutableString alloc] initWithString:@""] autorelease];	
	NSInteger count = [items count];
	
	for (NSInteger i=0; i<count; ++i) {
		
		ListItem *item = [items objectAtIndex:i];
		
		if (item.isSelected == YES)
			[cmd appendFormat:@"%@=%@&", name, item.key];						
	}
	
	return [[cmd copy] autorelease];
}

- (NSString*)selectedPropertyTypes
{
	NSMutableString *ms = [[NSMutableString alloc] initWithString:@""];
		
	int i = 0;
	for (ListItem *item in propertyTypes) {
		
		if (item.isSelected) {
			
			if (i>0) {
				[ms appendString:@", "];
			}
			++i;
			
			[ms appendString:item.name];
		}
		
	}
	
	NSString *selected = [[ms copy] autorelease];
	
	[ms release];
	
	return selected;
}

- (NSString*)selectedLocations
{
	NSMutableString *ms = [[NSMutableString alloc] initWithString:@""];	
	NSMutableArray *ma = nil;		
	
	switch (selectedLocationList) {
		case 0:
			ma = neighborhoods;
			break;
		case 1:
			ma = zipcodes;
			break;
		case 2:
			ma = area;
			break;
			
		default:
			break;
	}
	
	
	int i=0;
	for (ListItem *item in ma) {
		
		if (item.isSelected) {
			
			if (i>0) {
				[ms appendString:@", "];
			}
			++i;
			
			[ms appendString:item.name];
		}
	}
	
	NSString *locations = [[ms copy] autorelease];
	
	[ms release];
	
	return locations;		
}

- (id)init
{
    self = [super init];
    if (!self) {
        return self;
    }
    
	price = [[DataController pricesArray] count] - 1;
	beds = 0;
	baths = 0;
	dom = [[DataController domArray] count] - 1;
	resultsPerPage = 0;
	structureSize = 0;
	selectedLocationList = 0;
    openHousesOnly = NO;
    shortSaleOnly = NO;
    reoSaleOnly = NO;
    priceReducedOnly = NO;
	
	// property types
	self.propertyTypes = [[[NSMutableArray alloc] init] autorelease];
    
	[self MutableArray:self.propertyTypes addItemName:@"Any" withKey:@"Any" isSelected:NO ];
	[self MutableArray:self.propertyTypes addItemName:@"Business Op" withKey:@"BUSO" isSelected:NO ];
	[self MutableArray:self.propertyTypes addItemName:@"Commercial For Sale" withKey:@"COMI" isSelected:NO];
	[self MutableArray:self.propertyTypes addItemName:@"Condo/Coop/TIC/Loft" withKey:@"COND" isSelected:NO];
	[self MutableArray:self.propertyTypes addItemName:@"Lots and Acreage" withKey:@"LOTL" isSelected:NO];
	[self MutableArray:self.propertyTypes addItemName:@"2-4 Units" withKey:@"MFM2" isSelected:NO];
	[self MutableArray:self.propertyTypes addItemName:@"5+ Units" withKey:@"MFM5" isSelected:NO];
	[self MutableArray:self.propertyTypes addItemName:@"Single-Family Homes" withKey:@"RESI" isSelected:YES];
	
	//zipcodes
	self.zipcodes = [[[NSMutableArray alloc] init] autorelease];
	[self MutableArray:self.zipcodes addItemName:@"Any" withKey:@"Any" isSelected:YES ];
    [self MutableArray:self.zipcodes addItemName:@"94102" withKey:@"94102" isSelected:NO ];
	[self MutableArray:self.zipcodes addItemName:@"94103" withKey:@"94103" isSelected:NO ];
	[self MutableArray:self.zipcodes addItemName:@"94104" withKey:@"94104" isSelected:NO ];
	[self MutableArray:self.zipcodes addItemName:@"94105" withKey:@"94105" isSelected:NO ];
	[self MutableArray:self.zipcodes addItemName:@"94107" withKey:@"94107" isSelected:NO ];
	[self MutableArray:self.zipcodes addItemName:@"94108" withKey:@"94108" isSelected:NO ];
	[self MutableArray:self.zipcodes addItemName:@"94109" withKey:@"94109" isSelected:NO ];
	[self MutableArray:self.zipcodes addItemName:@"94110" withKey:@"94110" isSelected:NO ];
	[self MutableArray:self.zipcodes addItemName:@"94111" withKey:@"94111" isSelected:NO ];
	[self MutableArray:self.zipcodes addItemName:@"94112" withKey:@"94112" isSelected:NO ];
	[self MutableArray:self.zipcodes addItemName:@"94114" withKey:@"94114" isSelected:NO ];
	[self MutableArray:self.zipcodes addItemName:@"94115" withKey:@"94115" isSelected:NO ];
	[self MutableArray:self.zipcodes addItemName:@"94116" withKey:@"94116" isSelected:NO ];
	[self MutableArray:self.zipcodes addItemName:@"94117" withKey:@"94117" isSelected:NO ];
	[self MutableArray:self.zipcodes addItemName:@"94118" withKey:@"94118" isSelected:NO ];	
	[self MutableArray:self.zipcodes addItemName:@"94119" withKey:@"94119" isSelected:NO ];
	[self MutableArray:self.zipcodes addItemName:@"94121" withKey:@"94121" isSelected:NO ];
	[self MutableArray:self.zipcodes addItemName:@"94122" withKey:@"94122" isSelected:NO ];
	[self MutableArray:self.zipcodes addItemName:@"94123" withKey:@"94123" isSelected:NO ];
	[self MutableArray:self.zipcodes addItemName:@"94124" withKey:@"94124" isSelected:NO ];
	[self MutableArray:self.zipcodes addItemName:@"94127" withKey:@"94127" isSelected:NO ];
    [self MutableArray:self.zipcodes addItemName:@"94129" withKey:@"94129" isSelected:NO ];
    [self MutableArray:self.zipcodes addItemName:@"94130" withKey:@"94130" isSelected:NO ];
	[self MutableArray:self.zipcodes addItemName:@"94131" withKey:@"94131" isSelected:NO ];
	[self MutableArray:self.zipcodes addItemName:@"94132" withKey:@"94132" isSelected:NO ];
	[self MutableArray:self.zipcodes addItemName:@"94133" withKey:@"94133" isSelected:NO ];
	[self MutableArray:self.zipcodes addItemName:@"94134" withKey:@"94134" isSelected:NO ];
	[self MutableArray:self.zipcodes addItemName:@"94158" withKey:@"94158" isSelected:NO ];
	
	// neighborhoods
	self.neighborhoods = [[[NSMutableArray alloc] init] autorelease];
	[self MutableArray:self.neighborhoods addItemName:@"Any" withKey:@"Any" isSelected:YES ];
	
	[self MutableArray:self.neighborhoods addItemName:@"Alamo Square" withKey:@"6-E Alamo Square" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Anza Vista" withKey:@"6-A Anza Vista" isSelected:NO ];	
	[self MutableArray:self.neighborhoods addItemName:@"Balboa Terrace" withKey:@"4-A Balboa Terrace" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Bayview" withKey:@"10-A Bayview" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Bayview Heights" withKey:@"10-K Bayview Heights" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Bernal Heights" withKey:@"9-A Bernal Heights" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Buena Vista/Ashbury Heights" withKey:@"5-F Buena Vista/Ashbury Heights" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Candlestick Point" withKey:@"10-M Candlestick Point" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Central Richmond" withKey:@"1-A Central Richmond" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Central Sunset" withKey:@"2-E Central Sunset" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Central Waterfront/Dogpatch" withKey:@"9-J Central Waterfront/Dogpatch" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Clarendon Heights" withKey:@"5-H Clarendon Heights" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Corona Height" withKey:@"5-G Corona Height" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Cow Hollow" withKey:@"7-D Cow Hollow" isSelected:NO ];
	
	[self MutableArray:self.neighborhoods addItemName:@"Crocker Amazon" withKey:@"10-B Crocker Amazon" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Diamond Heights" withKey:@"4-B Diamond Heights" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Downtown" withKey:@"8-A Downtown" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Duboce Triangle" withKey:@"5-J Duboce Triangle" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Eureka Valley/Dolores Heights" withKey:@"5-K Eureka Valley/Dolores Heights" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Excelsior" withKey:@"10-C Excelsior" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Financial District/Barbary Coast" withKey:@"8-B Financial District/Barbary Coast" isSelected:NO ];
	
	[self MutableArray:self.neighborhoods addItemName:@"Forest Hill" withKey:@"4-C Forest Hill" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Forest Hill Extension" withKey:@"4-J Forest Hill Extension" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Forest Knolls" withKey:@"4-D Forest Knolls" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Glen Park" withKey:@"5-A Glen Park" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Golden Gate Heights" withKey:@"2-A Golden Gate Heights" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Haight Ashbury" withKey:@"5-B Haight Ashbury" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Hayes Valley" withKey:@"6-B Hayes Valley" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Hunters Point" withKey:@"10-J Hunters Point" isSelected:NO ];
	
	[self MutableArray:self.neighborhoods addItemName:@"Ingleside" withKey:@"3-H Ingleside" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Ingleside Heights" withKey:@"3-G Ingleside Heights" isSelected:NO ];
    [self MutableArray:self.neighborhoods addItemName:@"Ingleside Terrace" withKey:@"4-E Ingleside Terrace" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Inner Mission" withKey:@"9-C Inner Mission" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Inner Parkside" withKey:@"2-G Inner Parkside" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Inner Richmond" withKey:@"1-B Inner Richmond" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Inner Sunset" withKey:@"2-F Inner Sunset" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Jordan Lake/Laurel Heights" withKey:@"1-C Jordan Lake/Laurel Heights" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Lake Street" withKey:@"1-D Lake Street" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Lake Shore" withKey:@"3-A Lake Shore" isSelected:NO ];
    [self MutableArray:self.neighborhoods addItemName:@"Lakeside" withKey:@"3-E Lakeside" isSelected:NO ];
    
	[self MutableArray:self.neighborhoods addItemName:@"Little Hollywood" withKey:@"10-N Little Hollywood" isSelected:NO ];
	
	[self MutableArray:self.neighborhoods addItemName:@"Lone Mountain" withKey:@"1-G Lone Mountain" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Lower Pacific Heights" withKey:@"6-C Lower Pacific Heights" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Marina" withKey:@"7-A Marina" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Merced Heights" withKey:@"3-B Merced Heights" isSelected:NO ];
    [self MutableArray:self.neighborhoods addItemName:@"Merced Manor" withKey:@"3-F Merced Manor" isSelected:NO ];
    
	[self MutableArray:self.neighborhoods addItemName:@"Midtown Terrace" withKey:@"4-F Midtown Terrace" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Miraloma Park" withKey:@"4-H Miraloma Park" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Mission Bay" withKey:@"9-D Mission Bay" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Mission Dolores" withKey:@"5-M Mission Dolores" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Mission Terrace" withKey:@"10-H Mission Terrace" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Monterey Heights" withKey:@"4-M Monterey Heights" isSelected:NO ];
	
	[self MutableArray:self.neighborhoods addItemName:@"Mount Davidson Manor" withKey:@"4-N Mount Davidson Manor" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Nob Hill" withKey:@"8-C Nob Hill" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Noe Valley" withKey:@"5-C Noe Valley" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"North Beach" withKey:@"8-D North Beach" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"North Panhandle" withKey:@"6-F North Panhandle" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"North Waterfront" withKey:@"8-H North Waterfront" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Oceanview" withKey:@"3-J Oceanview" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Outer Mission" withKey:@"10-D Outer Mission" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Outer Parkside" withKey:@"2-B Outer Parkside" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Outer Richmond" withKey:@"1-E Outer Richmond" isSelected:NO ];
	
	[self MutableArray:self.neighborhoods addItemName:@"Outer Sunset" withKey:@"2-C Outer Sunset" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Pacific Heights" withKey:@"7-B Pacific Heights" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Parkside" withKey:@"2-D Parkside" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Cole Valley/Parnassus Heights" withKey:@"5-E Cole Valley/Parnassus Heights" isSelected:NO ];
    [self MutableArray:self.neighborhoods addItemName:@"Pine Lake Park" withKey:@"3-C Pine Lake Park" isSelected:NO ];
    [self MutableArray:self.neighborhoods addItemName:@"Stonestown" withKey:@"3-D Stonestown" isSelected:NO ];
        
	[self MutableArray:self.neighborhoods addItemName:@"Portola" withKey:@"10-F Portola" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Potrero Hill" withKey:@"9-E Potrero Hill" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Presidio Heights" withKey:@"7-C Presidio Heights" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Russian Hill" withKey:@"8-E Russian Hill" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Sea Cliff" withKey:@"1-F Sea Cliff" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Sherwood Forest" withKey:@"4-K Sherwood Forest" isSelected:NO ];
	
	[self MutableArray:self.neighborhoods addItemName:@"Silver Terrace" withKey:@"10-G Silver Terrace" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"South Beach" withKey:@"9-H South Beach" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"South of Market" withKey:@"9-F South of Market" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"St. Francis Wood" withKey:@"4-G St. Francis Wood" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Sunnyside" withKey:@"4-S Sunnyside" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Telegraph Hill" withKey:@"8-G Telegraph Hill" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Tenderloin" withKey:@"8-J Tenderloin" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Twin Peaks" withKey:@"5-D Twin Peaks" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Van Ness/Civic Center" withKey:@"8-F Van Ness/Civic Center" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Visitacion Valley" withKey:@"10-E Visitacion Valley" isSelected:NO ];
	
	[self MutableArray:self.neighborhoods addItemName:@"West Portal" withKey:@"4-T West Portal" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Western Addition" withKey:@"6-D Western Addition" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Westwood Park" withKey:@"4-R Westwood Park" isSelected:NO ];
    [self MutableArray:self.neighborhoods addItemName:@"Westwood Highlands" withKey:@"4-P Westwood Highlands" isSelected:NO ];
	[self MutableArray:self.neighborhoods addItemName:@"Yerba Buena" withKey:@"9-G Yerba Buena" isSelected:NO ];


	// area
	self.area = [[[NSMutableArray alloc] init] autorelease];
	[self MutableArray:self.area addItemName:@"Any" withKey:@"Any" isSelected:YES ];
	
	[self MutableArray:self.area addItemName:@"1-A Central Richmond" withKey:@"1-A Central Richmond" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"1-B Inner Richmond" withKey:@"1-B Inner Richmond" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"1-C Jordan Lake/Laurel Heights" withKey:@"1-C Jordan Lake/Laurel Heights" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"1-D Lake Street" withKey:@"1-D Lake Street" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"1-E Outer Richmond" withKey:@"1-E Outer Richmond" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"1-F Sea Cliff" withKey:@"1-F Sea Cliff" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"1-G Lone Mountain" withKey:@"1-G Lone Mountain" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"2-A Golden Gate Heights" withKey:@"2-A Golden Gate Heights" isSelected:NO ];
	
	[self MutableArray:self.area addItemName:@"2-B Outer Parkside" withKey:@"2-B Outer Parkside" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"2-C Outer Sunset" withKey:@"2-C Outer Sunset" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"2-D Parkside" withKey:@"2-D Parkside" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"2-E Central Sunset" withKey:@"2-E Central Sunset" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"2-F Inner Sunset" withKey:@"2-F Inner Sunset" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"2-G Inner Parkside" withKey:@"2-G Inner Parkside" isSelected:NO ];
    
	[self MutableArray:self.area addItemName:@"3-A Lake Shore" withKey:@"3-A Lake Shore" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"3-B Merced Heights" withKey:@"3-B Merced Heights" isSelected:NO ];
    [self MutableArray:self.area addItemName:@"3-C Pine Lake Park" withKey:@"3-C Pine Lake Park" isSelected:NO ];
    [self MutableArray:self.area addItemName:@"3-D Stonestown" withKey:@"3-D Stonestown" isSelected:NO ];
    [self MutableArray:self.area addItemName:@"3-E Lakeside" withKey:@"3-E Lakeside" isSelected:NO ];
    [self MutableArray:self.area addItemName:@"3-F Merced Manor" withKey:@"3-F Merced Manor" isSelected:NO ];    
	[self MutableArray:self.area addItemName:@"3-G Ingleside Heights" withKey:@"3-G Ingleside Heights" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"3-H Ingleside" withKey:@"3-H Ingleside" isSelected:NO ];	
	[self MutableArray:self.area addItemName:@"3-J Oceanview" withKey:@"3-J Oceanview" isSelected:NO ];
    
	[self MutableArray:self.area addItemName:@"4-A Balboa Terrace" withKey:@"4-A Balboa Terrace" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"4-B Diamond Heights" withKey:@"4-B Diamond Heights" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"4-C Forest Hill" withKey:@"4-C Forest Hill" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"4-D Forest Knolls" withKey:@"4-D Forest Knolls" isSelected:NO ];
    [self MutableArray:self.area addItemName:@"4-E Ingleside Terrace" withKey:@"4-E Ingleside Terrace" isSelected:NO ];
    
	[self MutableArray:self.area addItemName:@"4-F Midtown Terrace" withKey:@"4-F Midtown Terrace" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"4-G St. Francis Wood" withKey:@"4-G St. Francis Wood" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"4-H Miraloma Park" withKey:@"4-H Miraloma Park" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"4-J Forest Hill Extension" withKey:@"4-J Forest Hill Extension" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"4-K Sherwood Forest" withKey:@"4-K Sherwood Forest" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"4-M Monterey Heights" withKey:@"4-M Monterey Heights" isSelected:NO ];
	
	[self MutableArray:self.area addItemName:@"4-N Mount Davidson Manor" withKey:@"4-N Mount Davidson Manor" isSelected:NO ];
    [self MutableArray:self.area addItemName:@"4-P Westwood Highlands" withKey:@"4-R Westwood Highlands" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"4-R Westwood Park" withKey:@"4-R Westwood Park" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"4-S Sunnyside" withKey:@"4-S Sunnyside" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"4-T West Portal" withKey:@"4-T West Portal" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"5-A Glen Park" withKey:@"5-A Glen Park" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"5-B Haight Ashbury" withKey:@"5-B Haight Ashbury" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"5-C Noe Valley" withKey:@"5-C Noe Valley" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"5-D Twin Peaks" withKey:@"5-D Twin Peaks" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"5-E Cole Valley/Parnassus Heights" withKey:@"5-E Cole Valley/Parnassus Heights" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"5-F Buena Vista/Ashbury Heights" withKey:@"5-F Buena Vista/Ashbury Heights" isSelected:NO ];
	
	[self MutableArray:self.area addItemName:@"5-G Corona Heights" withKey:@"5-G Corona Heights" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"5-H Clarendon Heights" withKey:@"5-H Clarendon Heights" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"5-J Duboce Triangle" withKey:@"5-J Duboce Triangle" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"5-K Eureka Valley/Dolores Heights" withKey:@"5-K Eureka Valley/Dolores Heights" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"5-M Mission Dolores" withKey:@"5-M Mission Dolores" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"6-A Anza Vista" withKey:@"6-A Anza Vista" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"6-B Hayes Valley" withKey:@"6-B Hayes Valley" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"6-C Lower Pacific Heights" withKey:@"6-C Lower Pacific Heights" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"6-D Western Addition" withKey:@"6-D Western Addition" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"6-E Alamo Square" withKey:@"6-E Alamo Square" isSelected:NO ];
	
	[self MutableArray:self.area addItemName:@"6-F North Panhandle" withKey:@"6-F North Panhandle" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"7-A Marina" withKey:@"7-A Marina" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"7-B Pacific Heights" withKey:@"7-B Pacific Heights" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"7-C Presidio Heights" withKey:@"7-C Presidio Heights" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"7-D Cow Hollow" withKey:@"7-D Cow Hollow" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"8-A Downtown" withKey:@"8-A Downtown" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"8-B Financial District/Barbary Coast" withKey:@"8-B Financial District/Barbary Coast" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"8-C Nob Hill" withKey:@"8-C Nob Hill" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"8-D North Beach" withKey:@"8-D North Beach" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"8-E Russian Hill" withKey:@"8-E Russian Hill" isSelected:NO ];
	
	[self MutableArray:self.area addItemName:@"8-F Van Ness/Civic Center" withKey:@"8-F Van Ness/Civic Center" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"8-G Telegraph Hill" withKey:@"8-G Telegraph Hill" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"8-H North Waterfront" withKey:@"8-H North Waterfront" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"8-J Tenderloin" withKey:@"8-J Tenderloin" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"9-A Bernal Heights" withKey:@"9-A Bernal Heights" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"9-C Inner Mission" withKey:@"9-C Inner Mission" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"9-D Mission Bay" withKey:@"9-D Mission Bay" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"9-E Potrero Hill" withKey:@"9-E Potrero Hill" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"9-F South of Market" withKey:@"9-F South of Market" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"9-G Yerba Buena" withKey:@"9-G Yerba Buena" isSelected:NO ];
	
	[self MutableArray:self.area addItemName:@"9-H South Beach" withKey:@"9-H South Beach" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"9-J Central Waterfront/Dogpatc" withKey:@"9-J Central Waterfront/Dogpatc" isSelected:NO ];
	
	[self MutableArray:self.area addItemName:@"10-A Bayview" withKey:@"10-A Bayview" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"10-B Crocker Amazon" withKey:@"10-B Crocker Amazon" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"10-C Excelsior" withKey:@"0-C Excelsior" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"10-D Outer Mission" withKey:@"10-D Outer Mission" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"10-E Visitacion Valley" withKey:@"10-E Visitacion Valley" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"10-F Portola" withKey:@"Portola" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"10-G Silver Terrace" withKey:@"10-G Silver Terrace" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"10-H Mission Terrace" withKey:@"10-H Mission Terrace" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"10-J Hunters Point" withKey:@"10-J Hunters Point" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"10-K Bayview Heights" withKey:@"10-K Bayview Heights" isSelected:NO ];	
	[self MutableArray:self.area addItemName:@"10-M Candlestick Point" withKey:@"10-M Candlestick Point" isSelected:NO ];
	[self MutableArray:self.area addItemName:@"10-N Little Hollywood" withKey:@"10-N Little Hollywood" isSelected:NO ];	
					
	return self;
}

+ (NSMutableArray*)bathsArray{
	
	static NSMutableArray *bathsArray = nil;
	
	if (bathsArray == nil) {
		
		bathsArray = [[NSMutableArray alloc] initWithObjects:
					  @"Any", @"0.5", @"1.0", @"1.5", @"2.0", @"2.5", @"3.0", @"3.5", @"4.0", @"4.5", @"5", nil];	
	}
	return bathsArray;	
}

+ (NSMutableArray*)bedsArray{
	
	static NSMutableArray *bedsArray = nil;
	
	if (bedsArray == nil) {
		
		bedsArray = [[NSMutableArray alloc] initWithObjects:
					 @"Any", @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10",nil];	};
	
	return bedsArray;	
}

+ (NSMutableArray*)domArray{
	
	static NSMutableArray *domArray = nil;
	
	if (domArray == nil) {		
		
		domArray = [[NSMutableArray alloc] initWithObjects:
					 @"2", @"3", @"7", @"14", @"21", @"28", @"Any", nil];						
	}
	
	return domArray;	
}

+ (NSMutableArray*)pricesArray{
	
	static NSMutableArray *pricesArray = nil;
	
	if (pricesArray == nil) {		
		
		pricesArray = [[NSMutableArray alloc] initWithObjects:
					@"$250,000", @"$300,000", @"$350,000", @"$400,000", @"$450,000",@"$500,000",
					   @"$550,000", @"$600,000", @"$650,000", @"$700,000", @"$750,000",@"$800,000",
					   @"$850,000", @"$900,000", @"$950,000", @"$1,000,000", @"$1,100,000", @"$1,200,000",
					   @"$1,300,000", @"$1,400,000", @"$1,500,000", @"$1,600,000", @"$1,700,000",@"$1,800,000",
					   @"$1,900,000", @"$2,000,000", @"$2,100,000", @"$2,200,000", @"$2,300,000",@"$2,400,000",
					   @"$2,500,000", @"$2,600,000", @"$2,700,000", @"$2,800,000", @"$2,900,000",@"$3,000,000",
					   @"Any", nil];				
	}
	
	return pricesArray;	
}


+ (NSMutableArray*)structureSizeArray{
	
	static NSMutableArray *structureSizeArray = nil;
	
	if (structureSizeArray == nil) {		
		
		structureSizeArray = [[NSMutableArray alloc] initWithObjects:
					   @"Any", @"1000", @"1250", @"1500", @"1750", @"2000", @"2250",
					   @"2500", @"2750", @"3000", @"3250", @"3500",@"3750",
					   @"4000", @"4250", @"4500", @"4750", @"5000",
					   nil];				
	}
	
	return structureSizeArray;	
}

+ (NSMutableArray*)resultsPerPageArray{
	
	static NSMutableArray *resultsPerPageArray = nil;
	
	if (resultsPerPageArray == nil) {		
		
		resultsPerPageArray = [[NSMutableArray alloc] initWithObjects:
							   @"10", @"15", @"25", @"50", @"75", @"100",
							   nil];				
	}
	
	return resultsPerPageArray;	
}

+ (NSMutableArray*) locationTypeNames{
	
	static NSMutableArray *locationTypes = nil;
	
	if (locationTypes == nil) {
		locationTypes = [[NSMutableArray alloc] initWithObjects:@"Area", @"NBRH", @"Zip", nil];
	}
	
	return locationTypes;	
}

+ (NSMutableDictionary*) keyedDictionaryFromArray:(NSArray*)array
{
	NSMutableDictionary *dict = [[[NSMutableDictionary alloc]initWithCapacity:array.count] autorelease];
	
	for (int i=0; i<array.count; ++i) {
		
		[dict setObject:[NSNumber numberWithInt:i] forKey:[array objectAtIndex:i]];
	}
	
	return dict;	
}

+ (NSString*) neighborhoodFromArea:(NSString *)areaKey
{
	static NSDictionary *hoodsDic = nil;
	
	
	if (hoodsDic == nil) {
		
		hoodsDic = [[NSDictionary dictionaryWithObjectsAndKeys:
					@"Alamo Square", @"6-E",
					@"Anza Vista", @"6-A",	
					@"Balboa Terrace" ,@"4-A",
					@"Bayview", @"10-A",
					@"Bayview Heights", @"10-K",
					@"Bernal Heights", @"9-A",
					@"Buena Vista/Ashbury Heights", @"5-F",
					@"Candlestick Point", @"10-M",
					@"Central Richmond", @"1-A",
					@"Central Sunset", @"2-E",
					@"Central Waterfront/Dogpatch", @"9-J",
					@"Clarendon Heights", @"5-H",
					@"Corona Height", @"5-G",
					@"Cow Hollow", @"7-D",
					
					@"Crocker Amazon", @"10-B",
					@"Diamond Heights", @"4-B",
					@"Downtown", @"8-A",
					@"Duboce Triangle", @"5-J",
					@"Eureka Valley/Dolores Heights", @"5-K",
					@"Excelsior", @"10-C",
					@"Financial District/Barbary Coast", @"8-B",
					
					@"Forest Hill", @"4-C",
					@"Forest Hill Extension", @"4-J",
					@"Forest Knolls", @"4-D",
					@"Glen Park", @"5-A",
					@"Golden Gate Heights", @"2-A",
					@"Haight Ashbury", @"5-B",
					@"Hayes Valley", @"6-B",
					@"Hunters Point", @"10-J",
					
					@"Ingleside", @"3-H",
					@"Ingleside Heights", @"3-G",
                    @"Ingleside Terrace", @"4-E",

					@"Inner Mission", @"9-C",
					@"Inner Parkside", @"2-G",
					@"Inner Richmond", @"1-B",
					@"Inner Sunset", @"2-F",
                     
					@"Jordan Lake/Laurel Heights", @"1-C",
                     
					@"Lake Street", @"1-D",
					@"Lake Shore", @"3-A",
                    @"Lakeside", @"3-E",                   
                    @"Little Hollywood", @"10-N",					
					@"Lone Mountain", @"1-G",
					@"Lower Pacific Heights", @"6-C",
                     
					@"Marina", @"7-A",
					@"Merced Heights", @"3-B",
                    @"Merced Manor", @"3-F",                                                                                          
					@"Midtown Terrace", @"4-F",
					@"Miraloma Park", @"4-H",
					@"Mission Bay", @"9-D",
					@"Mission Dolores", @"5-M",
					@"Mission Terrace", @"10-H",
					@"Monterey Heights", @"4-M",
					
					@"Mount Davidson Manor", @"4-N",
					@"Nob Hill", @"8-C",
					@"Noe Valley", @"5-C",
					@"North Beach", @"8-D",
					@"North Panhandle", @"6-F",
					@"North Waterfront", @"8-H",
					@"Oceanview", @"3-J",
					@"Outer Mission", @"10-D",
					@"Outer Parkside", @"2-B",
					@"Outer Richmond", @"1-E",					
					@"Outer Sunset", @"2-C",
   
					@"Pacific Heights", @"7-B",
					@"Parkside", @"2-D",
					@"Cole Valley/Parnassus Heights", @"5-E",
                    @"Pine Lake Park", @"3-C",
                    @"Stonestown", @"3-D",
					@"Portola", @"10-F",
					@"Potrero Hill", @"9-E",
					@"Presidio Heights", @"7-C",
					@"Russian Hill", @"8-E",
					@"Sea Cliff", @"1-F",
					@"Sherwood Forest", @"4-K",
					
					@"Silver Terrace", @"10-G",
					@"South Beach", @"9-H",
					@"South of Market", @"9-F",
					@"St. Francis Wood", @"4-G",
					@"Sunnyside", @"4-S",
					@"Telegraph Hill", @"8-G",
					@"Tenderloin", @"8-J",
					@"Twin Peaks", @"5-D",
					@"Van Ness/Civic Center", @"8-F",
					@"Visitacion Valley", @"10-E",
					
					@"West Portal", @"4-T",
					@"Western Addition", @"6-D",
                    @"Westwood Highlands", @"4-P",
					@"Westwood Park", @"4-R",
					@"Yerba Buena", @"9-G",
					nil] copy];
	}
	
	NSString *hood = [hoodsDic objectForKey:areaKey];
	
	return hood;
	
}


- (id)initWithCoder:(NSCoder *)decoder
{
	// if the superclass initializes properly
	if (self = [super init])
	{
		self.beds = [decoder  decodeIntForKey:@"beds"];
		self.baths = [decoder  decodeIntForKey:@"baths"];
		self.dom = [decoder  decodeIntForKey:@"dom"];
		self.price = [decoder  decodeIntForKey:@"price"];
		self.resultsPerPage = [decoder decodeIntForKey:@"resultsPerPage"];
		self.structureSize = [decoder  decodeIntForKey:@"structureSize"];
		self.propertyTypes = [decoder decodeObjectForKey:@"propertyTypes"];
		self.zipcodes = [decoder decodeObjectForKey:@"zipcodes"];
		self.neighborhoods = [decoder decodeObjectForKey:@"neighborhoods"];
		self.area = [decoder decodeObjectForKey:@"area"];
		self.selectedLocationList = [decoder decodeIntForKey:@"selectedLocationList"];
        self.openHousesOnly = [decoder decodeBoolForKey:@"openHousesOnly"];
        self.shortSaleOnly = [decoder decodeBoolForKey:@"shortSaleOnly"];
        self.reoSaleOnly = [decoder decodeBoolForKey:@"reoSaleOnly"];
        self.priceReducedOnly = [decoder decodeBoolForKey:@"priceReducedOnly"];
	}
	
	return self; // return this object
}

// encode this object into the given NSCoder
- (void)encodeWithCoder:(NSCoder *)coder
{
	[coder encodeInt:(int)beds forKey:@"beds"];
	[coder encodeInt:(int)baths forKey:@"baths"];
	[coder encodeInt:(int)dom forKey:@"dom"];
	[coder encodeInt:(int)price forKey:@"price"];
	[coder encodeInt:(int)structureSize forKey:@"structureSize"];
	[coder encodeInt:(int)resultsPerPage forKey:@"resultsPerPage"];
	[coder encodeObject:propertyTypes forKey:@"propertyTypes"];	
	[coder encodeObject:zipcodes forKey:@"zipcodes"];	
	[coder encodeObject:neighborhoods forKey:@"neighborhoods"];	
	[coder encodeObject:area forKey:@"area"];
	[coder encodeInt:(int)selectedLocationList forKey:@"selectedLocationList"];
    [coder encodeBool:openHousesOnly forKey:@"openHousesOnly"];
    [coder encodeBool:shortSaleOnly forKey:@"shortSaleOnly"];
    [coder encodeBool:reoSaleOnly forKey:@"reoSaleOnly"];
    [coder encodeBool:priceReducedOnly forKey:@"priceReducedOnly"];
	
} 

-(void)MutableArray:(NSMutableArray *)theArray addItemName:(NSString*)name withKey:(NSString*)key isSelected:(BOOL)isSelected{
	
	ListItem *item = [[ListItem alloc] init];
	
	item.name = name;
	item.key = key;
	item.isSelected = isSelected;	
	
	[theArray addObject:item];
	
	[item release];	
}

- (void)dealloc{
    [super dealloc];
}



@end
