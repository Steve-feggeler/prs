//
//  PropertyTableViewCell.m
//  TestSDWebImage1
//
//  Created by stephen feggeler on 3/4/13.
//  Copyright 2013 self. All rights reserved.
//

#import "PropertyTableViewCell.h"


@implementation PropertyTableViewCell

@synthesize label1;
@synthesize label2;
@synthesize label3;
@synthesize label4;
@synthesize label5;
@synthesize label6;
@synthesize mainImage;
@synthesize openHouseDays;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code.
    }
    return self;
}

- (void)layoutSubviewsHIDE {  
	[super layoutSubviews];  
	self.imageView.frame = CGRectMake(5,5,40,32.5);  
	self.imageView.frame = CGRectMake(5,5,135,90); 
	float limgW =  self.imageView.image.size.width;  
	if(limgW > 0) {  
		self.textLabel.frame = CGRectMake(55,self.textLabel.frame.origin.y,self.textLabel.frame.size.width,self.textLabel.frame.size.height);  
		self.detailTextLabel.frame = CGRectMake(55,self.detailTextLabel.frame.origin.y,self.detailTextLabel.frame.size.width,self.detailTextLabel.frame.size.height);  
	}  
}  


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state.
}


- (void)dealloc {
    [super dealloc];
}


@end
