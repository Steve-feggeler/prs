//
//  PropertyListController.h
//  R2
//
//  Created by stephen feggeler on 2/28/13.
//  Copyright 2013 self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "R2AppDelegate.h"
#import "PropertyFinder.h"
#import "Reachability.h"
#import "PropertyDetailsViewController.h"

@interface PropertyListController : UITableViewController
<PropertyFinderDelegate, UIAlertViewDelegate, PropertySelectionChanged>
{
    Reachability* reach;
    BOOL connectionUP;
}


@property (nonatomic, assign) NSInteger totalProperties;
@property (nonatomic, retain) NSMutableArray * properties;


@end
