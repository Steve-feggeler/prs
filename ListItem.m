//
//  ListItem.m
//  R1
//
//  Created by stephen feggeler on 1/26/13.
//  Copyright 2013 self. All rights reserved.
//

#import "ListItem.h"


@implementation ListItem

//@synthesize name;
//@synthesize isSelected;
//@synthesize key;

- (id)initWithCoder:(NSCoder *)decoder
{
	// if the superclass initializes properly
	if (self = [super init])
	{
		self.name = [decoder decodeObjectForKey:@"name"];
		self.key = [decoder decodeObjectForKey:@"key"];		
		self.isSelected = [decoder decodeBoolForKey:@"isSelected"];
	}
	
	return self; // return this object
}

// encode this object into the given NSCoder
- (void)encodeWithCoder:(NSCoder *)coder
{
	[coder encodeObject:_name forKey:@"name"];
	[coder encodeObject:_key forKey:@"key"];
	[coder encodeBool:_isSelected forKey:@"isSelected"];
}

-(void)dealloc{
    
    [_name release];
    [_key release];
    
    [super dealloc];
}


@end
