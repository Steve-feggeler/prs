

//#import <UIKit/UIKit.h>
#define TIMEOUT_INTERVAL 45

@class FSTargetCallback;
@protocol FSRequesterErrorDelegate;

@interface FSRequester : NSObject <UIAlertViewDelegate>{
    BOOL needToShowErrorAlert;
}

@property (strong,nonatomic) NSMutableArray *requestHistory;
@property (strong, nonatomic) NSMutableDictionary *asyncConnDict;
@property (nonatomic, assign) id <FSRequesterErrorDelegate> errorDelegate;

- (void) handleConnectionError:(NSError *)error;
- (void) makeAsyncRequest:(NSURL *)url target:(FSTargetCallback *)target;
- (void) makeAsyncRequestWithRequest:(NSURLRequest *)urlRequest target:(FSTargetCallback *)target;
+ (void) setDelegate:(id <FSRequesterErrorDelegate>)delegate;

@end

@protocol FSRequesterErrorDelegate

- (void) requester:(FSRequester *)requester didFailWithErrror:(NSError*)error;

@end
