//
//  VenueAnnotation.m
//  Foursquare2-iOS
//
//  Created by Constantine Fry on 1/21/13.
//
//

#import "FSVenue.h"


@implementation FSLocation

@synthesize coordinate = _coordinate;
@synthesize distance = _distance;
@synthesize address = _address;

-(void)dealloc{
   // [_distance release];
    [_address release];
    [super dealloc];
}

@end

@implementation FSVenue

@synthesize location = _location;
@synthesize name = _name;
@synthesize query = _query;

- (id)init
{
    self = [super init];
    if (self) {
        _location = [[FSLocation alloc] init];
    }
    return self;
}

-(CLLocationCoordinate2D)coordinate{
    return self.location.coordinate;
}

-(NSString*)title{
    return self.name;
}

- (NSString *)subtitle {
   return self.location.address;
}

-(void)dealloc{
    [_location release];
    [_name release];
    [_query release];
    [super dealloc];
}


@end
