//
//  VenueAnnotation.h
//  Foursquare2-iOS
//
//  Created by Constantine Fry on 1/21/13.
//
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface FSLocation : NSObject 
{

}

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, assign) NSNumber *distance;
@property (copy) NSString *address;

@end

@interface FSVenue : NSObject<MKAnnotation>

@property (nonatomic, copy) NSString* name;
@property (nonatomic, assign) NSString* venueId;
@property (nonatomic, assign) FSLocation* location;
@property (nonatomic, copy) NSString* query;

@end
