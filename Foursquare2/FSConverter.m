//
//  FSConverter.m
//  Foursquare2-iOS
//
//  Created by Constantine Fry on 2/7/13.
//
//

#import "FSConverter.h"
#import "FSVenue.h"

@implementation FSConverter

-(NSMutableArray*)convertToObjects:(NSArray*)venues query:(NSString*)query{
    
    NSMutableArray *objects = [NSMutableArray arrayWithCapacity:venues.count];
    
    for (NSDictionary *v in venues) {
        
        FSVenue *ann = [[[FSVenue alloc]init] autorelease];
        
        ann.name = v[@"name"];
        ann.venueId = v[@"id"];
        ann.location.address = v[@"location"][@"address"];
        ann.location.distance = v[@"location"][@"distance"];
        ann.query = query;
        [ann.location setCoordinate:CLLocationCoordinate2DMake(
                                                      [v[@"location"][@"lat"] doubleValue],
                                                      [v[@"location"][@"lng"] doubleValue]
                                                  )];
        [objects addObject:ann];
    }
    return objects;
}

@end
