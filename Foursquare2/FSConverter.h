//
//  FSConverter.h
//  Foursquare2-iOS
//
//  Created by Constantine Fry on 2/7/13.
//
//

#import <Foundation/Foundation.h>

@interface FSConverter : NSObject

-(NSMutableArray*)convertToObjects:(NSArray*)venues query:(NSString*)query;

@end
