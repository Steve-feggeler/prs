//
//  PropertyListController.m
//  R2
//
//  Created by stephen feggeler on 2/28/13.
//  Copyright 2013 self. All rights reserved.
//

#import "PropertyListController.h"
#import "UIImageView+WebCache.h"
#import "PropertyTableViewCell.h"
#import "Property.h"
#import "PropertyDetailsViewController.h"
#import "Reachability.h"


@implementation PropertyListController

@synthesize totalProperties;
@synthesize postCMD;
@synthesize isAppend;

- (void)propertyFinder:(PropertyFinder *)finder didFailWithError:(NSError *)error
{
    
}

- (void)propertyFinder:(PropertyFinder *)finder didFindProperties:(NSMutableArray *)newProperties inRange:(NSRange)range ofTotal:(NSInteger) total
{
    //self.properties = [newProperties mutableCopy];
    
	self.properties = newProperties;
	
	NSString *results = @"No Properties";
	
	if (total>0) {
		//results = [NSString stringWithFormat:@"%lu of %ld", (unsigned long)self.properties.count, (long)total];
        results = [NSString stringWithFormat:@"%ld Properties", (long)total];
	}
    
    if (self.properties.count > total) {
        int i=0;
        ++i;
    }
	
	self.totalProperties = total;
    self.navigationItem.title = [NSString stringWithFormat:@"%@", results];
    
    // if not append scroll to top
    if (!self.isAppend) {
       // [self.tableView setContentOffset:CGPointZero animated:NO];
        [self.tableView setContentOffset:CGPointMake(0, -64) animated:NO];
    }
		
	[self.tableView reloadData];
    
  //  [self.tableView setContentOffset:CGPointMake(0, -64) animated:NO];
	
	NSArray *viewControllers = self.navigationController.viewControllers;
	
	if (viewControllers.count == 2) {
		PropertyDetailsViewController *detailViewController = (PropertyDetailsViewController*)[viewControllers objectAtIndex:1];
		
		detailViewController.properties = self.properties;
		if (range.location == 0) {
			detailViewController.indexOfProperty = 0;
		}
				detailViewController.totalProperties = self.totalProperties;	
		[detailViewController setupPropertyDetails];
	}
	//[self.navigationController.viewControllers
}

#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // fetch properties here
    R2AppDelegate *appDelegate = (R2AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.propertyFinderDelegate = self;
    [appDelegate fetchProperties];
    
    connectionUP = YES;    
    
    // Allocate a reachability object
    reach = [Reachability reachabilityWithHostname:@"www.pacificrealtyservice.com"];
    
    // Tell the reachability that we DON'T want to be reachable on 3G/EDGE/CDMA
    reach.reachableOnWWAN = YES;
    
    // Here we set up a NSNotification observer. The Reachability that caused the notification
    // is passed in the object parameter
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    
    [reach startNotifier];
    
    self.navigationItem.title = @"";
}

- (void)reachabilityChanged:(NSString*)notification
{
    if (connectionUP == reach.isReachable) {
        return;
    }
    
    connectionUP = reach.isReachable;
    
    if (!connectionUP) {
        /*
        UIAlertView *alertView = [[UIAlertView alloc]
        initWithTitle:NSLocalizedString(@"Internet not available!", @"AlertView")
        message:NSLocalizedString(@"Please connect to a network.", @"AlertView")
        delegate:self
        cancelButtonTitle:NSLocalizedString(@"Ok", @"AlertView")
        otherButtonTitles:nil, nil];
        // otherButtonTitles:NSLocalizedString(@"Open settings", @"AlertView"), nil];
        
        [alertView show];
         */
    }
    else{
        R2AppDelegate *appDelegate = (R2AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        NSUInteger selectedIndex = self.tabBarController.selectedIndex;
        
        // check for fetch error, and fetch again if necessary
        
        if(selectedIndex == 0 && appDelegate.listFetchError){
            [appDelegate fetchProperties];
        }
        else if(selectedIndex == 1 && appDelegate.mapFetchError){
            [appDelegate fetchProperties];
        }
    }
    
    /*
    if(!(flags & kSCNetworkReachabilityFlagsReachable)){
        connectionUP = NO;
        NSLog(@"UNREACHABLE!");
    }
    else
    {
        NSLog(@"REACHABLE!");
    }
    */
    
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"prefs:root=WIFI"]];
    }
}

- (void)viewWillAppear:(BOOL)animated {
 
    [super viewWillAppear:animated];
	
	R2AppDelegate *appDelegate = (R2AppDelegate*)[[UIApplication sharedApplication] delegate];
	
	appDelegate.propertyFinderDelegate = self;
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7){
        self.automaticallyAdjustsScrollViewInsets = YES;
        self.edgesForExtendedLayout = UIRectEdgeAll;
        
        self.edgesForExtendedLayout    = UIRectEdgeTop | UIRectEdgeLeft | UIRectEdgeRight;
    }
    
    // force portrait
    /*
     UIApplication* application = [UIApplication sharedApplication];
     if (application.statusBarOrientation != UIInterfaceOrientationPortrait)
     {
     UIViewController *vc = [[UIViewController alloc]init];
     [self presentModalViewController:vc animated:NO];
     [self dismissModalViewControllerAnimated:NO];
     [vc release];
     }
     */

	
	//self.properties = appDelegate.properties;
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return _properties.count;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	static NSString *CellIdentifier = @"Cell";
    
    PropertyTableViewCell *cell = (PropertyTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
    if (cell == nil) {
		
		cell = [[[NSBundle mainBundle] loadNibNamed:@"PropertyTableViewCell" owner:self options:nil] objectAtIndex:0];
    }
    
	// Configure the cell.
    //cell.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:251.0/255.0 blue:222.0/255.0 alpha:1.0];
    //cell.backgroundColor = [UIColor colorWithRed:255.0/255.0 green:252.0/255.0 blue:227.0/255.0 alpha:1.0];
	
	Property *property = [_properties objectAtIndex:indexPath.row];
	
	NSString *path = [property mainThumbImage];
	
   // [cell.mainImage setImageWithURL:[NSURL URLWithString:path]
   //                placeholderImage:[UIImage imageNamed:@"placeholder"]];
    
    [cell.mainImage setImageWithURL:[NSURL URLWithString:path]
    success:^(UIImage *image, BOOL cached)
    {
        
    }
    failure:^(NSError *error)
    {
        cell.mainImage.image = [UIImage imageNamed:@"No-Image-available.png"];
    }];    
     
	cell.mainImage.contentMode = UIViewContentModeScaleAspectFit;
    
	cell.label1.text = [property price];
	cell.label2.text = [property neighborhood];
	cell.label3.text = [property address1];
	cell.label4.text = [property address2];
	cell.label5.text = [property detials1];
    cell.label6.text = [NSString stringWithFormat:@"San Francisco MLS, %@", [property listingOfficeName]];
    cell.label6.textColor = [UIColor colorWithWhite: 0.55 alpha:1];
    cell.openHouseDays.text = [property openHouseDays];
    
    BOOL dont_show_address = [property.showAddressToPublic isEqualToString:@"0"];
    if (dont_show_address) {
        cell.label3.text = @"Address intentionally hidden";
    }
    
	
	// if last row, fetch more properties
	//
	if (_properties.count - 1 == indexPath.row) {
		
		R2AppDelegate *appDelegate = (R2AppDelegate*)[[UIApplication sharedApplication] delegate];
		
		[appDelegate fetchMoreProperties];
	}			
	
    return cell;
}

- (void)didChangeSelectedProperty:(Property*)property
{
    NSInteger index = [self.properties indexOfObject:property];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
    
    PropertyDetailsViewController *detailViewController = [[PropertyDetailsViewController alloc] initWithNibName:@"PropertyDetailsViewController" bundle:nil];
	
	detailViewController.properties = self.properties;
	detailViewController.indexOfProperty = indexPath.row;
	detailViewController.totalProperties = self.totalProperties;
    detailViewController.hidesBottomBarWhenPushed = YES;
    detailViewController.selectedPropertyDelegate = self;
    
    self.navigationItem.backBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil] autorelease];

    // Pass the selected object to the new view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
	
    [detailViewController release];    
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    [_properties release];
     [super dealloc];
}


@end

