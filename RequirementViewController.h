//
//  FlipsideViewController.h
//  R1
//
//  Created by stephen feggeler on 12/20/12.
//  Copyright 2012 self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "R2AppDelegate.h"
#import "ItemCell.h"
#import "SliderCell.h"
#import "CheckListController.h"
#import "DataController.h"
#import "SliderRequirementsCell.h"
#import "MultiCheckListController.h"
#import "CheckListItem.h"

@protocol FlipsideViewControllerDelegate;

@interface RequirementViewController : UITableViewController 
	 <UIPickerViewDelegate, 
	 CheckListControllerDelegate, 
     MultiCheckListControllerDelegate> {
	
	id <FlipsideViewControllerDelegate> delegate;
	IBOutlet UITableView *table; // table that displays editable fields
	NSMutableDictionary *requirements;
	NSArray *keys;
	UIPickerView *pickerView;
	UIToolbar *pickerToolbar;
	UIView *mask;
	
	NSMutableArray *arrayColors;
	NSMutableArray *arrayPrices;
	NSMutableArray *beds;
	
	NSMutableDictionary *sections;
	NSMutableArray *sectionNames;
	NSMutableArray *propertyTypes;
		
	DataController *dataController;
		
	UILabel *priceValue;
	UILabel *bedsValue;
	UILabel *bathsValue;
	UILabel	*structureSizeValue;
    UILabel *domValue;
	
	NSArray *fields; // an array containing the field names
}

@property (nonatomic,retain) DataController* dataController;
@property (nonatomic, assign) id <FlipsideViewControllerDelegate> delegate;
@property (nonatomic, retain) IBOutlet UITableView *table;

- (IBAction)done:(id)sender;
- (void)finishedHidingPicker;
- (IBAction)priceValueChanged:(UISlider *)sender;
 
- (UITableViewCell *)tableView:(UITableView *)tableView itemCellForRowAtIndexPath:(NSIndexPath *)indexPath;
- (UITableViewCell *)tableView:(UITableView *)tableView sliderCellForRowAtIndexPath:(NSIndexPath *)indexPath;

@end


@protocol FlipsideViewControllerDelegate
- (void)flipsideViewControllerDidFinish:(RequirementViewController *)controller;
@end

