//
//  CheckListController.h
//  R1
//
//  Created by stephen feggeler on 1/24/13.
//  Copyright 2013 self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListItem.h"

@protocol CheckListControllerDelegate; 

@interface CheckListController : UIViewController <UITableViewDataSource> {
	
	id <CheckListControllerDelegate> delegate;
	NSDictionary *items;
	NSMutableArray *itemsArray;
		
}

@property (nonatomic, assign) id <CheckListControllerDelegate> delegate;
@property (nonatomic, retain) NSDictionary *items;
@property (nonatomic, retain) NSMutableArray *itemsArray;

- (IBAction)doneSelecting:sender; 

@end


// notifies ViewController that Done Button was touched
@protocol CheckListControllerDelegate

- (void) checkListControllerDidFinishSelecting:(CheckListController *)controller;

@end
