//
//  CheckListItem.h
//  R1
//
//  Created by stephen feggeler on 2/16/13.
//  Copyright 2013 self. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CheckListItem : NSObject {

}

@property (nonatomic, copy) NSString *name;
@property (nonatomic, retain) NSMutableArray *list;

@end
