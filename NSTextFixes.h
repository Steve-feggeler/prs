//
//  NSTextFixes.h
//  R2
//
//  Created by stephen feggeler on 5/24/13.
//
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

#ifndef __IPHONE_6_0 // before ios6
#   define NSTextAlignmentCenter    UITextAlignmentCenter
#   define NSTextAlignmentLeft      UITextAlignmentLeft
#   define NSTextAlignmentRight     UITextAlignmentRight
#   define NSLineBreakByTruncatingTail     UILineBreakModeTailTruncation
#   define NSLineBreakByTruncatingMiddle   UILineBreakModeMiddleTruncation
#endif



#ifndef R2_NSTextFixes_h
#define R2_NSTextFixes_h



#endif
