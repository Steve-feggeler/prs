//
//  ItemTableCell.m
//  R1
//
//  Created by stephen feggeler on 12/26/12.
//  Copyright 2012 self. All rights reserved.
//

#import "ItemCell.h"


@implementation ItemCell

@synthesize labelKey;   // synthesize get and set methods for label
@synthesize labelValue; // synthesize get and set methods for label


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	
    if (self) {
        // Initialization code.
		
		// create the label on the left side
		labelKey = [[UILabel alloc] initWithFrame:CGRectMake(14, 13, 165, 25)];
		//labelKey.backgroundColor = [UIColor brownColor];
        
        CGFloat widthLabel = [[UIScreen mainScreen] bounds].size.width - 180 - 20;
		
		// create the label on the right of the label
		labelValue = [[UILabel alloc] initWithFrame:CGRectMake(180, 13, widthLabel, 25)];
		
		labelValue.textAlignment = NSTextAlignmentRight;
		//labelValue.backgroundColor = [UIColor blueColor];
		
		// add label to the cell
		[self.contentView addSubview:labelKey];
		
		// add label to the cell
		[self.contentView addSubview:labelValue];
		
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state.
}


- (void)dealloc {
    [super dealloc];
}


@end
