//
//  PropertyTableViewCell.h
//  TestSDWebImage1
//
//  Created by stephen feggeler on 3/4/13.
//  Copyright 2013 self. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PropertyTableViewCell : UITableViewCell {
	

}

@property (nonatomic, retain) IBOutlet UILabel *label1;
@property (nonatomic, retain) IBOutlet UILabel *label2;
@property (nonatomic, retain) IBOutlet UILabel *label3;
@property (nonatomic, retain) IBOutlet UILabel *label4;
@property (nonatomic, retain) IBOutlet UILabel *label5;
@property (nonatomic, retain) IBOutlet UILabel *label6;
@property (nonatomic, retain) IBOutlet UIImageView *mainImage;
@property (nonatomic, retain) IBOutlet UILabel *openHouseDays;

@end
