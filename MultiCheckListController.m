//
//  MultiCheckListController.m
//  R1
//
//  Created by stephen feggeler on 2/11/13.
//  Copyright 2013 self. All rights reserved.
//

#import "MultiCheckListController.h"

@interface MultiCheckListController ()

@property (nonatomic, retain) NSMutableArray *sideIndexes;
@property (nonatomic, retain) NSMutableArray *tablesData;
@property (nonatomic, retain) NSMutableArray *alphabetsArray;

@end

@implementation MultiCheckListController
{
    NSMutableArray *searchResults;
    BOOL isIndexScroll;
    BOOL searchBarCancelClicked;
    BOOL searchBarClicked;
}

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{    
	[super viewDidLoad];
    
	UIBarButtonItem *cancelButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Reset" style:UIBarButtonItemStyleBordered target:self action:@selector(reset)];
    self.navigationItem.leftBarButtonItem = cancelButtonItem;
    [cancelButtonItem release];
    
    UIBarButtonItem *saveButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(done)];
    self.navigationItem.rightBarButtonItem = saveButtonItem;
    [saveButtonItem release];
    
    isIndexScroll = NO;
    
    // create table data and side indexes
    //
    
    [self createSideIndexes];
    [self createTablesData];
    
	NSString *hood = [[_checkLists objectAtIndex:0] name];
	NSString *zip = [[_checkLists objectAtIndex:1] name];
	NSString *area = [[_checkLists objectAtIndex:2] name];
	
	UISegmentedControl *segmentedControl =
        [[UISegmentedControl alloc] initWithItems:
            [NSArray arrayWithObjects: hood, zip, area, nil]];
	
	[segmentedControl setSelectedSegmentIndex: _selectedList];
    
	[segmentedControl addTarget:self action:@selector(changeLocationList:) 
        forControlEvents:UIControlEventValueChanged];
	
	//CGRect frame = [self.navigationController.toolbar bounds];
	//frame.size.height = frame.size.height - 13;
	//[segmentedControl setFrame:frame];
	
	self.navigationItem.titleView = segmentedControl;
	[segmentedControl release];
    
      [self performSelector:@selector(scrollToTop) withObject:self afterDelay:0.0 ];
 }

#pragma mark
#pragma scrollViewDidScroll helpers

// keeps top header visible
-(void)scrollViewSectionHeaders:(UIScrollView*)scrollView
{
    NSArray *visible = [_table indexPathsForVisibleRows];
    NSIndexPath *firstIndexPath = (NSIndexPath*)[visible objectAtIndex:0];
    
    NSInteger sectionNumber = firstIndexPath.section;
    UITableViewHeaderFooterView * header = [self.table headerViewForSection:sectionNumber];
    
    // the 108 is from statusBar height(20) + navigationBar height (44) + status bar height (44);
    
    while (header)
    {
        CGRect frame = header.frame;
        CGFloat delta = [self deltaYBetweenFrame:frame andPoint:scrollView.contentOffset];
        UITableViewHeaderFooterView * nextHeader = [self.table headerViewForSection:(sectionNumber+1)];
        
        // nextHeader may move header underneath searchBar
        // delta2 is the amount moved by nextHeader
        CGFloat delta2 = 0;
        if(nextHeader)
        {
            CGRect nextFrame = nextHeader.frame;
            CGFloat nextDelta = [self deltaYBetweenFrame:nextFrame andPoint:scrollView.contentOffset];
            if(nextDelta < (108 + 20)){
                delta2 = nextDelta - (108.0 + 20.0);
            }
        }
        
        // if header to move under searchBar, adjust its position
        if (delta < 108.0) {
            frame.origin.y += MAX(0, (108.0 + delta2) - delta);
            header.frame = frame;
        }
        [scrollView bringSubviewToFront:header];
        
        ++sectionNumber;
        header = [self.table headerViewForSection:sectionNumber];
    }
}

- (CGFloat)deltaYBetweenFrame:(CGRect)frame andPoint:(CGPoint)point
{
    CGFloat delta = frame.origin.y - point.y;
    return delta;
}

-(void)scrollViewScrolling:(UIScrollView*)scrollView
{
    UISearchBar *searchBar = self.searchDisplayController.searchBar;
    CGRect rect = searchBar.frame;
    
    if (scrollView.contentOffset.y > -64.0)
    {
        // this keeps locked below nav bar when scrolling table up
        rect.origin.y = MAX(0, scrollView.contentOffset.y + 64.0);
    }
    else
    {
        // this keeps locked below nav bar when scrolling table down
        rect.origin.y = MIN(0, scrollView.contentOffset.y + 64.0);
    }
    
    [scrollView layoutSubviews];
    searchBar.frame = rect;
    
    if (isIndexScroll)
    {
        [self performSelector:@selector(fixIndexOffset) withObject:self afterDelay:0.0 ];
        isIndexScroll = NO;
    }
    
    [scrollView bringSubviewToFront:searchBar];
}

// user touched inside UITextField in searchbar
-(void)scrollViewSearchModeStarting:(UIScrollView*)scrollView
{
    UISearchBar *searchBar = self.searchDisplayController.searchBar;
    CGRect rect = searchBar.frame;
    
    NSLog(@"table y offset %f", scrollView.frame.origin.y);
    
    if (scrollView.contentOffset.y > -64.0)
    {
        rect.origin.y = MAX(0, scrollView.contentOffset.y + 64.0 - 44);
        //NSLog(@"Search bar frame y %f", rect.origin.y);
    }
    
    [scrollView layoutSubviews];
    searchBar.frame = rect;
    
    [scrollView bringSubviewToFront:searchBar];
}

// user touched 'cancel' button. Note: 'cancel' text can change.
-(void)scrollViewSearchModeCanceled:(UIScrollView*)scrollView
{
    // do nothing for ios 7
}


// this keeps scrollbar locked to top
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (searchBarClicked) {
        return [self scrollViewSearchModeStarting:scrollView];
    }
    else if (self.searchDisplayController.isActive) {
        return;
    }
    else if (searchBarCancelClicked) {
        return [self scrollViewSearchModeCanceled:scrollView];
    }
    
    [self scrollViewScrolling:scrollView];
    
    [self scrollViewSectionHeaders:scrollView];
    
    UISearchBar *searchBar = self.searchDisplayController.searchBar;
    [scrollView bringSubviewToFront: searchBar];
}

- (void)fixIndexOffset
{
    CGPoint offset = [_table contentOffset];
    CGSize  size = [_table contentSize];
    CGFloat height = _table.frame.size.height;
    CGFloat distanceToBottom = size.height - offset.y;
    CGFloat remainingDistance = distanceToBottom - height;
    //NSLog(@"remainingDistance is %f",remainingDistance);
    
    CGFloat delta = 44.0f - remainingDistance;
    delta = MAX(0, delta);
    //NSLog(@"delta is %f",delta);
    
    CGFloat adjustedOffset = 44.0f - delta;
    
    if (adjustedOffset <=0.0f) {
        return;
    }
    
    offset.y -= adjustedOffset;
    
    [_table setContentOffset:offset animated:NO];
}

- (void)fixTableOffset
{
    [_table setContentOffset:CGPointMake(0, 0) animated:NO];
}

- (void)scrollToTop
{
    [_table setContentOffset:CGPointMake(0, -64) animated:NO];
}

- (void)fixSearchBarPosition
{
    CGPoint offset =  _table.contentOffset;
    ++offset.y;
    [_table setContentOffset:offset animated:NO];
    --offset.y;
    [_table setContentOffset:offset animated:NO];
}

# pragma mark
# pragma  UISearchDisplayDelegate

- (void)searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller
{
    searchBarClicked = NO;
    searchBarCancelClicked = YES;
}

- (void)searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller
{
    searchBarClicked = YES;
    
    // change button from 'cancel' to 'done'
    [controller.searchBar setShowsCancelButton:YES animated:NO];
    
    // for IOS 7
    UIButton *cancelButton = nil;
    UIView *topView = controller.searchBar.subviews[0];
    for (UIView *subView in topView.subviews) {
        if ([subView isKindOfClass:NSClassFromString(@"UINavigationButton")]) {
            cancelButton = (UIButton*)subView;
        }
    }
    if (cancelButton) {
        [cancelButton setTitle:@"Done" forState:UIControlStateNormal];
    }
    

    // for IOS 6.1
    for (UIView *subview in [controller.searchBar subviews]) {
        if ([subview isKindOfClass:[UIButton class]]) {
            [(UIButton *)subview setTitle:@"Done" forState:UIControlStateNormal];
        }
    }
}

- (void) searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller
{
    searchBarClicked = NO;
    searchBarCancelClicked = NO;
    [self fixSearchBarPosition];
}

// called when cancel button pressed
- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    searchBarCancelClicked = YES;
}

- (void)done
{
	UISegmentedControl *segment = (UISegmentedControl*)self.navigationItem.titleView;
	
	NSInteger index =  [segment selectedSegmentIndex];
	
	[self.delegate MultiCheckListController:self doneWithCheckList:_checkLists selectedListIndex:index];
}

- (void)reset
{
    NSMutableArray *list = [[_checkLists objectAtIndex: _selectedList] list];
    
    for (NSUInteger i=0; i< list.count; ++i) {
        
        ListItem *item = (ListItem *)[list objectAtIndex:i];
        if (i==0) {
            item.isSelected = YES;
        }
        else{
            item.isSelected = NO;
        }
    }
    
    //
    // the multiple offset calls keep the searchBar on top.
    // I have to change the offset for this to work,
    // so I add one, and subtract one to keep it in the
    // same spot
    //
    
    CGPoint offset = self.table.contentOffset;
    offset.y += 1;
    [self.table reloadData];
    [self.table setContentOffset:offset animated:NO];
    offset.y -= 1;
    [self.table setContentOffset:offset animated:NO];
}

- (void) changeLocationList:(id)sender
{	
	UISegmentedControl *segmentedControl = (UISegmentedControl *)sender;
	
	_selectedList = [segmentedControl selectedSegmentIndex];
    
	[_table reloadData];
    
   // [self.table setContentOffset:CGPointZero animated:NO];
    [self performSelector:@selector(scrollToTop) withObject:self afterDelay:0.0 ];
 }


#pragma mark - UISearchBarDelegate delegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    return YES;
}

#pragma mark - UISearchDisplayController delegate methods

- (void)searchDisplayController:(UISearchDisplayController *)controller didHideSearchResultsTableView:(UITableView *)tableView
{
    [_table reloadData];
    
}

- (void)searchDisplayController:(UISearchDisplayController *)controller willHideSearchResultsTableView:(UITableView *)tableView
{
    
    [_table reloadData];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller
    shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
          scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
          objectAtIndex:[self.searchDisplayController.searchBar
          selectedScopeButtonIndex]]];
    
    return YES;
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    [searchResults release];
    
    searchResults = [[NSMutableArray alloc] init];
    
    NSMutableArray *list = [[_checkLists objectAtIndex: _selectedList] list];
    
    for (int i = 0; i< [list count]; i++) {
        
        ListItem *item = [list objectAtIndex:i];
        
        BOOL found =  checkmatch(item.name, searchText);
        
        if (found) {
            
            [searchResults addObject:item];
        }
    }
}

// helper to find matches
BOOL checkmatch(NSString *item, NSString *matchphrase)
{
	BOOL match = NO;
	NSPredicate *containPred = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", matchphrase];
	match = match | [containPred evaluateWithObject:item];
	
	NSPredicate *matchPred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", matchphrase];
	match = match | [matchPred evaluateWithObject:item];
    
	return match;
}

#pragma mark - UITableViewDataSource delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{            
    NSMutableArray *list = nil;
    
    // handle selecting filtered location
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        list = searchResults;
    }
    else
    {
        list = [[_checkLists objectAtIndex: _selectedList] list];
    }
	
	ListItem *item = [list objectAtIndex:indexPath.row];
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    
    BOOL isAnyRow = checkmatch(selectedCell.textLabel.text, @"Any");
    
    if (isAnyRow)
    {
        if (selectedCell.accessoryType == UITableViewCellAccessoryCheckmark)
        {
            // already selected so don't do anything
            return;
        }
        else
        {
            // user selected 'any' so uncheck everything else
            
            item.isSelected = YES;
            
            // do the UI checkmark stuff here
            
            NSArray *visible = [_table indexPathsForVisibleRows];
            
            for (int i=0; i<visible.count; ++i)
            {
                NSIndexPath *theIndexPath = (NSIndexPath*)[visible objectAtIndex:i];
                
                UITableViewCell *cell = [tableView cellForRowAtIndexPath:theIndexPath];
                
                if(i==0)
                {
                    // check the any row
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                }
                else
                {
                    // uncheck everything else
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
            }
            
            // select any row, unselect everything else
    
            // use master list
            //
            list = [[_checkLists objectAtIndex: _selectedList] list];
            
            for (NSUInteger i=0; i< list.count; ++i)
            {                
                ListItem *item = (ListItem *)[list objectAtIndex:i];
                
                if (i==0)
                {
                    item.isSelected = YES;
                }
                else
                {
                    item.isSelected = NO;
                }
            }
            
            return;
        }
    }
    
    // if here selected non 'any' row
    
    list = [[_checkLists objectAtIndex: _selectedList] list];
    
    // update the UI for selected row
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    // find the selected item
    
    ListItem *selectedItem = nil;
    
    for (int i=0; i<list.count; ++i)
    {
        ListItem* item = (ListItem *)[list objectAtIndex:i];
        
        if ([item.name isEqualToString:cell.textLabel.text])
        {
            selectedItem = item;
            break;
        }
    }      
    
    if(cell.accessoryType == UITableViewCellAccessoryNone)
    {
        // check the any row
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        selectedItem.isSelected = YES;
    }
    else
    {
        // uncheck everything else
        cell.accessoryType = UITableViewCellAccessoryNone;
        selectedItem.isSelected = NO;
    }
    
    // Get the 'any' listItem we may have to change this below
    ListItem *anyItem = (ListItem *)[list objectAtIndex:0];
    
    // check if all the items in the list.
    // if at least one is checked, uncheck 'any' row
    // if none are checked, check 'any' row
    
    list = [[_checkLists objectAtIndex: _selectedList] list];
    BOOL atLeastOneItemChecked = NO;
    for (int i=1; i<list.count; ++i)
    {
        ListItem *item = [list objectAtIndex:i];
        
        if (item.isSelected)
        {
            atLeastOneItemChecked = YES;
            break;
        }
    }
    
    if (atLeastOneItemChecked)
    {
        anyItem.isSelected = NO;
    }
    else
    {
          anyItem.isSelected = YES;
    }
    
    // get the first row and look for 'any' row
    
    NSArray *visible = [_table indexPathsForVisibleRows];
    
    NSIndexPath *firstIndexPath = (NSIndexPath*)[visible objectAtIndex:0];
    
    UITableViewCell *firstCell = [tableView cellForRowAtIndexPath:firstIndexPath];
    
    // uncheck 'any' row if at least one other row is checked
    
    BOOL anyRowVisible = checkmatch(firstCell.textLabel.text, @"Any");
    
    if (anyRowVisible)
    {        
        if (cell.accessoryType == UITableViewCellAccessoryCheckmark)
        {
            firstCell.accessoryType = UITableViewCellAccessoryNone;
        }
        else
        {
            if (!atLeastOneItemChecked) {
                 firstCell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
                     
        }

    }

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        return 1;
    }
    
    NSInteger numberOfSections = [[self.tablesData objectAtIndex:self.selectedList] count];
    return numberOfSections;
    
	return 1; // the number of sections in the table
}

// 
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        return [searchResults count];        
    }
    else
    {
        NSMutableArray *sections = [self.tablesData objectAtIndex:self.selectedList];
        return [[sections objectAtIndex:section] count];
        
        return [[[_checkLists objectAtIndex:_selectedList] list] count];
    }
} 

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{		
	// used to identify cell as a normal cell
	static NSString *MyIdentifier = @"NormalCell";
	
	// get a reusable cell
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
	
	// if there are no cells to be reused, create one
	if (cell == nil)
    {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier] autorelease];
	}
    
    ListItem *item = nil;
    
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        item = [searchResults objectAtIndex:indexPath.row];
    }
    else
    {
       // NSMutableArray *list = [[_checkLists objectAtIndex: selectedList] list];
       // item = [list objectAtIndex:indexPath.row];
        
        NSMutableArray *sections = [self.tablesData objectAtIndex:self.selectedList];
        NSMutableArray *section = [sections objectAtIndex:indexPath.section];
        item = [section objectAtIndex:indexPath.row];
    }		
	
	cell.textLabel.text = item.name;
	
	if(item.isSelected)
    {
		cell.accessoryType = UITableViewCellAccessoryCheckmark;		
	}
    else
    {
		cell.accessoryType = UITableViewCellAccessoryNone;	
	}
	
    return cell;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        return nil;
    }
    NSMutableArray *sideIndexs = [self.sideIndexes objectAtIndex:self.selectedList];
    
    return [sideIndexs objectAtIndex:section];
    
    return @"hello";
}

// section names for index array on right side
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        return nil;
    }
    
    // don't index for zipcode
    if (_selectedList == 1) {
        return nil;
    }
    
    NSMutableArray *sectionTitles = [self.sideIndexes objectAtIndex:self.selectedList];
    
    return sectionTitles;
    
    /*
    alphabetsArray = [[NSMutableArray alloc] init];
    [alphabetsArray addObject:@"A"];
    [alphabetsArray addObject:@"B"];
    [alphabetsArray addObject:@"C"];
    [alphabetsArray addObject:@"D"];
    [alphabetsArray addObject:@"E"];
    [alphabetsArray addObject:@"F"];
    [alphabetsArray addObject:@"G"];
    [alphabetsArray addObject:@"H"];
    [alphabetsArray addObject:@"I"];
    [alphabetsArray addObject:@"J"];
    [alphabetsArray addObject:@"K"];
    [alphabetsArray addObject:@"L"];
    [alphabetsArray addObject:@"M"];
    [alphabetsArray addObject:@"N"];
    [alphabetsArray addObject:@"O"];
    [alphabetsArray addObject:@"P"];
    [alphabetsArray addObject:@"Q"];
    [alphabetsArray addObject:@"R"];
    [alphabetsArray addObject:@"S"];
    [alphabetsArray addObject:@"T"];
    [alphabetsArray addObject:@"U"];
    [alphabetsArray addObject:@"V"];
    [alphabetsArray addObject:@"W"];
    [alphabetsArray addObject:@"Y"];
    [alphabetsArray addObject:@"X"];
    [alphabetsArray addObject:@"Z"];
     */

  //  self.alphabetsArra = alphabetsArray;
    
    return self.alphabetsArray;
}

// section index to table section mapping. This could be different
//
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    isIndexScroll = YES;
    return index;
    
    NSMutableArray *dataArray = [[_checkLists objectAtIndex: _selectedList] list];
    
    if(self.searchDisplayController.isActive)
    {
        dataArray = searchResults;
    }

    NSInteger row = 0;
	
    for (int i = 0; i< [dataArray count]; i++) {
        
        ListItem *item = [dataArray objectAtIndex:i];
        NSString *letterString = [item.name substringToIndex:1];
        
        if ([item.name rangeOfString:@"-"].location != NSNotFound)
        {
            // use number
            int number = [item.name intValue];
            letterString = [NSString stringWithFormat:@"%d", number];
        }
  
        if ([letterString isEqualToString:title])
        {
            isIndexScroll = YES;
            [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:i-1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
            
          //  [tableView setContentOffset:CGPointMake(0,i*44) animated:YES];
            row = i;
            break;
        }
    }
    return row;
}

#pragma mark - Build data for side indexs

- (void)createSideIndexes
{
    if (self.sideIndexes != nil) {
        [_sideIndexes release];
    }
    
    self.sideIndexes = [[[NSMutableArray alloc] initWithCapacity:3] autorelease];
    
    for (int i=0; i < _checkLists.count; ++i)
    {
        NSMutableArray *dataArray = [[_checkLists objectAtIndex: i] list];
        NSMutableArray *indexs = [[NSMutableArray alloc] init];
        
        // loop through data to build indexs
        
        for (int i = 0; i < [dataArray count]; i++) {
            
            ListItem *item = [dataArray objectAtIndex:i];
            
            // use first letter
            
            NSString *letterString = [item.name substringToIndex:1];
            
            // if item name containg a '-' handle differently
            
            if ([item.name rangeOfString:@"-"].location != NSNotFound)
            {
                // use number
                int number = [item.name intValue];
                letterString = [NSString stringWithFormat:@"%d", number];
            }
            
            // if letter not in array, add it
            
            if (![indexs containsObject:letterString]) {
                [indexs addObject:letterString];
            }
        }
        
        // finished building indexs
        
        // add to sideIndexs
        
        [self.sideIndexes addObject:indexs];
        [indexs release];
    }
}


- (void)createAlphabetArray
{
    if(self.alphabetsArray)
    {
        [_alphabetsArray release];
    }
    
    NSMutableArray *dataArray = [[_checkLists objectAtIndex: _selectedList] list];
    NSMutableArray *tempFirstLetterArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [dataArray count]; i++) {
        
        ListItem *item = [dataArray objectAtIndex:i];
        
        NSString *letterString = [item.name substringToIndex:1];
        
        if ([item.name rangeOfString:@"-"].location != NSNotFound)
        {
            // use number
            int number = [item.name intValue];
            letterString = [NSString stringWithFormat:@"%d", number];
        }
        
        if (![tempFirstLetterArray containsObject:letterString]) {
            [tempFirstLetterArray addObject:letterString];
        }
    }
    self.alphabetsArray = tempFirstLetterArray;
    [tempFirstLetterArray release];
    
    [self createTableSectionsFromIndexes:self.alphabetsArray];
}

- (void)createTablesData
{
    NSInteger numberOfTables = self.sideIndexes.count;
    
    if (_tablesData != nil) {
        [_tablesData release];
    }
    self.tablesData = [[[NSMutableArray alloc] initWithCapacity:numberOfTables] autorelease];
    
    for (int i=0; i<numberOfTables; ++i)
    {
        NSMutableArray *indexes = [self.sideIndexes objectAtIndex:i];
        
        NSMutableArray *sections = [[NSMutableArray alloc] initWithCapacity:indexes.count];
        
        // create a dictionary of the sections
        
        NSMutableDictionary *dicOfSections = [[NSMutableDictionary alloc] initWithCapacity:indexes.count];
        
        // build a section dictionary of the index names for fast lookup
        
        for (int i = 0; i < indexes.count; ++i)
        {
            NSString *key = [indexes objectAtIndex:i];
            NSMutableArray *section = [[NSMutableArray alloc] init];
            
            [dicOfSections  setObject:section forKey:key];
            [sections addObject:section];
            [section release];
        }
        
        // put the items in the matching section
        
        NSMutableArray *dataArray = [[_checkLists objectAtIndex: i] list];
        
        for (int i = 0; i < [dataArray count]; i++)
        {
            ListItem *item = [dataArray objectAtIndex:i];
            
            NSString *letterString = [item.name substringToIndex:1];
            
            if ([item.name rangeOfString:@"-"].location != NSNotFound)
            {
                // use number
                int number = [item.name intValue];
                letterString = [NSString stringWithFormat:@"%d", number];
            }
            
            NSMutableArray *aSection = [dicOfSections objectForKey:letterString];
            
            // add item to section
            [aSection addObject:item];
        }
        
        // save finished sections table
        [self.tablesData addObject:sections];
        
        // free memory
        [dicOfSections release];
        [sections release];
        
    } // end table loop
    
    
   // finished building tableData        
}

- (void)createTableSectionsFromIndexes:(NSMutableArray*)indexes
{
    NSMutableArray *sections = [[[NSMutableArray alloc] initWithCapacity:indexes.count] autorelease];
    
    // get items array to index
    
    NSMutableArray *dataArray = [[_checkLists objectAtIndex: _selectedList] list];
    
    // create a dictionary of the sections
    
    NSMutableDictionary *dicOfSections = [[[NSMutableDictionary alloc] initWithCapacity:indexes.count] autorelease];
    
    // build a section dictionary of the index names for fast lookup
    
    for (int i = 0; i < indexes.count; ++i)
    {
        NSString *key = [indexes objectAtIndex:i];
        NSMutableArray *section = [[[NSMutableArray alloc] init] autorelease];
        [dicOfSections  setObject:section forKey:key];
        [sections addObject:section];
    }
    
    // put the items in the matching section
    
    for (int i = 0; i < [dataArray count]; i++)
    {        
        ListItem *item = [dataArray objectAtIndex:i];
        
        NSString *letterString = [item.name substringToIndex:1];
        
        if ([item.name rangeOfString:@"-"].location != NSNotFound)
        {
            // use number
            int number = [item.name intValue];
            letterString = [NSString stringWithFormat:@"%d", number];
        }
        
        NSMutableArray *aSection = [dicOfSections objectForKey:letterString];
        
        [aSection addObject:item];
    }

    
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [searchResults release];
    [_sideIndexes release];
    [_alphabetsArray release];
    [_tablesData release];
    [_checkLists release];
    [super dealloc];
}


@end
