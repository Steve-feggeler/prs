//
//  CheckListItem.m
//  R1
//
//  Created by stephen feggeler on 2/16/13.
//  Copyright 2013 self. All rights reserved.
//

#import "CheckListItem.h"

@implementation CheckListItem

-(void)dealloc{
    [_name release];
    [_list release];
    [super dealloc];
}

@end
