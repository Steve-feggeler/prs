//
//  ListItem.h
//  R1
//
//  Created by stephen feggeler on 1/26/13.
//  Copyright 2013 self. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ListItem : NSObject <NSCoding> {

	//NSString *name;
	//NSString *key;
	//BOOL isSelected;
}

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *key;
@property (nonatomic, assign) BOOL isSelected;

@end
