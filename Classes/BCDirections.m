//
//  BCDirections.m
//  R2
//
//  Created by stephen feggeler on 9/6/13.
//
//

#import "BCDirections.h"

@implementation BCDirections

+ (void)GetDirectionsLocation:(CLLocationCoordinate2D)location
{
    MKPlacemark* place = [[[MKPlacemark alloc] initWithCoordinate: location addressDictionary: nil] autorelease];
    MKMapItem* destination = [[[MKMapItem alloc] initWithPlacemark: place] autorelease];
    destination.name = @"Name Here!";
    NSArray* items = [[[NSArray alloc] initWithObjects: destination, nil] autorelease];
    NSDictionary* options = [[[NSDictionary alloc] initWithObjectsAndKeys:
                             MKLaunchOptionsDirectionsModeDriving,
                             MKLaunchOptionsDirectionsModeKey, nil] autorelease];
    [MKMapItem openMapsWithItems: items launchOptions: options];
}

@end
