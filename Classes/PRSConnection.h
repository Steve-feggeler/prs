//
//  RemoteConnection.h
//  R2
//
//  Created by stephen feggeler on 2/28/13.
//  Copyright 2013 self. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PRSConnectionDelegate;

@interface PRSConnection : NSObject {
	
	id <PRSConnectionDelegate> delegate;
	NSMutableData *receivedData;
}

@property (nonatomic, assign) id <PRSConnectionDelegate> delegate;

- (void)performSearch:(NSString *)search;

@end

@protocol PRSConnectionDelegate

- (void)prsConnection:(PRSConnection *)connection
       didReceiveData:(NSData *)data;
- (void)prsConnection:(PRSConnection *)connection
       didFailWithError:(NSError *)error;

@end

