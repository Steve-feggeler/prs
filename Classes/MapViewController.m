//
//  MapViewController.m
//  R2
//
//  Created by stephen feggeler on 3/26/13.
//  Copyright 2013 self. All rights reserved.
//
#import "NSTextFixes.h"
#import "UIImageView+WebCache.h"
#import "MapViewController.h"
#import "Annotation.h"
#import "SFPropertiesPinView.h"
#import "BCDirections.h"
#import "BasicMapAnnotationView.h"
#import "CalloutMapAnnotationView.h"
#import "PropertyCalloutView.h"
#import "PropertyDetailsViewController.h"
#import "StreetViewController.h"
#import "NSTextFixes.h"

#define CURRENT_PAGE_SIZE 50

@interface MapViewController ()

@property (retain, nonatomic) NSMutableDictionary * annotationsAtLocation;
@property (retain, nonatomic) NSMutableDictionary * propertyWithMLN;
@property (retain, nonatomic) NSMutableDictionary * blockedPropertyWithMLN;
@property (assign, nonatomic) NSInteger numberPropertiesBlocked;
@property (assign, nonatomic) BOOL allPropertiesDownloaded;
@property (assign, nonatomic) BOOL usePageFetchForDownload;
@property (retain, nonatomic) CLLocation *lastLocation;
@property (assign, nonatomic) BOOL blockCalloutRemoval;

@end

@implementation MapViewController

@synthesize properties;
@synthesize mapView;
@synthesize totalProperties;
@synthesize resetMap;
@synthesize calloutMapAnnotation;
@synthesize selectedAnnotationView;
@synthesize callouts;
@synthesize prevCenter;
@synthesize postCMD;
@synthesize isAppend;
@synthesize locationManager;
@synthesize selectedPropertyIndex;

#pragma mark -
#pragma mark find annotations/properties on map

- (Annotation*)annotationForProperty:(Property*)property{
    
    // get coordinate
    CLLocationCoordinate2D loc = property.coordinate;
    
    // encode to value
    NSValue *coordinateValue = [NSValue valueWithBytes:&loc objCType:@encode(CLLocationCoordinate2D)];
    
    // look in dictionary for coordinateValue
    Annotation *ann = self.annotationsAtLocation[coordinateValue];
    
    return ann;
}

-(NSMutableArray*)getVisiblePropertiesOnMap
{
    NSArray *visibleAnnotationsOnMap = [[self.mapView annotationsInMapRect:self.mapView.visibleMapRect] allObjects];
    
    // don't return user location annotation
    
    NSMutableArray *visibleProperties = [[[NSMutableArray alloc] init] autorelease];
    for (id element in visibleAnnotationsOnMap){
        if ([element isKindOfClass:[Annotation class]]) {
            Annotation * ann = (Annotation*)element;
            for (Property * p in ann.properties)
                [visibleProperties addObject:p];
        }
    }
    
    return visibleProperties;
}

- (NSInteger)numberBlockedInRegion{
    
    NSMutableArray *props = [NSMutableArray arrayWithArray:[self.blockedPropertyWithMLN allValues]];
    NSInteger number_blocked_in_region = 0;
    
    for (Property * p in props) {
        
        float d_lat = [p.latitude floatValue] - self.mapView.region.center.latitude;
        float d_lon = [p.longitude floatValue] - self.mapView.region.center.longitude;
        
        BOOL inside_of_lat = fabsf(d_lat) <= self.mapView.region.span.latitudeDelta/2;
        BOOL inside_of_lon = fabsf(d_lon) <= self.mapView.region.span.longitudeDelta/2;
        
        if (inside_of_lat && inside_of_lon) {
            ++number_blocked_in_region;
        }
    }
    return number_blocked_in_region;
}

- (NSMutableArray*)copyOfBlockedPropertiesOnMap{
    
    NSMutableArray *props = [NSMutableArray arrayWithArray:[self.blockedPropertyWithMLN allValues]];
    NSMutableArray *blockedProperties = [[NSMutableArray alloc] init];
    
    for (Property * p in props) {
        
        float d_lat = [p.latitude floatValue] - self.mapView.region.center.latitude;
        float d_lon = [p.longitude floatValue] - self.mapView.region.center.longitude;
        
        BOOL inside_of_lat = fabsf(d_lat) <= self.mapView.region.span.latitudeDelta/2;
        BOOL inside_of_lon = fabsf(d_lon) <= self.mapView.region.span.longitudeDelta/2;
        
        if (inside_of_lat && inside_of_lon) {
            [blockedProperties addObject:p];
        }
    }
    return blockedProperties;
}

#pragma mark -
#pragma mark add/remove properties on map

- (void)removeAllPropertiesOnMap
{
    id userLocation = [self.mapView userLocation];
    NSMutableArray *annotations = [[NSMutableArray alloc] initWithArray:[self.mapView annotations]];
    
    if ( userLocation != nil ) {
        [annotations removeObject:userLocation]; // don't remove user location
    }
    
    // unselect all pins
    NSArray * selected_anns = [self.mapView selectedAnnotations];
    for (Annotation * ann in selected_anns) {
        [self.mapView deselectAnnotation:ann animated:NO];
    }
	
    [self.mapView removeAnnotations:annotations];
    
    [self.annotationsAtLocation removeAllObjects];
    [self.propertyWithMLN removeAllObjects];
    [self.blockedPropertyWithMLN removeAllObjects];
    
    [annotations release];
    annotations = nil;
    
    // reset since now no properties
    self.allPropertiesDownloaded = NO;
    
    // reset
    self.usePageFetchForDownload = NO;
}

- (void)addPropertyToMap:(Property*)property{
    
    if (!self.annotationsAtLocation) {
        self.annotationsAtLocation = [NSMutableDictionary dictionary];
    }
    
    if (!self.propertyWithMLN) {
        self.propertyWithMLN = [NSMutableDictionary dictionary];
    }
    
    if (!self.blockedPropertyWithMLN) {
        self.blockedPropertyWithMLN = [NSMutableDictionary dictionary];
    }
    
    int mln = [[property mln] intValue];
    NSValue *mlnValue = [NSValue valueWithBytes:&mln objCType:@encode(int)];
    
    BOOL dont_show_address = [property.showAddressToPublic isEqualToString:@"0"];
    BOOL lat_nil =  property.latitude == nil;
    BOOL lat_zero = 0.0 == [property.latitude floatValue];
    
    // check for valid/blocked address
    if (dont_show_address ||  lat_nil || lat_zero){
        
        // keep list of blocked/invalid addressed
        if(!self.blockedPropertyWithMLN[mlnValue]){
            self.blockedPropertyWithMLN[mlnValue] = property;
        }
        return;
    }
    
    // add property to propertyWithMLN dictionary if not there
    // don't add property more than once!
    // check if already on map.  Use propertyWithMLN dictionary
    
    if(!self.propertyWithMLN[mlnValue]){
        self.propertyWithMLN[mlnValue] = property;
    }
    else{
        // property already on map, so nothing to do.
        return;
    }
    
    // get coordinate
    CLLocationCoordinate2D loc = property.coordinate;
    
    // encode to value
    NSValue *coordinateValue = [NSValue valueWithBytes:&loc objCType:@encode(CLLocationCoordinate2D)];
    
    // look in dictionary for coordinateValue
    Annotation *ann = self.annotationsAtLocation[coordinateValue];
    
    // if not found, create annotation, add propety in annotation
    if (!ann) {
        
        Annotation *ann = [[[Annotation alloc]initWithLocation:loc] autorelease];
        
        self.annotationsAtLocation[coordinateValue] = ann;
        
        // add property to annotation
        NSMutableArray *propertiesAtLoc = [[[NSMutableArray alloc] init] autorelease];
        
        // properties
        [propertiesAtLoc addObject:property];
        
        // add properties to annotation
        ann.properties = propertiesAtLoc;
        
        // add annotation to map
        [self.mapView addAnnotation:ann];
    }
    
    // location already exist, so add to pin
    else
    {
        [ann.properties addObject:property];
        
        SFPropertiesPinView * pin = (SFPropertiesPinView *)[self.mapView viewForAnnotation:ann];
        
        // pin may not be visible on map
        if (pin) {
            
            // update pin text
            [pin updatePrice];
        }
    }
}

- (void)RemovePropertiesFromMap:(NSMutableArray*)pinProperties{
    
    if (!self.annotationsAtLocation) {
        return;
    }
    
    for (Property *p in pinProperties) {
        [self RemovePropertyFromMap: p];
    }
}

- (void)RemovePropertyFromMap:(Property*)property{
    
    if (!self.annotationsAtLocation) {
        return;
    }
    
    // remove property from propertyWithMLN dictionary
    int mln = [[property mln] intValue];
    NSValue *mlnValue = [NSValue valueWithBytes:&mln objCType:@encode(int)];
    if(!self.propertyWithMLN[mlnValue]){
        [self.propertyWithMLN removeObjectForKey:mlnValue];
    }
    
    // get coordinate
    CLLocationCoordinate2D loc = property.coordinate;
    
    // encode to value
    NSValue *coordinateValue = [NSValue valueWithBytes:&loc objCType:@encode(CLLocationCoordinate2D)];
    
    // look in dictionary for coordinateValue
    Annotation *ann = self.annotationsAtLocation[coordinateValue];
    
    // if not found, create annotation, add propety in annotation
    if (!ann) {
        
        return;
    }
    
    // location exist, so remove property
    else
    {
        [ann.properties removeObject:property];
        
        // if no propertys with pin, remove
        
        if(ann.properties.count==0){
            [self.mapView removeAnnotation:ann];
        }
        
        // update the pin with the removed property
        
        else{
        
            SFPropertiesPinView * pin = (SFPropertiesPinView *)[self.mapView viewForAnnotation:ann];
        
            // pin may not be visible on map
            if (!pin) {
            
                // update pin text
                [pin updatePrice];
            }
        }
    }
}

#pragma mark -
#pragma mark - SFPropertyPinDelegate

- (void)mapView:(MKMapView *)mapView detailsForProperty:(Property *)property{
	
	PropertyDetailsViewController *detailViewController =
    [[PropertyDetailsViewController alloc] initWithNibName:@"PropertyDetailsViewController" bundle:nil];
	
    NSMutableArray *shown = [self getVisiblePropertiesOnMap];
    NSMutableArray *hidden = [[self copyOfBlockedPropertiesOnMap] autorelease];
    
    NSMutableArray *shownAndHiddenProps = [NSMutableArray arrayWithArray:shown];
    
    [shownAndHiddenProps addObjectsFromArray:hidden];
    
    // sort in desending order using price and mln
    
    [shownAndHiddenProps sortUsingComparator:
     ^NSComparisonResult(id obj1, id obj2){
         
         Property *p1 = (Property*)obj1;
         Property *p2 = (Property*)obj2;
         NSInteger n1 = [p1 priceNumber];
         NSInteger n2 = [p2 priceNumber];
         if (n1 > n2) {
             return (NSComparisonResult)NSOrderedAscending;
         }
         
         if (n1 < n2) {
             return (NSComparisonResult)NSOrderedDescending;
         }
         
         NSInteger mln1 = [p1.mln integerValue];
         NSInteger mln2 = [p2.mln integerValue];
         
         // if here price is same so use mln
         
         if (mln1 > mln2) {
             return (NSComparisonResult)NSOrderedAscending;
         }
         
         if (mln1 < mln2) {
             return (NSComparisonResult)NSOrderedDescending;
         }
         
         return (NSComparisonResult)NSOrderedSame;
       }
     ];
    
	detailViewController.properties = shownAndHiddenProps;
	detailViewController.indexOfProperty = [shownAndHiddenProps indexOfObject:property];
	detailViewController.totalProperties = shownAndHiddenProps.count;
    detailViewController.selectedPropertyDelegate = self;
    detailViewController.blockFetchMoreData = YES;
    detailViewController.hidesBottomBarWhenPushed = YES;
    
    self.navigationItem.backBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil] autorelease];
	
    // Pass the selected object to the new view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
	
    [detailViewController release];
    
    self.blockCalloutRemoval = YES;
}

- (void)mapView:(MKMapView *)mapView streetViewForProperty:(Property *)property{
    
    StreetViewController *streetViewController =
    [[[StreetViewController alloc] initWithNibName:@"StreetView" bundle:nil] autorelease];
    
    streetViewController.lat = property.latitude;
    streetViewController.lon = property.longitude;
    streetViewController.title = property.detials1;
    streetViewController.subtitle = [NSString stringWithFormat:@"%@, %@", property.address1, property.address2];
    streetViewController.hidesBottomBarWhenPushed = YES;
    
     self.navigationItem.backBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil] autorelease];
    
    [self.navigationController pushViewController:streetViewController animated:YES];
    
    self.blockCalloutRemoval = YES;
}

- (void)mapView:(MKMapView *)mapView directionsToProperty:(Property *)property{
    
    CLLocationCoordinate2D location;
    location.latitude = [property.latitude doubleValue];
    location.longitude = [property.longitude doubleValue];
    [BCDirections GetDirectionsLocation:location];
}

#pragma mark -
#pragma mark MKMapViewDelegate

- (void)removeCallout{

    // remove callout
    for (NSObject<MKAnnotation> *ann in [self.mapView annotations]) {
        
        if (ann == mapView.userLocation){
            [self.mapView deselectAnnotation:ann animated:NO];
            continue;
        }
        
        MKAnnotationView *av = [mapView viewForAnnotation: ann];
        
        // just deselect for all classes but SFPropertiesPinView
        if (![av isKindOfClass:[SFPropertiesPinView class]]) {
            [self.mapView deselectAnnotation:ann animated:NO];
            continue;
        }
        
        // remove callout here
        SFPropertiesPinView* pinView = (SFPropertiesPinView*)[mapView viewForAnnotation: ann];
        if (pinView){
            [self.mapView deselectAnnotation:ann animated:NO];
            [pinView removeCallout];
          
        }
    }
}

- (void)didDragMap:(UIGestureRecognizer*)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan){
        [self removeCallout];
    }
}

-(void)mapView:(MKMapView *)map regionWillChangeAnimated:(BOOL)animated{
    
    return;
    
    if (!self.blockCalloutRemoval) {
        [self removeCallout];
    }
}

- (void)mapView:(MKMapView *)map regionDidChangeAnimated:(BOOL)animated {
    
    if (self.properties == nil) {
        return;
    }
    
    if (self.allPropertiesDownloaded) {
        [self updateTitle];
        return;
    }
    
	CGFloat lat_diff = fabs( map.region.center.latitude - prevRegion.center.latitude);
	CGFloat lon_diff = fabs( map.region.center.longitude - prevRegion.center.longitude);
	
	CGFloat diff = sqrt(lat_diff + lon_diff);
	
	if(diff >= 0.002 )
    {
		prevRegion.center = map.region.center;
		[self updateMap];
	}
	
    /*
	NSLog(@"lat = %f, lon = %f, dlat = %f, dlon = %f",
		  map.region.center.latitude,
		  map.region.center.longitude,
		  map.region.span.latitudeDelta,
		  map.region.span.longitudeDelta);
    */
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
	
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
		
}

- (void)unblockSetRegion:(id)object
{
    blockSetRegion = NO;
}

-(void)removeCallout:(id)object
{
	if (callouts.count == 0) {
		return;
	}
	
	CalloutMapAnnotation *callout = (CalloutMapAnnotation*)[self.callouts objectAtIndex:0];
	
	[self.callouts removeObjectAtIndex:0];
	
	[self.mapView removeAnnotation: callout];
    
    theRegion = self.mapView.region;
    
    if (blockSetRegion == NO) {
        [self performSelector:@selector(setRegion:) withObject:nil afterDelay:0.05];
    }    
}

- (void)updateMap
{				
	MKCoordinateRegion region = mapView.region;
	
	region.span.latitudeDelta = mapView.region.span.latitudeDelta/2.0;
	region.span.longitudeDelta = mapView.region.span.longitudeDelta/2.0;
	
	if(region.span.latitudeDelta < 0.0001)
	{		
		[self setPropertyRegion];
	}
	else 
	{
		R2AppDelegate *appDelegate = (R2AppDelegate *)[[UIApplication sharedApplication] delegate];
        if (self.usePageFetchForDownload) {
            [appDelegate fetchMoreProperties];
        }
        else{
            [appDelegate fetchPropertiesInRegion: region];
        }
	}
}

- (MKAnnotationView *)mapView:(MKMapView *)aMapView viewForAnnotation:(id <MKAnnotation>)annotation {
	
    // If it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;  //return nil to use default blue dot view
    
    //MKAnnotationView *pin = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotationView"];
    //pin.image = [UIImage imageNamed:@"pin.png"];
    //pin.canShowCallout = YES;
    //return pin;
    
    // Handle any custom annotations.
    if ([annotation isKindOfClass:[Annotation class]]){
        
        // Try to dequeue an existing pin view first.
        SFPropertiesPinView* pinView = (SFPropertiesPinView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"SFPropertiesPinView"];
        if (!pinView){
            
            pinView = [[SFPropertiesPinView alloc] initWithAnnotation:annotation reuseIdentifier:@"SFPropertiesPinView"];
        }
        
        pinView.annotation = annotation;
        
        [pinView setCanShowCallout:NO];
        
        pinView.calloutPosition = SFAvoveOrBelowCalloutPosition;
        pinView.map = mapView;
        pinView.opaque = NO;
        pinView.delegate = self;
        
        [pinView updatePrice];
        
        return pinView;
    }
    
    return nil;
}

- (void)showCalloutForPropertyIndex:(Property*)property
{
    // find annotation for property
    
    Annotation * ann = [self annotationForProperty:property];
    
    // show the annotion if found
    
    if(ann)
    {
        [mapView selectAnnotation:ann animated:NO];
        SFPropertiesPinView* pinView = (SFPropertiesPinView*)[mapView viewForAnnotation: ann];
        [pinView mapView:self.mapView selectedProperty:property];        
    }
    else{
        for (NSObject<MKAnnotation> *annotation in [mapView selectedAnnotations]) {
            [mapView deselectAnnotation:(id <MKAnnotation>)annotation animated:NO];
        }
    }
}

-(void)setRegion:object 
{
	[self.mapView setRegion:theRegion];		
}

#pragma mark -
#pragma mark PropertyFinderDelegate

- (void)propertyFinder:(PropertyFinder *)finder didFailWithError:(NSError *)error
{
    
}

- (void)propertyFinder:(PropertyFinder *)finder didFindProperties:(NSMutableArray *)newProperties inRange:(NSRange)range ofTotal:(NSInteger) total
{
    if (self.isAppend) {
        
        [self appendProperties:newProperties ofTotal:total];
        return;
    }
    
    blockSetRegion = YES;
    
    [self removeAllPropertiesOnMap];
    
    // set the new list
    
    self.properties = newProperties;
	self.totalProperties = total;
    
    if (total  <= CURRENT_PAGE_SIZE) {
        self.allPropertiesDownloaded = YES;
    }
    
    // if only one more download needed, download the page,
    // not the region
    
    else if(total <= (2 * CURRENT_PAGE_SIZE)){
        self.usePageFetchForDownload = YES;
    }
			
	[self setPropertyRegion];
    [self updateTitle];
}

#pragma mark -
#pragma mark PropertyFinderDelegate helpers

- (void)appendProperties:(NSMutableArray*)newProperties ofTotal:(NSInteger)total
{
    // if this is a page fetch, check if this is all the downloads.
    // if it is, then no more downloads
    
    if (self.usePageFetchForDownload) {
        if (newProperties.count == total) {
            self.allPropertiesDownloaded = YES;
        }
    }
    
    // keep properties already on map, but don't duplicate
  /*
    NSMutableArray *keepOnMap = [[[NSMutableArray alloc] init] autorelease];
    
    for (Property *p in self.properties){
        
        float d_lat = [p.latitude floatValue] - mapView.region.center.latitude;
        float d_lon = [p.longitude floatValue] - mapView.region.center.longitude;
        
        BOOL inside_of_lat = fabsf(d_lat) <= mapView.region.span.latitudeDelta/2;
        BOOL inside_of_lon = fabsf(d_lon) <= mapView.region.span.longitudeDelta/2;
        
        // keep properties on map
        
        if( inside_of_lat && inside_of_lon ){
            
            BOOL already_in_list = NO;
            
            for (Property *newProperty in newProperties){
                
                if( [newProperty.mln intValue] == [p.mln intValue]){
                    already_in_list = YES;
                }
            }
            
            if(already_in_list == NO){
                
                [keepOnMap addObject:p];
            }
        }
    }
    
    // combine new list with keepOnMap
    //NSMutableArray *theProperties = [[newProperties mutableCopy] autorelease];
     NSMutableArray *theProperties = newProperties;
    
    [theProperties addObjectsFromArray:keepOnMap];        
    
    // set the new list
    
	self.properties = theProperties;
   */
    
    for (Property * new_p in newProperties) {
        
        [self addPropertyToMap:new_p];
    }
    
	self.totalProperties = total;
	
	[self updateTitle];
//	[self setPropertyRegion];
}

- (void)updateTitle
{
	NSString *results = @"No Properties";
    
    NSInteger blocked_in_region = [self numberBlockedInRegion];
	
	if (self.totalProperties>0) {
        NSInteger visible_on_map = [self getVisiblePropertiesOnMap].count;
        
        NSInteger total_properties_in_region = 0;
        if (self.allPropertiesDownloaded) {
            total_properties_in_region = visible_on_map;
        }
        else{
            total_properties_in_region = self.totalProperties - blocked_in_region;
        }
        
        if (visible_on_map > total_properties_in_region) {
            total_properties_in_region = visible_on_map; // this happens rarely
        }
        
        if (visible_on_map < total_properties_in_region) {
            self.navigationItem.prompt = @"Zoom in to see more properties.";
        }
        else {
            self.navigationItem.prompt = nil;
        }
        
        if (blocked_in_region>0)
        {
            if (total_properties_in_region>0)
            {
                if (visible_on_map == total_properties_in_region)
                {
                    results = [NSString stringWithFormat:@"%ld Properties, %ld Hidden",(long)visible_on_map, (long)blocked_in_region];
                }
                else
                {
                    results = [NSString stringWithFormat:@"%ld of %ld Properties, %ld Hidden", (long)visible_on_map, (long)total_properties_in_region, (long)blocked_in_region];
                }
            }
            else
            {
                results = [NSString stringWithFormat:@"%ld Properties Hidden",  (long)blocked_in_region];
            }
        }
        else if(total_properties_in_region>0)
        {
            if (visible_on_map == total_properties_in_region)
            {
                results = [NSString stringWithFormat:@"%ld Properties", (long)total_properties_in_region];
            }
            else
            {
                results = [NSString stringWithFormat:@"%ld of %ld Properties", (long)visible_on_map, (long)total_properties_in_region];
            }
        }
        else{
            // no properties
        }
	}
	
    //self.navigationItem.title = [NSString stringWithFormat:@"Map (%@)", results];
    self.navigationItem.title = [NSString stringWithFormat:@"%@", results];
}

-(void)findLocationAndAddPropertyToMap:(Property*)property{
    
    NSString *location = [NSString stringWithFormat:@"%@, %@",property.address1, property.address2];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:location
                 completionHandler:^(NSArray* placemarks, NSError* error){
                     if (placemarks && placemarks.count > 0) {
                         CLPlacemark *topResult = [placemarks objectAtIndex:0];
                         MKPlacemark *placemark = [[MKPlacemark alloc] initWithPlacemark:topResult];
                         
                         // if not in san francisco, don't display property
                         if (![self isInSanFrancisco:placemark.location.coordinate]) {
                             return;
                         }
                         
                         // If this takes a long time, this property could be freed
                         @try {
                             property.latitude = [NSString stringWithFormat:@"%f", placemark.location.coordinate.latitude];
                             property.longitude = [NSString stringWithFormat:@"%f", placemark.location.coordinate.longitude];
                             
                             self.resetMap = YES;
                             [self setPropertyRegion];
                                                    }
                         @catch (NSException *exception) {
                                                    }
                         @finally {
                            
                         }
                                                }
                 }
     ];
}

-(void)setPropertyRegion
{
	if ((self.properties == nil)||(self.properties.count==0)) {
        
        [self removeAllPropertiesOnMap];        
        [self setRegionToSanFrancisco];
        
		return;
	}
	
	CGFloat minLat = CGFLOAT_MAX;
	CGFloat maxLat = -CGFLOAT_MAX;
	CGFloat minLon = CGFLOAT_MAX;
	CGFloat maxLon = -CGFLOAT_MAX;
    
	CGFloat sumLat = 0;
	CGFloat sumLon = 0;
	
	// add properties
    
	for (Property *property in self.properties) {
		
		CGFloat lat = [property.latitude floatValue];
		CGFloat lon = [property.longitude floatValue];
        
        BOOL isInSF = [self isInSanFrancisco:property.coordinate];
        
        if (lat == 0.0 || lon == 0.0 || (isInSF == NO)) {
            [self findLocationAndAddPropertyToMap:property];
            continue;
        }
		
        [self addPropertyToMap:property];
		
		sumLat += lat;
		sumLon += lon;
		
		minLat = MIN(minLat, lat);
		maxLat = MAX(maxLat, lat);
		minLon = MIN(minLon, lon);
		maxLon = MAX(maxLon, lon);
	}
	
	if(resetMap == NO){
		return;
	}
	resetMap = NO;
    
    CGFloat centerLat = (maxLat + minLat) / 2.;
    CGFloat centerLon = (maxLon + minLon) / 2.;
    
    // set prev here
    prevRegion.center.latitude = centerLat;
    prevRegion.center.longitude = centerLon;
    
    float spread = 1.3; // 1.1
	CGFloat latDelta = (maxLat - minLat) * spread;
	CGFloat lonDelta = (maxLon - minLon) * spread;
    
    latDelta = MAX(latDelta, 0.03);
    lonDelta = MAX(lonDelta, 0.03);
	
	CLLocationCoordinate2D coordinate = {centerLat, centerLon};
	[self.mapView setRegion:
     MKCoordinateRegionMake(coordinate,
        MKCoordinateSpanMake(latDelta, lonDelta))];
}

#pragma mark -
#pragma mark MapViewController creation


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

- (UIButton*)currentLocationButton{
    
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    UIImage *image = [UIImage imageNamed:@"193-location-arrow"];
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    [button setImage:image forState:UIControlStateNormal];
    [button addTarget:self action:@selector(setToCurrentLocation) forControlEvents:UIControlEventTouchUpInside];
    //[button setFrame:CGRectMake(280, 25, 50, 34)];
    [button setFrame:CGRectMake(280, 25, 22, 34)];
    
    // make dark image white
    button.tintColor = [UIColor whiteColor];
    
    return button;
    
    /*
    // uncomment for dark image with border
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    
    [button.layer setBackgroundColor: [[UIColor colorWithRed:237/255.0f green:237/255.0f blue:237/255.0f alpha:1] CGColor]];
    [button.layer setBorderWidth:1.0f];
    [button.layer setBorderColor:[[UIColor colorWithRed:.3 green:.3 blue:.3 alpha:1] CGColor]];
    //  [button.layer setShadowOpacity:0.4f];
    [button.layer setCornerRadius:10];
    
    return button;
    */
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    /*
    // detect map pan for removing callout
    UIPanGestureRecognizer* panRec = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(didDragMap:)];
    [panRec setDelegate:self];
    [self.mapView addGestureRecognizer:panRec];
    
    // detect map pinch for removing callout
    UIPinchGestureRecognizer* pinchRec = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(didDragMap:)];
    [pinchRec setDelegate:self];
    [self.mapView addGestureRecognizer:pinchRec];
    
    // two finger tap
    UITapGestureRecognizer *twoFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didDragMap:)];
    twoFingerTap.numberOfTouchesRequired = 2;
    [self.mapView addGestureRecognizer:twoFingerTap];
	*/
    
	self.resetMap = YES;
    
    self.selectedPropertyIndex = -1;
    self.selectedProperty = nil;
    
  // self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:117/255.0f green:4/255.0f blue:32/255.0f alpha:1];
    
   // self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:20/255.0f green:87/255.0f blue:36/255.0f alpha:1];
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:50/255.0f green:160/255.0f blue:0/255.0f alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:156/255.0f green:156/255.0f blue:156/255.0f alpha:1];
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:24/255.0f green:60/255.0f blue:97/255.0f alpha:1];

    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    self.navigationItem.hidesBackButton = YES; // Important
    
	self.callouts = [[[NSMutableArray alloc] init] autorelease];
    self.navigationItem.title = @"";
}

- (void)unblockRegionChange{
    self.blockCalloutRemoval = NO;
}

- (void)viewWillAppear:(BOOL)animated{
	
	[super viewWillAppear: animated];
    
    R2AppDelegate *appDelegate = (R2AppDelegate*)[[UIApplication sharedApplication] delegate];
	
	appDelegate.propertyFinderDelegate = self;
    
    // show the callout
    if (self.selectedProperty != nil) {
         [self showCalloutForPropertyIndex:self.selectedProperty];
    }
    
    [self startStandardUpdates];
    
    [self performSelector:@selector(unblockRegionChange) withObject:Nil afterDelay:0.7];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear: animated];
    self.navigationController.tabBarController.tabBar.translucent = NO;
    self.navigationController.tabBarController.tabBar.translucent = YES;
}

- (void)viewDidDisappear:(BOOL)animated
{
    self.selectedPropertyIndex = -1;
    self.selectedProperty = nil;
    
    // stop location updates
    [self stopStandardUpdates];
}

#pragma mark -
#pragma mark PropertySelectionChanged delegate

- (void) didChangeSelectedProperty:(Property*)property
{
    self.selectedProperty = property;
}

- (void)setToCurrentLocation
{
    MKCoordinateRegion region;
    region.center = self.lastLocation.coordinate;
    region.span.longitudeDelta = .01;
    region.span.latitudeDelta = .01;
    
    [self.mapView setRegion:region animated:YES];
}

/*
 *  Location Manager
 *
 */

#pragma mark -
#pragma mark CLLocationManagerDelegate helpers

// this is not exact, just an approximation
- (BOOL)isInSanFrancisco:(CLLocationCoordinate2D)location{
    
    CLLocationCoordinate2D san_francisco_ctr = CLLocationCoordinate2DMake(37.758, -122.444);
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(san_francisco_ctr, 14000, 17000);
    
    double lat_distance_from_sf_ctr = fabs(location.latitude - san_francisco_ctr.latitude);
    double lon_distance_from_sf_ctr = fabs(location.longitude - san_francisco_ctr.longitude);
    
    BOOL in_lat_region = region.span.latitudeDelta/2.0 >= lat_distance_from_sf_ctr;
    BOOL in_lon_region = region.span.longitudeDelta/2.0 >= lon_distance_from_sf_ctr;
    
    return (in_lat_region && in_lon_region);
}

- (void)setRegionToSanFrancisco{
    CLLocationCoordinate2D san_francisco_ctr = CLLocationCoordinate2DMake(37.758, -122.444);
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(san_francisco_ctr, 14000, 17000);
    
    [self.mapView setRegion:region];
}

- (void)startStandardUpdates
{
    // Create the location manager if this object does not
    // already have one.
    if (nil == locationManager)
        locationManager = [[CLLocationManager alloc] init];
    
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    
    // Set a movement threshold for new events.
    locationManager.distanceFilter = 20;
    
    [locationManager startUpdatingLocation];
    [locationManager requestWhenInUseAuthorization];
    
    self.mapView.showsUserLocation = YES;
}

- (void)stopStandardUpdates
{
    [locationManager stopUpdatingLocation];
    self.mapView.showsUserLocation = NO;
}

- (void)startSignificantChangeUpdates
{
    // Create the location manager if this object does not
    // already have one.
    if (nil == locationManager)
        locationManager = [[CLLocationManager alloc] init];
    
    locationManager.delegate = self;
    [locationManager startMonitoringSignificantLocationChanges];
}

- (void)stopSignificantChangeUpdates
{
    [locationManager stopMonitoringSignificantLocationChanges];
}

#pragma mark -
#pragma mark kCLLocationManagerDelegate

// Delegate method from the CLLocationManagerDelegate protocol.
- (void)locationManager:(CLLocationManager *)manager  didUpdateLocations:(NSArray *)locations
{
    // If it's a relatively recent event, turn off updates to save power
    self.lastLocation = [locations lastObject];
    NSDate* eventDate = self.lastLocation.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    if (fabs(howRecent) < 15.0) {
        // If the event is recent, do something with it.
        NSLog(@"latitude %+.6f, longitude %+.6f\n",
              self.lastLocation.coordinate.latitude,
              self.lastLocation.coordinate.longitude);
        
        BOOL isInSF = [self isInSanFrancisco:self.lastLocation.coordinate];
        
        // if in San francisco and no left button, add it
        if (isInSF && !self.navigationItem.leftBarButtonItem) {
            // left bar button
            UIButton *button =  [self currentLocationButton];
            self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
            self.navigationItem.leftBarButtonItem.style = UIBarButtonItemStyleBordered;
                   }
        else if(!isInSF){
            self.navigationItem.leftBarButtonItem = nil;
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(nonnull NSError *)error
{
    NSLog(@"location error: %@", error);
}


#pragma mark -
#pragma mark MapViewController ending

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
