//
//  R2AppDelegate.h
//  R2
//
//  Created by stephen feggeler on 2/27/13.
//  Copyright 2013 self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "DataController.h"
#import "PropertyFinder.h"

@protocol propertyFinderDelegate;

@interface R2AppDelegate : NSObject 
	<UIApplicationDelegate, UITabBarControllerDelegate, PropertyFinderDelegate> 
{		
    UIWindow *window;
    UITabBarController * tabBarController;
	DataController * dataController;
    
	NSMutableArray * properties;
    NSMutableArray * listProperties;
    NSMutableArray * mapProperties;
    
	PropertyFinder * propertyFinder;
    PropertyFinder * listFinder;
    PropertyFinder * mapFinder;
    
    id <PropertyFinderDelegate> propertyFinderDelegate;
	
    NSInteger listCurrentPage;
    NSInteger mapCurrentPage;
    NSInteger listTotalProperties;
    NSInteger mapTotalProperties;
	NSInteger requirementsMayHaveChanged;
}

@property (nonatomic, assign) BOOL mapFetchError;
@property (nonatomic, assign) BOOL listFetchError;

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UITabBarController *tabBarController;

@property (nonatomic, retain) NSMutableArray *properties;
@property (nonatomic, retain) NSMutableArray *mapProperties;
@property (nonatomic, retain) NSMutableArray *listProperties;

@property (nonatomic, retain) DataController *dataController;
@property (nonatomic, assign) id <PropertyFinderDelegate> propertyFinderDelegate;
@property (nonatomic, assign) id <PropertyFinderDelegate> mapViewControllerDelegate;
@property (nonatomic, assign) id <PropertyFinderDelegate> listViewControllerDelegate;


- (DataController*)loadDataController;
- (void)saveDataController;
- (void)fetchProperties;
- (void)fetchMoreProperties;
- (void)fetchPropertiesInRegion:(MKCoordinateRegion)region;

@end


