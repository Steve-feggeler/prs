//
//  PropertyDetailsViewController.h
//  R2
//
//  Created by stephen feggeler on 3/16/13.
//  Copyright 2013 self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <QuartzCore/CATransaction.h>
#import <QuartzCore/CAAnimation.h>
#import <MessageUI/MessageUI.h>
#import "ImagesTableViewCell.h"
#import "ScrollingImageDelegate.h"

@protocol PropertySelectionChanged;

@interface PropertyDetailsViewController : UITableViewController <MKAnnotation, MFMailComposeViewControllerDelegate> {
	
}

@property (nonatomic, retain) NSMutableArray *properties;
@property (nonatomic, assign) NSInteger indexOfProperty;
@property (nonatomic, assign) NSInteger totalProperties;
@property (nonatomic, retain) NSArray *imagePaths;
@property (nonatomic, retain) ScrollingImageDelegate *scrollImageDelegate1;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, retain) UITapGestureRecognizer *tapGesture;
@property (nonatomic, assign) id <PropertySelectionChanged> selectedPropertyDelegate;
@property (nonatomic, assign) BOOL blockFetchMoreData;
@property (nonatomic, retain) NSMutableDictionary* propertyTypeData;

@property (nonatomic, retain) IBOutlet UIView *tableHeaderView;
@property (nonatomic, retain) IBOutlet UIScrollView *imageScroller;
@property (nonatomic, retain) IBOutlet UILabel *labelImageScroller;


- (UITableViewCell *)styleValue1TableViewCell:(UITableView*)tableView;
- (UITableViewCell *)styleSubtitleTableViewCell:(UITableView*)tableView;
- (ImagesTableViewCell *)imagesTableViewCell:(UITableView *)tableView;
- (UITableViewCell *)mapTableViewCell:(UITableView *)tableView;
- (UITableViewCell *)longTextTableViewCell:(UITableView *)tableView;
- (void)changePage:(id)sender;
- (void)setupPropertyDetails;
- (void)setupImageScroller1;
- (CGFloat)heightForLongText:(NSString*)text;
- (void) myCustomBack;
- (void)fetchMoreData;

@end

// called when selected property index chanes

@protocol PropertySelectionChanged

- (void)didChangeSelectedProperty:(Property*)property;

@end
