//
//  R2AppDelegate.m
//  R2
//
//  Created by stephen feggeler on 2/27/13.
//  Copyright 2013 self. All rights reserved.
//

#import "R2AppDelegate.h"
#import "PropertyListController.h"
#import "MapViewController.h"
#import "RequirementViewController.h"
#import "SFAlertHelper.h"


@implementation R2AppDelegate

@synthesize window;
@synthesize tabBarController;
@synthesize dataController;
@synthesize properties;
@synthesize listProperties;
@synthesize mapProperties;
@synthesize propertyFinderDelegate;

// for protocal PropertyFinderDelegate
@synthesize postCMD;
@synthesize isAppend;

#pragma mark -
#pragma mark Application data

- (void)fetchProperties
{
    NSString *postCmd = [dataController getPostCMDForPage:0];
    
    if ([postCmd isEqualToString:self.propertyFinderDelegate.postCMD]) {
        return;
    }
    
    self.propertyFinderDelegate.postCMD = postCmd;
    self.propertyFinderDelegate.isAppend = NO;
    
    if ([(id)propertyFinderDelegate isKindOfClass:[PropertyListController class]])
    {
        listTotalProperties = 0;
        listCurrentPage = 1;
        self.listViewControllerDelegate = propertyFinderDelegate;
        [listFinder getPropertiesFromQueryString:postCmd];
    }
    else if([(id)propertyFinderDelegate isKindOfClass:[MapViewController class]]){
        mapTotalProperties = 0;
        mapCurrentPage = 1;
        self.mapViewControllerDelegate = propertyFinderDelegate;
        [mapFinder getPropertiesFromQueryString:postCmd];
    }	
}

-(void)fetchMoreProperties
{
    if ([(id)propertyFinderDelegate isKindOfClass:[PropertyListController class]])
    {
        NSInteger cnt = self.listProperties.count;
        
        if (listTotalProperties == cnt) {
            return;
        }
        
        ++listCurrentPage;
        
        NSString *postCmd = [dataController getPostCMDForPage:listCurrentPage];
        
        self.propertyFinderDelegate.isAppend = YES;
        
        self.listViewControllerDelegate = propertyFinderDelegate;
        
        [listFinder getPropertiesFromQueryString:postCmd];
    }
    
    else if ([(id)propertyFinderDelegate isKindOfClass:[MapViewController class]]){
    
        NSInteger cnt = self.mapProperties.count;
        
        if (mapTotalProperties == cnt) {
            return;
        }
        
        ++mapCurrentPage;
        
        NSString *postCmd = [dataController getPostCMDForPage:mapCurrentPage];
        
        self.propertyFinderDelegate.isAppend = YES;
        
        self.mapViewControllerDelegate = propertyFinderDelegate;
        
        [mapFinder getPropertiesFromQueryString:postCmd];
    }
}

- (void)fetchPropertiesInRegion:(MKCoordinateRegion)region
{	
	mapTotalProperties = 0;
	mapCurrentPage = 1;
	
	NSString *postCmd = [dataController getPostCMDForPage:mapCurrentPage region:region];
    
    self.propertyFinderDelegate.isAppend = YES;
    
    self.listViewControllerDelegate = propertyFinderDelegate;
	
	[mapFinder getPropertiesFromQueryString:postCmd];
}

- (void)propertyFinder:(PropertyFinder *)finder didFailWithError:(NSError *)error
{
    if ([(id)propertyFinderDelegate isKindOfClass:[MapViewController class]]) {
        NSLog(@"MapViewController");
        self.mapFetchError = YES;
        //propertyFinderDelegate.postCMD = nil;
        self.mapViewControllerDelegate.postCMD = nil;
    }
    else if([(id)propertyFinderDelegate isKindOfClass:[PropertyListController class]]){
        NSLog(@"PropertyListController");
        self.listFetchError = YES;
        //propertyFinderDelegate.postCMD = nil;
        self.listViewControllerDelegate.postCMD = nil;
    }
    
    [SFAlertHelper showAlertForError:error delegate:nil];
}

- (void)propertyFinder:(PropertyFinder *)finder didFindProperties:(NSMutableArray *)newProperties inRange:(NSRange)range ofTotal:(NSInteger)total
{
    // reset error flags if getting data
    /*
    if ([(id)propertyFinderDelegate isKindOfClass:[MapViewController class]]) {
        self.mapFetchError = NO;
    }
    else if([(id)propertyFinderDelegate isKindOfClass:[PropertyListController class]]){
        self.listFetchError = NO;
    }
    */
    
    // keep seperate property list for map and list searches
    
    if (finder == listFinder)
    {
        self.listFetchError = NO;
        
        if (listCurrentPage > 1)
        {
            [self.listProperties addObjectsFromArray:newProperties];
        }
        else
        {
            self.listProperties = newProperties;
        }
        
        listTotalProperties = total;
        
        [self.listViewControllerDelegate propertyFinder:finder didFindProperties:self.listProperties inRange:range ofTotal:total];
    }
    else if (finder == mapFinder)
    {
        self.mapFetchError = NO;
        
        if (mapCurrentPage > 1)
        {
            [self.mapProperties addObjectsFromArray:newProperties];
        }
        else
        {
            self.mapProperties = newProperties;
        }
        
        mapTotalProperties = total;
        
        [self.mapViewControllerDelegate  propertyFinder:finder didFindProperties:self.mapProperties inRange:range ofTotal:total];
    }
}

-(DataController*)loadDataController{
	
	// find this app's documents directory
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	
	// get the first directory
	NSString *directory = [paths objectAtIndex:0];
	
	// concatenate the file name "data.propertyTypes" to the end of the path
	NSString *filePath = [[NSString alloc] initWithString:
						  [directory stringByAppendingPathComponent:@"data.reqs"]];	
	
	DataController* dc =[NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
	
	[filePath release];
	
	if (dc == nil) {
		dc = [[[DataController alloc] init] autorelease];
	}
	
	return dc;	
}

-(void)saveDataController
{	
	// find this app's documents directory
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	
	// get the first directory
	NSString *directory = [paths objectAtIndex:0];
	
	// concatenate the file name "data.propertyTypes" to the end of the path
	NSString *filePath = [[NSString alloc] initWithString:
						  [directory stringByAppendingPathComponent:@"data.reqs"]];
	
	// archive the slideshows to the file data.slideshows
	[NSKeyedArchiver archiveRootObject:dataController toFile:filePath];
	
	[filePath release];
}

#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    
    // Override point for customization after application launch.

	self.dataController = [self loadDataController];
	
	requirementsMayHaveChanged = YES;
	
	propertyFinder = [[PropertyFinder alloc] init];
	propertyFinder.delegate = self;
    
    listFinder = [[PropertyFinder alloc] init];
	listFinder.delegate = self;
    
    mapFinder = [[PropertyFinder alloc] init];
	mapFinder.delegate = self;
	
	tabBarController.delegate = self;
    tabBarController.selectedIndex = 0;
    
    [self.window setRootViewController:tabBarController];

    // Add the tab bar controller's view to the window and display.
//  [self.window addSubview:tabBarController.view];
    [self.window makeKeyAndVisible];
	
    // changed for ios 8
	//[self fetchProperties];
 
    UIColor* navBarColor = [UIColor colorWithRed:24/255.0f green:60/255.0f blue:97/255.0f alpha:1];

    [[UINavigationBar appearance] setTintColor:navBarColor];
    
     if ([[UIDevice currentDevice].systemVersion floatValue] >= 7){
       //  UIColor* navBarColor = [UIColor colorWithRed:46.0/255.0 green:68.0/255.0 blue:132.0/255.0 alpha:0.9f];
         UIColor* navBarColor = [UIColor colorWithRed:63.0/255.0 green:131.0/255.0 blue:143.0/255.0 alpha:1.0f];
         [[UINavigationBar appearance] setBarTintColor:navBarColor];
         [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
         [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
         [[UINavigationBar appearance] setBarStyle:UIBarStyleBlack];
    }
    
    // set cache size
    int cacheSizeMemory = 4*1024*1024; // 4MB
    int cacheSizeDisk = 32*1024*1024; // 32MB
    //cacheSizeMemory = 1; // 4MB
    //cacheSizeDisk = 1; // 32MB

    //NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:@"nsurlcache"];
    NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:nil];
    [NSURLCache setSharedURLCache:sharedCache];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}


- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}


#pragma mark -
#pragma mark UITabBarControllerDelegate methods


// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
		
	if([viewController isMemberOfClass:[UINavigationController class]])
	{
		UINavigationController *navController = (UINavigationController*)viewController;
		UIViewController *rootViewController = [navController.viewControllers objectAtIndex:0];
		
		// PropertyListController
		
		if ([rootViewController isMemberOfClass:[PropertyListController class]]) {
            
            PropertyListController* propListController = (PropertyListController*)rootViewController;
			
			self.propertyFinderDelegate = propListController;
                   
			[self fetchProperties];
		}
		
		// MapViewController
		
		else if([rootViewController isMemberOfClass:[MapViewController class]]){
			
			MapViewController* mapController = (MapViewController*)rootViewController;
			
			self.propertyFinderDelegate = mapController;
            
            // if requirements changed, reset map
            
            NSString *postCmd = [dataController getPostCMDForPage:listCurrentPage];
            if (![postCmd isEqualToString:self.propertyFinderDelegate.postCMD]) {
                 mapController.resetMap = YES;
            }
           
            [self fetchProperties];
			
            /*
			if(requirementsMayHaveChanged){
				requirementsMayHaveChanged = NO;
				mapController.resetMap = YES;
				[self fetchProperties];
			}
             */
		}
		
		else if([rootViewController isMemberOfClass:[RequirementViewController class]]){
			requirementsMayHaveChanged = YES;
		}
	}
	
}


/*
// Optional UITabBarControllerDelegate method.
- (void)tabBarController:(UITabBarController *)tabBarController didEndCustomizingViewControllers:(NSArray *)viewControllers changed:(BOOL)changed {
}
*/


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    NSLog(@"DidReceiveMemoryWarning");
}


- (void)dealloc
{	
    [propertyFinder release];
    [mapFinder release];
    [listFinder release];
    
	[dataController release];
    [tabBarController release];
    [window release];
    [super dealloc];
}

@end

