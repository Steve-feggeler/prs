//
//  DirectionMapView.h
//  R2
//
//  Created by stephen feggeler on 5/30/13.
//
//
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface DirectionMapView : MKMapView

@property (nonatomic, assign) float heading;

@end
