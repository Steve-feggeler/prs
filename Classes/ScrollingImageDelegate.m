//
//  ScrollingImageDelegate.m
//  ScrollImages1
//
//  Created by stephen feggeler on 3/14/13.
//  Copyright 2013 self. All rights reserved.
//

#import "ScrollingImageDelegate.h"
#import "UIImageView+WebCache.h"


@implementation ScrollingImageDelegate

@synthesize scrollObjWidth;
@synthesize scrollObjHeight;
@synthesize positionLabel;
@synthesize navigationItem;

- (void)scrollViewDidScroll:(UIScrollView *)myScrollView {
	
	[self scrollViewAddMissingViews:myScrollView];
	
	[self scrollViewRemoveUnseenViews:myScrollView];	
}

- (void)resetScrollView:(UIScrollView*)scrollView{
	
	NSArray *subviews = [scrollView subviews];
	
	for (int i=0; i<subviews.count; ++i) {
		UIView *view = [subviews objectAtIndex:i];
		if (view.tag>0) {
			[view removeFromSuperview];
		}
	}
	
	scrollView.contentOffset = CGPointMake(0,0);
    
    // must set the presentationLayer bounds also!!!!
    CGRect rect = [[scrollView.layer presentationLayer] bounds];
    rect.origin.x = 0;
    [[scrollView.layer presentationLayer] setBounds:rect];
}

- (void)updatePositionLabel:(UIScrollView *)scrollView
{    
    //NSLog(@"scroll offset is %d",loc);
    CGPoint pos = [[scrollView.layer presentationLayer] bounds].origin;
    //NSLog(@"scroll pos is %f",pos.x);
    int loc = (pos.x + scrollObjWidth/2) / scrollObjWidth + 1;

    if (loc == 0) {
        return;
    }
    
    [self setPositionLabelLocation:loc];
}

-(void)setPositionLabelLocation:(int)loc{
    NSUInteger cnt = _imageNames.count;
    
    NSString *label  = [NSString stringWithFormat:@"%d of %lu", loc, (unsigned long)cnt];
    
    if (cnt == 0) {
        label = @"no pictures";
    }
	
	if(self.positionLabel) {
		
		self.positionLabel.text = label;
	}
    
    if (self.navigationItem) {
        self.navigationItem.title = label;
    }
}

// gets the needed indexes for the subviews
-(NSRange)scrollViewImageRange:(UIScrollView *)myScrollView{
	
	// start at one image before first visible
	int loc = myScrollView.contentOffset.x / scrollObjWidth - 1;
	
	// end one image after last visible
	int len = myScrollView.frame.size.width / scrollObjWidth + 1;
	
	// keep loc positive since this is an index
	if (loc < 0) {
		loc = 0;
	}
	
	// for fractional images, add one to the length
	int width = myScrollView.frame.size.width;
	NSInteger imageWidth = scrollObjWidth;
    if (imageWidth) {
        int mod = width % imageWidth;
        if (mod != 0) {
            ++len;
        }
    }
	
	return NSMakeRange(loc, len);
}


- (void)scrollViewRemoveUnseenViews:(UIScrollView *)myScrollView{
	
	NSRange range = [self scrollViewImageRange:myScrollView];
	
	NSInteger firstTag = range.location;
	NSInteger lastTag = range.location + range.length + 2;
	
	NSArray *views = [myScrollView subviews];
	
	for (UIView *view in views) {
		
		if (view.tag == 0) {
			continue;
		}
		
		if( (view.tag < firstTag) || (view.tag > lastTag) ){
			
			NSInteger tag = view.tag;
			
			NSLog(@"remove tag %ld ",(long)tag);
			
			[view removeFromSuperview];
		}
	}		
}


-(void)scrollViewAddMissingViews:(UIScrollView *)myScrollView{
	
	NSRange range = [self scrollViewImageRange:myScrollView];
	
	NSInteger first = range.location;
	NSInteger last = range.location + range.length;
	
	NSInteger max = _imageNames.count - 1;
	
	if (last > max) {
		last = max;
	}
    
    if (last < 0) {
        UIImageView *imageView = [[UIImageView alloc] initWithImage:nil];
		imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.image = [UIImage imageNamed:@"No-Image-available.png"];
       // imageView.image = nil;
        CGRect rect = imageView.frame;
		rect.size.height = scrollObjHeight;
		rect.size.width = scrollObjWidth;
		rect.origin = CGPointMake(0, 0);
		imageView.frame = rect;
        imageView.tag = 1;
        [myScrollView addSubview:imageView];
		[imageView release];
        self.positionLabel.text = @"";
        return;
    }
    
	for (NSInteger i = first; i <= last; i++){
		
		if ( [myScrollView viewWithTag:(i + 1)] ) {
			continue;
		}
		
        UIImageView *imageView = [[UIImageView alloc] initWithImage:nil];
		
		imageView.contentMode = UIViewContentModeScaleAspectFit;
		
		NSString *name = [_imageNames objectAtIndex:i];
		//[imageView setImageWithURL:[NSURL URLWithString:name] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        
        [imageView setImageWithURL:[NSURL URLWithString:name]
         success:^(UIImage *image, BOOL cached)
         {
             
         }
         failure:^(NSError *error)
         {
             imageView.image = [UIImage imageNamed:@"No-Image-available.png"];
         }];
		
		CGRect rect = imageView.frame;
		
		rect.size.height = scrollObjHeight;
		rect.size.width = scrollObjWidth;
		rect.origin = CGPointMake(scrollObjWidth * i, 0);
		
		imageView.frame = rect;
		
		imageView.tag = i + 1;
		NSLog(@"added tag %ld ",(long)imageView.tag);
		
		[myScrollView addSubview:imageView];
		[imageView release];
	}
	
	[self updatePositionLabel:myScrollView];
}

- (void)resizeScrollView:(UIScrollView *)scrollView withFrame:(CGRect)frame;
{
    // get current page before rotation
    
    CGPoint pos = [[scrollView.layer presentationLayer] bounds].origin;
    int page = (pos.x + scrollObjWidth/2) / scrollObjWidth;
    
    // set scroll frame
    
    scrollView.frame = frame;
    
    // set the page size
    
	self.scrollObjWidth = frame.size.width;
	self.scrollObjHeight = frame.size.height;
    
    // calc new scroll offset
    
    CGFloat newScrollPos = page * frame.size.width;
    CGPoint newScrollOffset = scrollView.contentOffset;
    newScrollOffset.x = newScrollPos;
    
    // set new scroll offset
	
    CGSize scrollSize;
    
    if (_imageNames.count > 0) {
        scrollSize.width = _imageNames.count * self.scrollObjWidth - 1;
        scrollSize.height = self.scrollObjHeight;
    }
    else{
        scrollSize.width = self.scrollObjWidth - 1;
        scrollSize.height = self.scrollObjHeight;
    }
	
	[scrollView setContentSize:scrollSize];
    [scrollView setContentOffset:newScrollOffset];
    
    // resize subview images
    //
    NSRange range = [self scrollViewImageRange:scrollView];
	
    NSInteger last = range.location + range.length;
	NSInteger max = _imageNames.count - 1;
	
	if (last > max) {
		last = max;
	}
    
	for (int i = 0; i <= last + 1; i++){
        
        UIView *subView = [scrollView viewWithTag:(i + 1)];
		
		if ( subView ) {
            
            CGRect imageViewFrame;
            
            imageViewFrame.size.height = scrollObjHeight;
            imageViewFrame.size.width = scrollObjWidth;
            imageViewFrame.origin = CGPointMake(scrollObjWidth * i, 0);
            
            subView.frame = imageViewFrame;			
		}		
    }
    
    // update the ui
    
    CGRect rect = [[scrollView.layer presentationLayer] bounds];
    rect.origin.x = scrollObjWidth * page;
    [[scrollView.layer presentationLayer] setBounds:rect];
    
    [self setPositionLabelLocation:page+1];
}

-(void)dealloc{
    [_imageNames release];
    [super dealloc];
}


@end
