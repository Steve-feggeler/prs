//
//  DirectionMapView.m
//  R2
//
//  Created by stephen feggeler on 5/30/13.
//
//

#import "DirectionMapView.h"

@implementation DirectionMapView

@synthesize heading;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        heading = 0;
        self.userInteractionEnabled = NO;

    }
    return self;
}


 /*

- (float)DegreeToRadian:(float) degree
{
    return ((degree / 180.0f) * M_PI);
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
 // Drawing code
 
 float from = 0.4;
 float to = 5.8;
 float angle = 25;
    
 from = [self DegreeToRadian:(heading - angle - 90)];
 to = [self DegreeToRadian:(heading + angle - 90)];
 
 CGPoint center = self.center;
 center.x = 40;
 center.y = 40;
 
 CGContextRef ctx = UIGraphicsGetCurrentContext();
 
 CGContextSetFillColor(ctx, CGColorGetComponents( [[UIColor colorWithRed: 0 green: 0 blue: 0 alpha: 0.2] CGColor]));
 CGContextMoveToPoint(ctx, center.x, center.y);
 CGContextAddArc(ctx, center.x, center.y, 38, from, to, 1);
 CGContextClosePath(ctx);
 CGContextFillPath(ctx);

}
 */

@end
