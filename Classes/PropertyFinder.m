//
//  PropertyFinder.m
//  R2
//
//  Created by stephen feggeler on 2/28/13.
//  Copyright 2013 self. All rights reserved.
//

#import "PropertyFinder.h"

@implementation PropertyFinder

@synthesize delegate;

- (void)getPropertiesFromQueryString:(NSString *)queryString
{	
	
	connection = [[PRSConnection alloc] init];
	connection.delegate = self;
	
	[connection performSearch:queryString];
	[connection release];
}

- (void)prsConnection:(PRSConnection *)connection didFailWithError:(NSError *)error
{
    [delegate propertyFinder:self didFailWithError:error];
}

-(void)findGeoCode:(Property*)property
{
    NSString * address = [NSString stringWithFormat:@"%@, %@, United States",property.address1, property.address2];
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:address completionHandler:^(NSArray* placemarks, NSError* error){
        for (CLPlacemark* aPlacemark in placemarks)
        {
            // Process the placemark.
            NSString *latDest1 = [NSString stringWithFormat:@"%.4f",aPlacemark.location.coordinate.latitude];
            NSString *lngDest1 = [NSString stringWithFormat:@"%.4f",aPlacemark.location.coordinate.longitude];
            property.latitude = latDest1;
            property.longitude = lngDest1;
        }
    }];

}

- (void)prsConnection:(PRSConnection *)connection didReceiveData:(NSData *)data
{
	NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
	
	NSArray *rows = [responseString componentsSeparatedByString:@"\n"];
	NSArray *cols = [[rows objectAtIndex:0] componentsSeparatedByString:@"|"];
	NSString *summary = [rows objectAtIndex:rows.count-2];
	NSArray *summaryParts = [summary componentsSeparatedByString:@"|"];
	
	NSInteger propertyCount = rows.count - 3;
	NSInteger index = [[summaryParts objectAtIndex:0] intValue];
	NSInteger totalForRequirement = [[summaryParts objectAtIndex:1] intValue];
	NSInteger pageSize  = [[summaryParts objectAtIndex:2] intValue];
	NSInteger location = index * pageSize;
	
	NSLog(@"%lu", (unsigned long)cols.count);
	
	properties = [[[NSMutableArray alloc] init] autorelease];
		
	for (NSInteger i=1; rows != nil && i<rows.count - 2; ++i) {
		
		NSArray *fields = [[rows objectAtIndex:i] componentsSeparatedByString:@","];
		
		Property *property = [[Property alloc] initWithRecord:fields];
        
        if(property.latitude == nil || 0.0 == [property.latitude floatValue] || 0.0 == [property.longitude floatValue])
        {
            // missing lat and lon
        }
               		
		[properties addObject:property];
		
		[property release];
	}
	
	[responseString release];
    
    // location is an index, propertyCount is the number of properies in this chunk
	NSRange range = NSMakeRange (location, propertyCount);
		
	[delegate propertyFinder:self didFindProperties: properties inRange:range ofTotal: totalForRequirement];
}

@end
