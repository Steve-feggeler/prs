//
//  SFAlertHelper.h
//  R2
//
//  Created by stephen feggeler on 12/20/13.
//
//

#import <Foundation/Foundation.h>

@interface SFAlertHelper : NSObject

+ (void)showAlertForError:(NSError *)error delegate:(id /*<UIAlertViewDelegate>*/)delegate;

@end
