//
//  PropertyCalloutView.h
//  R2
//
//  Created by stephen feggeler on 4/23/13.
//  Copyright 2013 self. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PropertyCalloutView : UIView {

}

@property (nonatomic, retain) IBOutlet UILabel *label1;
@property (nonatomic, retain) IBOutlet UILabel *label2;
@property (nonatomic, retain) IBOutlet UILabel *label3;
@property (nonatomic, retain) IBOutlet UILabel *label4;
@property (nonatomic, retain) IBOutlet UILabel *label5;
@property (nonatomic, retain) IBOutlet UIImageView *mainImage;
@property (nonatomic, retain) IBOutlet UIButton *accessory;
@property (nonatomic, retain) IBOutlet UIButton *streetView;

- (IBAction) showDetails;

@end
