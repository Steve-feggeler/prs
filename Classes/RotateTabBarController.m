//
//  RotateTabBarController.m
//  R2
//
//  Created by stephen feggeler on 6/29/13.
//
//

#import "RotateTabBarController.h"

@interface RotateTabBarController ()

@end

@implementation RotateTabBarController

- (BOOL)shouldAutorotate
{
    UINavigationController *tabsNav = [self.viewControllers objectAtIndex:self.selectedIndex];
    
    NSString *className = NSStringFromClass([tabsNav.visibleViewController class]);
    
    if ([className isEqualToString:@"PropertyDetailsViewController"]) {
        return [tabsNav.visibleViewController shouldAutorotate];
    }
   
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    @try {
        UINavigationController *tabsNav = [self.viewControllers objectAtIndex:self.selectedIndex];
        
        NSString *className = NSStringFromClass([tabsNav.visibleViewController class]);
        
        if ([className isEqualToString:@"PropertyDetailsViewController"]) {
            return [tabsNav.visibleViewController supportedInterfaceOrientations];
        }

        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
        
    return UIInterfaceOrientationMaskPortrait;    
}

@end
