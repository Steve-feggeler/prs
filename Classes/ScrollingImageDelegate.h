//
//  ScrollingImageDelegate.h
//  ScrollImages1
//
//  Created by stephen feggeler on 3/14/13.
//  Copyright 2013 self. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ScrollingImageDelegate : NSObject <UIScrollViewDelegate> {
	
	NSInteger scrollObjWidth;
	NSInteger scrollObjHeight;
}

@property (nonatomic, retain) NSArray* imageNames;
@property (nonatomic, assign) NSInteger scrollObjWidth;
@property (nonatomic, assign) NSInteger scrollObjHeight;
@property (nonatomic, retain) UILabel *positionLabel;
@property (nonatomic, assign) UINavigationItem *navigationItem;

- (void)scrollViewRemoveUnseenViews:(UIScrollView *)scrollView;
- (void)scrollViewAddMissingViews:(UIScrollView *)scrollView;
- (NSRange)scrollViewImageRange:(UIScrollView *)scrollView;
- (void)updatePositionLabel:(UIScrollView *)scrollView;
- (void)resetScrollView:(UIScrollView*)scrollView;
- (void)resizeScrollView:(UIScrollView *)scrollView withFrame:(CGRect)frame;
@end
