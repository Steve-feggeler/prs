//
//  SFAlertHelper.m
//  R2
//
//  Created by stephen feggeler on 12/20/13.
//
//

#import "SFAlertHelper.h"

@implementation SFAlertHelper

- (id)init
{
    //Don't allow init to initialize any memory state
    //Perhaps throw an exception to let some other programmer know
    //not to do this
    return nil;
}

+ (void)showAlertForError:(NSError *)error delegate:(id /*<UIAlertViewDelegate>*/)theDelegate
{
    // alert the user of an error
    NSInteger code =[error code];
    if (code == -1003) {
        
        // can't connect to host server
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:NSLocalizedString(@"Host Server not available!", @"AlertView")
                                  message:NSLocalizedString(@"Please try again soon.", @"AlertView")
                                  delegate:theDelegate
                                  cancelButtonTitle:NSLocalizedString(@"Ok", @"AlertView")
                                  otherButtonTitles:nil, nil];
        
        [alertView show];
    }
    else if(code == -1009)
    {
        // if code is 1009 can't connect to internet
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:NSLocalizedString(@"Not connected to Internet!", @"AlertView")
                                  message:NSLocalizedString(@"Please connect to the Internet.", @"AlertView")
                                  delegate:theDelegate
                                  cancelButtonTitle:NSLocalizedString(@"Ok", @"AlertView")
                                  otherButtonTitles:nil, nil];
        
        [alertView show];
    }
    else{
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:NSLocalizedString(@"Network Error!", @"AlertView")
                                  message:NSLocalizedString(@"Please try again soon.", @"AlertView")
                                  delegate:theDelegate
                                  cancelButtonTitle:NSLocalizedString(@"Ok", @"AlertView")
                                  otherButtonTitles:nil, nil];
        
        [alertView show];
    }
}

@end
