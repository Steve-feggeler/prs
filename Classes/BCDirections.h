//
//  BCDirections.h
//  R2
//
//  Created by stephen feggeler on 9/6/13.
//
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface BCDirections : NSObject

+ (void)GetDirectionsLocation:(CLLocationCoordinate2D)location;


@end
