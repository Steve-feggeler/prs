//
//  MapViewController.h
//  R2
//
//  Created by stephen feggeler on 3/26/13.
//  Copyright 2013 self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <QuartzCore/QuartzCore.h>

#import "R2AppDelegate.h"
#import "PropertyFinder.h"
#import "CalloutMapAnnotation.h"
#import "PropertyDetailsViewController.h"

@interface MapViewControllerOLD1 : UIViewController
    <MKMapViewDelegate, PropertyFinderDelegate,
    CLLocationManagerDelegate, PropertySelectionChanged>
{
	NSMutableArray *callouts;
	MKCoordinateRegion theRegion;    
    MKCoordinateRegion prevRegion;
    BOOL blockSetRegion;
}

@property (nonatomic, retain) NSMutableArray *callouts;
@property (nonatomic, retain) IBOutlet MKMapView *mapView;
@property (nonatomic, assign) NSInteger totalProperties;
@property (nonatomic, assign) bool resetMap;
@property (nonatomic, retain) CalloutMapAnnotation *calloutMapAnnotation;
@property (nonatomic, retain) MKAnnotationView *selectedAnnotationView;
@property (nonatomic, assign) CLLocationCoordinate2D prevCenter;
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, assign) NSInteger selectedPropertyIndex;
@property (nonatomic, retain) NSMutableArray *properties;

- (void)setPropertyRegion;
- (void)removeAllPinsButUserLocation;
- (void)updateTitle;
- (void)updateMap;
- (void)removeHiddenPins;
- (MKAnnotationView *)mapView:(MKMapView *)aMapView viewForPriceAnnotation:(id <MKAnnotation>)annotation;
- (MKAnnotationView *)mapView:(MKMapView *)aMapView viewForCalloutAnnotation:(id <MKAnnotation>)annotation;
- (void)removeCallout:(id)object;
- (void)showDetailsAction:(UIButton*)sender;
- (void)showStreetViewAction:(UIButton*)sender;
- (void)unblockSetRegion:(id)object;
- (void)showCalloutForPropertyIndex:(NSInteger)index;

@end
