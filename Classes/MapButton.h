//
//  MapButtin.h
//  R2
//
//  Created by stephen feggeler on 5/14/13.
//
//

#import <UIKit/UIKit.h>

@interface MapButton : UIButton

@property (nonatomic, assign) float heading;

@end
