//
//  PropertyFinder.h
//  R2
//
//  Created by stephen feggeler on 2/28/13.
//  Copyright 2013 self. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PRSConnection.h"
#import "Property.h"

@protocol PropertyFinderDelegate;

@interface PropertyFinder : NSObject <PRSConnectionDelegate>
{	
	id <PropertyFinderDelegate> delegate;
	
	NSMutableArray *properties;
	PRSConnection *connection;	
}

@property (nonatomic, assign) id <PropertyFinderDelegate> delegate;

- (void)getPropertiesFromQueryString:(NSString *)queryString;

@end

@protocol PropertyFinderDelegate

- (void)propertyFinder:(PropertyFinder *)finder
     didFindProperties:(NSMutableArray *)properties
               inRange:(NSRange)range
               ofTotal:(NSInteger)totalForRequirement;

- (void)propertyFinder:(PropertyFinder *)finder didFailWithError:(NSError *)error;

@property (nonatomic, retain) NSString *postCMD;
@property (nonatomic, assign) BOOL isAppend;
@property (nonatomic, retain) NSMutableArray *properties;

@end



