//
//  MapViewController.m
//  R2
//
//  Created by stephen feggeler on 3/26/13.
//  Copyright 2013 self. All rights reserved.
//
#import "NSTextFixes.h"
#import "UIImageView+WebCache.h"
#import "MapViewControllerOLD1.h"
#import "BasicMapAnnotationView.h"
#import "CalloutMapAnnotationView.h"
#import "PropertyCalloutView.h"
#import "PropertyDetailsViewController.h"
#import "StreetViewController.h"
#import "NSTextFixes.h"

@implementation MapViewControllerOLD1

@synthesize properties;
@synthesize mapView;
@synthesize totalProperties;
@synthesize resetMap;
@synthesize calloutMapAnnotation;
@synthesize selectedAnnotationView;
@synthesize callouts;
@synthesize prevCenter;
@synthesize postCMD;
@synthesize isAppend;
@synthesize locationManager;
@synthesize selectedPropertyIndex;

#pragma mark -
#pragma mark MKMapViewDelegate

- (void)mapView:(MKMapView *)map regionDidChangeAnimated:(BOOL)animated {
    
    if (self.properties == nil) {
        return;
    }
    
	CGFloat lat_diff = fabs( map.region.center.latitude - prevRegion.center.latitude);
	CGFloat lon_diff = fabs( map.region.center.longitude - prevRegion.center.longitude);
	
	CGFloat diff = sqrt(lat_diff + lon_diff);
	
	if(diff >= 0.002 )
    {
		prevRegion.center = map.region.center;
		[self updateMap];
	}
	
	NSLog(@"lat = %f, lon = %f, dlat = %f, dlon = %f",
		  map.region.center.latitude,
		  map.region.center.longitude,
		  map.region.span.latitudeDelta,
		  map.region.span.longitudeDelta);
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
	
	if (![view isKindOfClass:[BasicMapAnnotationView class]]) {
		return;
	}
    
	if (!((BasicMapAnnotationView *)view).preventSelectionChange)
    {
        [self removeCallout:self];
        // [self performSelector:@selector(removeCallout:) withObject:nil afterDelay:0.05];
	}
}


- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
		
	BOOL isBasicMapAnnotationView = [view isKindOfClass:[BasicMapAnnotationView class]];
	
	if (isBasicMapAnnotationView == NO) {
		return;
	}
	
	BOOL prevent = ((BasicMapAnnotationView *)view).preventSelectionChange;	
	if (prevent) {		
		return;
	}
	
	if ([view isKindOfClass: [MKPinAnnotationView class]]) {	
		
		CalloutMapAnnotation *callout = [[CalloutMapAnnotation alloc] 
             initWithLatitude:view.annotation.coordinate.latitude					   
             andLongitude:view.annotation.coordinate.longitude];
		
		callout.property = view.annotation;			
		[callouts addObject: callout];
        
        blockSetRegion =  true;
        [self performSelector:@selector(unblockSetRegion:) withObject:nil afterDelay:0.5];
        
        self.selectedAnnotationView = view;	
        [self performSelector:@selector(addCalloutAfterDelay:) withObject:callout afterDelay:0.1];
	}

}

- (void)addCalloutAfterDelay:(CalloutMapAnnotation*)callout
{
    [self.mapView addAnnotation: callout];
}

- (void)unblockSetRegion:(id)object
{
    blockSetRegion = NO;
}

-(void)removeCallout:(id)object
{
	if (callouts.count == 0) {
		return;
	}
	
	CalloutMapAnnotation *callout = (CalloutMapAnnotation*)[self.callouts objectAtIndex:0];
	
	[self.callouts removeObjectAtIndex:0];	
	[self.mapView removeAnnotation: callout];
    
    theRegion = self.mapView.region;
    
    if (blockSetRegion == NO) {
        [self performSelector:@selector(setRegion:) withObject:nil afterDelay:0.05];
    }    
}

- (void)updateMap
{				
	MKCoordinateRegion region = mapView.region;
	
	region.span.latitudeDelta = mapView.region.span.latitudeDelta/2.0;
	region.span.longitudeDelta = mapView.region.span.longitudeDelta/2.0;
	
	if(region.span.latitudeDelta < 0.0001)
	{		
		[self setPropertyRegion];
	}
	else 
	{
		R2AppDelegate *appDelegate = (R2AppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate fetchPropertiesInRegion: region];
	}			
}

- (MKAnnotationView *)mapView:(MKMapView *)aMapView viewForAnnotation:(id <MKAnnotation>)annotation {
	
	if([annotation isKindOfClass:[CalloutMapAnnotation class]])
	{	
		return [self mapView:aMapView viewForCalloutAnnotation:annotation];
	}
	
	if([annotation isKindOfClass:[Property class]])
	{	
		return [self mapView:aMapView viewForPriceAnnotation:annotation];
	}
    
    return nil;
}

//
// property callout annotation
//
- (MKAnnotationView *)mapView:(MKMapView *)aMapView viewForCalloutAnnotation:(id <MKAnnotation>)annotation
{	
	static NSString *viewIdentifier = @"callout identifier";
	
    CalloutMapAnnotationView *annotationView = (CalloutMapAnnotationView *) [aMapView dequeueReusableAnnotationViewWithIdentifier:viewIdentifier];
  
	if (annotationView == nil) {
		
    	annotationView = [[[CalloutMapAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:viewIdentifier] autorelease];		
		annotationView.contentHeight = 130.0f;	
		
		PropertyCalloutView *propertyView = [[[NSBundle mainBundle] loadNibNamed:@"PropertyCalloutAccessory" owner:self options:nil] objectAtIndex:0];
		propertyView.tag = 1;
		
		[annotationView.contentView addSubview:propertyView];
    }
	
	PropertyCalloutView *propertyView = (PropertyCalloutView *)[annotationView viewWithTag:1];
	
	Property *property = ((CalloutMapAnnotation*)annotation).property;
	
	NSString *path = [property mainThumbImage];
	
	[propertyView.mainImage setImageWithURL:[NSURL URLWithString:path]
						   placeholderImage:[UIImage imageNamed:@"placeholder"]];
	
	propertyView.mainImage.contentMode = UIViewContentModeScaleAspectFit;
	
	propertyView.label1.text = [property price];
	propertyView.label2.text = [property neighborhood];
	propertyView.label3.text = [property address1];
	propertyView.label4.text = [property address2];
	propertyView.label5.text = [property detials1];
		
	annotationView.parentAnnotationView = self.selectedAnnotationView;
	annotationView.mapView = self.mapView;
	annotationView.accessory = propertyView.accessory;
		
	[propertyView.accessory addTarget:self action:@selector(showDetailsAction:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchCancel];
	[propertyView.streetView addTarget:self action:@selector(showStreetViewAction:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchCancel];
	
    return annotationView;			
}

- (void)showStreetViewAction:(UIButton*)sender{
    
    Property * property = [(CalloutMapAnnotation *)[(MKAnnotationView*)[[[sender superview] superview] superview] annotation] property];
    
    StreetViewController *streetViewController = [[[StreetViewController alloc] initWithNibName:@"StreetView" bundle:nil] autorelease];
    
    streetViewController.lat = property.latitude;
    streetViewController.lon = property.longitude;
    streetViewController.hidesBottomBarWhenPushed = YES;
   
    [self.navigationController pushViewController:streetViewController animated:YES];
}

- (void)showCalloutForPropertyIndex:(NSInteger)index
{
    // get the property from the index
    
    Property * seekedProperty = [self.properties objectAtIndex:index];
    
    // find annotation for property
    
    NSArray * annosOnMap = [mapView annotations];
    
    // find annotation for the property
    
    id <MKAnnotation> foundAnnotation = nil;
    
    for (int i = 0; i<annosOnMap.count; ++i)
    {
        id <MKAnnotation> anAnnotation = [annosOnMap objectAtIndex:i];
        
        // if not a property continue
        
        if(![anAnnotation isKindOfClass:[Property class]])
            continue;
        
        Property * aProperty = (Property*)anAnnotation;
        
        if ([seekedProperty.mln isEqualToString:aProperty.mln]) {
            foundAnnotation = anAnnotation;
        }
    }
    
    // show the annotion if found
    
    if(foundAnnotation != nil)
    {
        [mapView selectAnnotation:foundAnnotation animated:NO];
    }
}

- (void)showDetailsAction:(UIButton*)sender{
	
	CalloutMapAnnotationView *view =
        (CalloutMapAnnotationView*)[[[sender superview] superview] superview];
    	
	Property * property = [(CalloutMapAnnotation *)[view annotation] property];
	
	PropertyDetailsViewController *detailViewController = [[PropertyDetailsViewController alloc] initWithNibName:@"PropertyDetailsViewController" bundle:nil];
	
	detailViewController.properties = properties;
	detailViewController.indexOfProperty = [properties indexOfObject:property];
	detailViewController.totalProperties = self.totalProperties;
    detailViewController.selectedPropertyDelegate = self;
    detailViewController.blockFetchMoreData = YES;
	
	int theIndex = 0;
	for (int k=0; k<properties.count; ++k) {
		
		BOOL sameMLN = [[[properties objectAtIndex:k] mln] intValue] == [[property mln] intValue];
		if(sameMLN)
		{
			theIndex = k;
			break;				
		}
	}
	
	detailViewController.indexOfProperty = theIndex;
	detailViewController.totalProperties = properties.count;
    detailViewController.hidesBottomBarWhenPushed = YES;
	
	// ...
    // Pass the selected object to the new view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
	
    [detailViewController release];    		
}


//
//	price pin annotation
//
- (MKAnnotationView *)mapView:(MKMapView *)aMapView viewForPriceAnnotation:(id <MKAnnotation>)annotation
{
	static NSString *pinIdentifier = @"price pin";
	
	BasicMapAnnotationView *av = (BasicMapAnnotationView *) [aMapView dequeueReusableAnnotationViewWithIdentifier:pinIdentifier];
	
	if (av == nil) {
		
		av = [[[BasicMapAnnotationView alloc] 
			   initWithAnnotation:annotation 
			   reuseIdentifier:pinIdentifier] autorelease];			
	}
	
	CGRect theFrame = CGRectMake(-12, 0, 40, 20);
	UIView	*containerView = [[[UIView alloc] initWithFrame:theFrame] autorelease];
	
	Property *property = (Property*)annotation;
	
	UILabel *lbl = [[[UILabel alloc] initWithFrame:theFrame] autorelease];
	lbl.backgroundColor = [UIColor blackColor];
	lbl.backgroundColor = [UIColor colorWithRed:0.205 green:0.3 blue:0.552 alpha:1.0];
	lbl.backgroundColor = [UIColor clearColor];
	lbl.textColor = [UIColor whiteColor];
	lbl.alpha = 1;
	lbl.textAlignment = NSTextAlignmentCenter;
	lbl.tag = 42;
	lbl.text = [property priceMini];
	//	lbl.adjustsFontSizeToFitWidth = YES;
	lbl.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:13];
	lbl.font = [UIFont boldSystemFontOfSize:13.5f];
	lbl.layer.cornerRadius = 8;
	
	//
	// adjust size
	//
	CGSize maximumLabelSize = CGSizeMake(296,20);
	
	CGSize expectedLabelSize = [lbl.text sizeWithFont:lbl.font 
									constrainedToSize:maximumLabelSize 
										lineBreakMode:lbl.lineBreakMode]; 
	
	//adjust the label the the new height.
	CGRect newFrame = lbl.frame;
	CGFloat newWidth = expectedLabelSize.width + 15;
	
	CGFloat xDiff = 0;
	if (newWidth > lbl.frame.size.width) {
		xDiff = (newWidth - lbl.frame.size.width) / 2;
		newFrame.size.width = newWidth;
		newFrame.origin.x -= xDiff;
	}
	
	
	lbl.frame = newFrame;
	containerView.frame = newFrame;
	
	lbl.shadowColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.35];
	lbl.shadowOffset = CGSizeMake(0, -1.0);
	
	
	CAGradientLayer *gradient = [CAGradientLayer layer];
	gradient.frame = lbl.bounds;
    
    UIColor *startColour = nil;
    UIColor *endColour = nil;
	
	//UIColor *startColour = [UIColor colorWithHue:.580555 saturation:0.31 brightness:0.70 alpha:1.0];
	//UIColor *endColour   = [UIColor colorWithHue:.58333  saturation:0.50 brightness:0.32 alpha:1.0];
	
    // purple
	//UIColor *startColour = [UIColor colorWithHue:.880555 saturation:0.31 brightness:0.70 alpha:1.0];
	//UIColor *endColour   = [UIColor colorWithHue:.88333  saturation:0.50 brightness:0.42 alpha:1.0];
	
    // blue 1
	startColour = [UIColor colorWithRed:0.0 green:0.62 blue:0.92 alpha:1.0];
	endColour = [UIColor colorWithRed:0.0 green:0.11 blue:0.42 alpha:1.0];
    
    // blue 2
    //startColour = [UIColor colorWithRed:79.0/255. green:119.0/255.0 blue:151.0/255 alpha:1.0];
	//endColour = [UIColor colorWithRed:17.0/255.0 green:37.0/255.0 blue:57.0/255. alpha:1.0];
	
    // brown
	//startColour = [UIColor colorWithRed:0.97 green:0.62 blue:0.40 alpha:1.0];
	//endColour = [UIColor colorWithRed:0.43 green:0.25 blue:0.19 alpha:1.0];
	
    // brown
	//startColour = [UIColor colorWithRed:0.95 green:0.51 blue:0.19 alpha:1.0];
	//endColour = [UIColor colorWithRed:0.32 green:0.17 blue:0.12 alpha:1.0];
	
	gradient.colors = [NSArray arrayWithObjects:(id)[startColour CGColor], (id)[endColour CGColor], nil];
	gradient.cornerRadius = 8;
	[containerView.layer insertSublayer:gradient atIndex:0];
	
	[av addSubview:containerView];
	[av addSubview:lbl];
	
	//	av.animatesDrop = YES;
	
	//Following lets the callout still work if you tap on the label...
	av.canShowCallout = NO;
	//   av.frame = lbl.frame;
	return av;		
}


- (void)removeHiddenPins
{				
	id userLocation = [mapView userLocation];
    NSMutableArray *pins = [[NSMutableArray alloc] initWithArray:[mapView annotations]];
	
    if ( userLocation != nil ) {
        [pins removeObject:userLocation]; // avoid removing user location off the map
    }
	
	// remove callouts
	[pins removeObjectsInArray:callouts];
	
	NSMutableArray *pinsToRemove = [[NSMutableArray alloc] init];
    NSMutableArray *calloutsToRemove = [[NSMutableArray alloc] init];
	
	for (Property *p in pins) {
		
		BOOL pinHidden = YES;
		for (Property *prop in properties) {
			
			@try
			{
				if ([p.mln intValue] == [prop.mln intValue]) {
					pinHidden = NO;
					break;
				}
			}
			@catch(NSException * e){
				  NSLog(@"Exception: %@", e);
			}
			@finally {
				// Added to show finally works as well
			}
		}
		if (pinHidden) {
			
			[pinsToRemove addObject:p];
			
			if (callouts.count>0) {
				CalloutMapAnnotation *callout = (CalloutMapAnnotation*)[callouts objectAtIndex:0];
				
				BOOL sameCoordinate = (callout.latitude == p.coordinate.latitude) && (callout.longitude == p.coordinate.longitude);
			
				if (sameCoordinate) {
                    [callouts removeObjectAtIndex:0];
                    [calloutsToRemove addObject: callout];
				
					theRegion = self.mapView.region;
				}
			}
		}		
	}
		
	[mapView removeAnnotations: calloutsToRemove];
	[mapView removeAnnotations: pinsToRemove];
}

-(void)setRegion:object 
{
	[self.mapView setRegion:theRegion];		
}

-(void)removeAnnotation:(NSMutableArray*)annotation
{
	[mapView removeAnnotations:annotation];	
}

- (void)removeAllPinsButUserLocation
{
    id userLocation = [mapView userLocation];
    NSMutableArray *pins = [[NSMutableArray alloc] initWithArray:[mapView annotations]];
    if ( userLocation != nil ) {
        [pins removeObject:userLocation]; // avoid removing user location off the map
    }
	
    [mapView removeAnnotations:pins];
    [pins release];
    pins = nil;
}

#pragma mark -
#pragma mark PropertyFinderDelegate

- (void)propertyFinder:(PropertyFinder *)finder didFailWithError:(NSError *)error
{
    
}

- (void)propertyFinder:(PropertyFinder *)finder didFindProperties:(NSMutableArray *)newProperties inRange:(NSRange)range ofTotal:(NSInteger) total
{
    if (self.isAppend) {
        
        [self appendProperties:newProperties ofTotal:total];
        return;
    }
    
    blockSetRegion = YES;
    
    [self removeCallout:self];
    
    // set the new list
    
	self.properties = [[newProperties mutableCopy] autorelease];
	self.totalProperties = total;
	
	[self updateTitle];			
	[self setPropertyRegion];			
}

#pragma mark -
#pragma mark PropertyFinderDelegate helpers

- (void)appendProperties:(NSMutableArray*)newProperties ofTotal:(NSInteger)total
{        
    // keep properties already on map, but don't duplicate
    
    NSMutableArray *keepOnMap = [[[NSMutableArray alloc] init] autorelease];
    
    for (Property *p in self.properties){
        
        float d_lat = [p.latitude floatValue] - mapView.region.center.latitude;
        float d_lon = [p.longitude floatValue] - mapView.region.center.longitude;
        
        BOOL inside_of_lat = fabsf(d_lat) <= mapView.region.span.latitudeDelta/2;
        BOOL inside_of_lon = fabsf(d_lon) <= mapView.region.span.longitudeDelta/2;
        
        // keep properties on map
        
        if( inside_of_lat && inside_of_lon ){
            
            BOOL already_in_list = NO;
            
            for (Property *newProperty in newProperties){
                
                if( [newProperty.mln intValue] == [p.mln intValue]){
                    already_in_list = YES;
                }
            }
            
            if(already_in_list == NO){
                
                [keepOnMap addObject:p];
            }
        }
    }
    
    // combine new list with keepOnMap
    NSMutableArray *theProperties = [[newProperties mutableCopy] autorelease];
    
    [theProperties addObjectsFromArray:keepOnMap];        
    
    // set the new list
    
	self.properties = theProperties;
	self.totalProperties = total;
	
	[self updateTitle];
	[self setPropertyRegion];
}

- (void)updateTitle
{
	NSString *results = @"No Properties";
	
	if (self.totalProperties>0) {
		results = [NSString stringWithFormat:@"%d of %d", self.properties.count, self.totalProperties];
	}
	
    self.navigationItem.title = [NSString stringWithFormat:@"Map (%@)", results];
}

-(void)setPropertyRegion
{
	if ((properties == nil)||(properties.count==0)) {
        
        [self removeAllPinsButUserLocation];
        
		return;
	}
	
	CGFloat minLat = CGFLOAT_MAX;
	CGFloat maxLat = -CGFLOAT_MAX;
	CGFloat minLon = CGFLOAT_MAX;
	CGFloat maxLon = -CGFLOAT_MAX;
    
	// remove hidden pins
	
	[self removeHiddenPins];
	
	// get the visible pins
	
	id userLocation = [mapView userLocation];
    NSMutableArray *pins = [[NSMutableArray alloc] initWithArray:[mapView annotations]];
	
	// remove callouts
	[pins removeObjectsInArray:callouts];
	
    if ( userLocation != nil ) {
        [pins removeObject:userLocation]; // avoid removing user location off the map
    }
	
	CGFloat sumLat = 0;
	CGFloat sumLon = 0;
	
	// add missing pins
	
	for (Property *property in properties) {
		
		CGFloat lat = [property.latitude floatValue];
		CGFloat lon = [property.longitude floatValue];
		
		BOOL alreadyPin = NO;
		for (Property *p in pins) {
			if ([p.mln intValue]==[property.mln intValue]) {
				alreadyPin = YES;
			}
		}
		
		if (alreadyPin == NO) {
            if ([property.showAddressToPublic isEqualToString:@"1"]
                && property.latitude != nil
                && 0.0 != [property.latitude floatValue] )
            {
                // show the property
                [self.mapView addAnnotation:property];
                
            }
            else
                continue;
        }
		
		sumLat += lat;
		sumLon += lon;
		
		minLat = MIN(minLat, lat);
		maxLat = MAX(maxLat, lat);
		minLon = MIN(minLon, lon);
		maxLon = MAX(maxLon, lon);
	}
	
    [pins release];
    pins = nil;
	
	
	if(resetMap == NO){
		return;
	}
	resetMap = NO;
    
    CGFloat centerLat = (maxLat + minLat) / 2.;
    CGFloat centerLon = (maxLon + minLon) / 2.;
    
    // set prev here
    prevRegion.center.latitude = centerLat;
    prevRegion.center.longitude = centerLon;
    
	CGFloat latDelta = (maxLat - minLat)*1.1;
	CGFloat lonDelta = (maxLon - minLon)*1.1;
	
	CLLocationCoordinate2D coordinate = {centerLat, centerLon};
	[self.mapView setRegion:
     MKCoordinateRegionMake(coordinate,
        MKCoordinateSpanMake(latDelta, lonDelta))];
    
}

#pragma mark -
#pragma mark MapViewController creation


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.resetMap = YES;
    
    self.selectedPropertyIndex = -1;
	
//	[self setPropertyRegion];
    
  //  self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:117/255.0f green:4/255.0f blue:32/255.0f alpha:1];
    
   // self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:20/255.0f green:87/255.0f blue:36/255.0f alpha:1];
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:50/255.0f green:160/255.0f blue:0/255.0f alpha:1];
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:156/255.0f green:156/255.0f blue:156/255.0f alpha:1];

    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:24/255.0f green:60/255.0f blue:97/255.0f alpha:1];

    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleBlackOpaque;
    
    self.navigationItem.hidesBackButton = YES; // Important
    
  //  UIButton *button = [[UIButton alloc] init];
    
  //  [button setImage:[UIImage imageNamed:@"193-location-arrow,png"] forState:UIControlStateNormal];
  //  [button addTarget:self action:@selector(setToCurrentLocation:)
     //   forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"193-location-arrow"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(setToCurrentLocation) forControlEvents:UIControlEventTouchUpInside];
    [button setFrame:CGRectMake(280, 25, 50, 34)];
    
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    
    [button.layer setBackgroundColor: [[UIColor colorWithRed:237/255.0f green:237/255.0f blue:237/255.0f alpha:1]CGColor]];    
    
    [button.layer setBorderWidth:1.0f];
    [button.layer setBorderColor:[[UIColor colorWithRed:.3 green:.3 blue:.3 alpha:1]CGColor]];
//  [button.layer setShadowOpacity:0.4f];
    [button.layer setCornerRadius:10];
    
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:button] autorelease];
    
    self.navigationItem.rightBarButtonItem.style = UIBarButtonItemStyleBordered;
    
    self.navigationItem.leftBarButtonItem =
      [[[UIBarButtonItem alloc]
      initWithTitle:@"Back"
      style:UIBarButtonItemStyleBordered
      target:self
      action:@selector(setToCurrentLocation)] autorelease];
    
    UIImage *smallerImage =  [[UIImage imageNamed:@"193-location-arrow"]
                              resizableImageWithCapInsets:UIEdgeInsetsMake(25.0f, 10.0f, 25.0f, 47.0f)  resizingMode:UIImageResizingModeStretch];
    
    self.navigationItem.leftBarButtonItem.image = smallerImage;
    
    
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor colorWithRed:237/255.0f green:237/255.0f blue:237/255.0f alpha:1];
    
	self.callouts = [[[NSMutableArray alloc] init] autorelease];
}


- (void)viewWillAppear:(BOOL)animated{
	
	[super viewWillAppear: animated];
    
    R2AppDelegate *appDelegate = (R2AppDelegate*)[[UIApplication sharedApplication] delegate];
	
	appDelegate.propertyFinderDelegate = self;
    
    // show the callout
    if (self.selectedPropertyIndex >= 0) {
         [self showCalloutForPropertyIndex:self.selectedPropertyIndex];
    }   
}

- (void)viewDidDisappear:(BOOL)animated
{
    self.selectedPropertyIndex = -1;
}

#pragma mark -
#pragma mark PropertySelectionChanged delegate

- (void) didChangeSelectedPropertyIndex:(NSInteger)index
{
   // [self showCalloutForPropertyIndex:index];
    self.selectedPropertyIndex = index;
}

- (void)setToCurrentLocation
{
    [self startStandardUpdates];
}


/*
 *  Location Manager
 *
 */

#pragma mark -
#pragma mark CLLocationManagerDelegate helpers

- (void)startStandardUpdates
{
    // Create the location manager if this object does not
    // already have one.
    if (nil == locationManager)
        locationManager = [[CLLocationManager alloc] init];
    
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    
    // Set a movement threshold for new events.
    locationManager.distanceFilter = 20;
    
    [locationManager startUpdatingLocation];
    
    self.mapView.showsUserLocation = YES;
}

- (void)stopStandardUpdates
{
    [locationManager stopUpdatingLocation];
}

- (void)startSignificantChangeUpdates
{
    // Create the location manager if this object does not
    // already have one.
    if (nil == locationManager)
        locationManager = [[CLLocationManager alloc] init];
    
    locationManager.delegate = self;
    [locationManager startMonitoringSignificantLocationChanges];
}

- (void)stopSignificantChangeUpdates
{
    [locationManager stopMonitoringSignificantLocationChanges];
}

#pragma mark -
#pragma mark kCLLocationManagerDelegate

// Delegate method from the CLLocationManagerDelegate protocol.
- (void)locationManager:(CLLocationManager *)manager  didUpdateLocations:(NSArray *)locations
{
    // If it's a relatively recent event, turn off updates to save power
    CLLocation* location = [locations lastObject];
    NSDate* eventDate = location.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    if (abs(howRecent) < 15.0) {
        // If the event is recent, do something with it.
        NSLog(@"latitude %+.6f, longitude %+.6f\n",
              location.coordinate.latitude,
              location.coordinate.longitude);        
        
        MKCoordinateRegion region;
        region.center = location.coordinate;
        region.span.longitudeDelta = .01;
        region.span.latitudeDelta = .01;
        [self.mapView setRegion:region animated:YES]; // Choose if you want animate or not
        
        // stop location updates
        [self stopStandardUpdates];
    }
}

#pragma mark -
#pragma mark MapViewController ending

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
