//
//  StreetViewController.h
//  R2
//
//  Created by stephen feggeler on 5/12/13.
//
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "FSRequester.h"


@interface StreetViewController : UIViewController
<UIWebViewDelegate, MKMapViewDelegate,
MKAnnotation, UIGestureRecognizerDelegate,
FSRequesterErrorDelegate>
{
    CGFloat _lastRotation;
    UISegmentedControl *_changeViewSegment;

}

@property (nonatomic, retain) IBOutlet UIWebView *webView;
@property (nonatomic, retain) IBOutlet MKMapView *mapView;
@property (nonatomic, retain) IBOutlet UIButton *mapButton;
@property (nonatomic, retain) IBOutlet UIToolbar *poiBar;
@property (nonatomic, assign) IBOutlet UIImageView *foursquere;
@property (nonatomic, assign) IBOutlet UIButton *reloadButton;

@property (nonatomic, copy) NSString *lat;
@property (nonatomic, copy) NSString *lon;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
//@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;

@property (nonatomic, assign) CGRect smallMapViewFrame;
@property (nonatomic, assign) BOOL webPageLoaded;
@property (nonatomic, copy) NSString *webPageCommand;
@property (nonatomic) NSInteger selectedMapIndex;

@property (nonatomic, retain) NSMutableDictionary *nearbyPlaces;

- (void)customBackHandler;
- (void)changeMap:(id)sender;

- (IBAction)reloadWebview:(id)sender;

- (IBAction)CafeToggle:(id)sender;
- (IBAction)barToggle:(id)sender;
- (IBAction)gasToggle:(id)sender;
- (IBAction)schoolToggle:(id)sender;
- (IBAction)shoppingToggle:(id)sender;
- (IBAction)parkToggle:(id)sender;

@end
