//
//  StreetViewController.m
//  R2
//
//  Created by stephen feggeler on 5/12/13.
//
//

#import "StreetViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "MapButton.h"
#import "SFAlertHelper.h"

#import "Foursquare2.h"
#import "FSVenue.h"
#import "FSConverter.h"


#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))
#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)

@interface StreetViewController ()

@property (nonatomic, copy) NSString *currentQuery;
@property (nonatomic, assign) id currentButton;

@end

@implementation StreetViewController

@synthesize webView;
@synthesize mapView;
@synthesize lat;
@synthesize lon;
@synthesize coordinate;
@synthesize mapButton;
@synthesize smallMapViewFrame;
@synthesize poiBar;
@synthesize webPageLoaded;
@synthesize webPageCommand;
@synthesize selectedMapIndex;
@synthesize nearbyPlaces;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        smallMapViewFrame = mapView.frame;
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated{
    
    // this hack is needed for the map view to be sized correctly
    if(_changeViewSegment.selectedSegmentIndex == 2){
        [self performSelector:@selector(showRoadView) withObject:nil afterDelay:0.1]; // 0.8
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    webPageLoaded = NO;
    
    self.nearbyPlaces = [[[NSMutableDictionary alloc] init] autorelease];
    
	// Do any additional setup after loading the view, typically from a nib.
    
    // rotation
    
    UIRotationGestureRecognizer *rotationRecognizer =
        [[[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotate:)] autorelease];
    [rotationRecognizer setDelegate:self];
    [self.webView addGestureRecognizer:rotationRecognizer];    
    
    // center segment control
	
	_changeViewSegment = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Street",@"Arial", @"Road",nil]];
	[_changeViewSegment addTarget:self action:@selector(changeMap:) forControlEvents:UIControlEventValueChanged];
		
	self.navigationItem.titleView = _changeViewSegment;
    
    switch (self.selectedMapIndex) {
        case 0:
            [self openWithStreetView];
            break;
            
        case 1:
            [self openWithArialView];
            break;
            
        case 2:
            [self openWithRoadView];
            break;
            
        default:
            [self openWithStreetView];
            break;
    }
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7){
        self.automaticallyAdjustsScrollViewInsets = YES;
        self.edgesForExtendedLayout = UIRectEdgeAll;
        
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
        [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    }
}

- (void)openWithStreetView
{
    _changeViewSegment.selectedSegmentIndex = 0;
    [self showStreetView];
}

- (void)openWithArialView
{
    _changeViewSegment.selectedSegmentIndex = 1;
    [self showArialView];
 }

- (void)openWithRoadView
{
    _changeViewSegment.selectedSegmentIndex = 2;
    [self showRoadView];
    self.mapView.hidden = YES;
}

- (void)loadWebView{
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    NSURL *url =
        [NSURL fileURLWithPath:[[NSBundle mainBundle]
                                pathForResource:@"justStreetView"
                                ofType:@"htm"
                                inDirectory:nil]];
    
    [webView loadRequest:[NSURLRequest requestWithURL:url]];   
}

- (void)showStreetView{
    
    [self hidePOIAnnotations:YES];
    
    if(webPageLoaded){
        [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"mapSteetViewLocation(%@,%@);", self.lat, self.lon]];
    }
    else{
        self.webPageCommand = [NSString stringWithFormat:@"mapSteetViewLocation(%@,%@);", self.lat, self.lon];
        [self loadWebView];
    }
    
    mapView.hidden = NO;
    mapButton.hidden = NO;
    webView.hidden = NO;
    self.reloadButton.hidden = YES;
    poiBar.hidden = YES;
    self.foursquere.hidden = YES;
    
    [self showSmallMapView];
       
    [self.view bringSubviewToFront:mapView];
    [self.view bringSubviewToFront:mapButton];
}

- (void)showArialView{
    
    [self hidePOIAnnotations:YES];
    
    if(webPageLoaded){
        [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"mapBirdsViewLocation(%@,%@);", self.lat, self.lon]];
    }
    else{
        self.webPageCommand = [NSString stringWithFormat:@"mapBirdsViewLocation(%@,%@);", self.lat, self.lon];
        [self loadWebView];
    }
    
    mapView.hidden = NO;
    mapButton.hidden = NO;
    webView.hidden = NO;
    self.reloadButton.hidden = YES;
    poiBar.hidden = YES;
    self.foursquere.hidden = YES;
    [self showSmallMapView];
    
    [self.view bringSubviewToFront:mapView];
    [self.view bringSubviewToFront:mapButton];
}

- (void)showRoadView{
    
    self.mapView.hidden = NO;
    mapButton.hidden = YES;
    webView.hidden = YES;
    self.reloadButton.hidden = YES;
    mapView.hidden = NO;
    poiBar.hidden = NO;
    self.foursquere.hidden = NO;
    
    self.foursquere.layer.zPosition = 1000;
    
    [self showBigMapView];
 }

- (void)disableSmallMap{
    
    // changing the map region enables the map.
    // We want to disable user interaction when map is small.
    // The map is small when it is a circle, and has a radius.
    
    if(self.mapView.layer.cornerRadius > 0){
        self.mapView.zoomEnabled = NO;
        self.mapView.scrollEnabled = NO;
        self.mapView.userInteractionEnabled = NO;
    }
}

- (void)mapViewDidFinishRenderingMap:(MKMapView *)mapView fullyRendered:(BOOL)fullyRendered{
    [self disableSmallMap];
}

- (void)showSmallMapView{
    
    self.mapView.clipsToBounds = YES;
    
    mapView.layer.cornerRadius = 40;//half of the width
    mapView.layer.borderColor=[UIColor whiteColor].CGColor;
    mapView.layer.borderWidth=2.0f;
    mapView.layer.shadowRadius = 3.0f;
    mapView.layer.shadowColor = [UIColor blackColor].CGColor;
    mapView.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);     
    mapView.layer.shadowOpacity = 0.5f;
    
    [self.mapView setTranslatesAutoresizingMaskIntoConstraints:NO];
    mapView.frame = self.smallMapViewFrame;
    
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake([self.lat doubleValue],[self.lon doubleValue]);
    MKCoordinateSpan span = MKCoordinateSpanMake( 0.003,  0.003); // pick desired values
    MKCoordinateRegion region = MKCoordinateRegionMake(center, span);
    
  //  MKCoordinateRegion regionAdjusted = [mapView regionThatFits:region];
  //  [mapView setRegion:region animated:YES];
    
    coordinate.longitude = region.center.longitude;
    coordinate.latitude = region.center.latitude;
    
    [mapView addAnnotation:self];
    
    self.mapView.zoomEnabled = NO;
    self.mapView.scrollEnabled = NO;
    self.mapView.userInteractionEnabled = NO;
    
    [self performSelector:@selector(disableSmallMap) withObject:Nil afterDelay:3.0];
}

- (void)showBigMapView{
    
    CGRect frame = self.view.frame;
    frame.size.height -= 44;
    
    [self.mapView setTranslatesAutoresizingMaskIntoConstraints:YES];
    self.mapView.frame = frame;
    
    self.mapView.clipsToBounds = YES;
    self.mapView.zoomEnabled = YES;
    self.mapView.scrollEnabled = YES;
    self.mapView.userInteractionEnabled = YES;
    
    mapView.layer.cornerRadius = 0;//half of the width
    mapView.layer.borderColor=[UIColor whiteColor].CGColor;
    mapView.layer.borderWidth=2.0f;
    mapView.layer.shadowRadius = 0.0f;
    mapView.layer.shadowColor = [UIColor blackColor].CGColor;
    mapView.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    mapView.layer.shadowOpacity = 0.5f;
      
    CLLocationCoordinate2D center;
    center.latitude = [self.lat floatValue];
    center.longitude = [self.lon floatValue];
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(center, 1000, 1000);
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:viewRegion];
    [self.mapView setRegion:adjustedRegion animated:NO];
    
    coordinate.longitude = center.longitude;
    coordinate.latitude = center.latitude;
    
    [mapView addAnnotation:self];
    
    [self hidePOIAnnotations:NO];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
     [webView loadHTMLString: @"" baseURL: nil];
    //[webView stringByEvaluatingJavaScriptFromString:@"document.body.innerHTML='';"];
}

-(void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    NSLog(@"%@", NSStringFromCGRect(self.mapView.frame));
    //self.mapView.frame = self.view.bounds;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{    
    return YES;
}

-(void)rotate:(id)sender
{    
    // only handle rotate for arial view
    if(_changeViewSegment.selectedSegmentIndex != 1)
        return;
    
    if([(UIRotationGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded) {
        
        _lastRotation = 0.0;
        return;
    }
    
    CGFloat rotation = 0.0 - (_lastRotation - [(UIRotationGestureRecognizer*)sender rotation]);
    
    rotation = RADIANS_TO_DEGREES(rotation);
    
    NSLog(@"Output radians as degrees: %f", rotation);
    
    if (fabs(rotation) >= 85.) {
      //  int rotationRounded = lrintf (rotation/90.) * 90;
         [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"mapBirdsViewRotation(%f);", rotation]];
        _lastRotation = [(UIRotationGestureRecognizer*)sender rotation];

    }
       
 //   _lastRotation = [(UIRotationGestureRecognizer*)sender rotation];
}

- (void)customBackHandler
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)changeMap:(id)sender{
    
    //[[NSURLCache sharedURLCache] removeAllCachedResponses];
    //[webView stringByEvaluatingJavaScriptFromString:@"document.body.innerHTML='';"];
    
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
	    
    switch(segmentedControl.selectedSegmentIndex)
    {
        case 0:
            [self showStreetView];
            break;
            
        case 1:
            [self showArialView];
            break;
            
        case 2:
            [self showRoadView];
            break;
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [SFAlertHelper showAlertForError:error delegate:nil];
}


- (void)webViewDidFinishLoad:(UIWebView *)theWebView
{
    if(webPageLoaded == YES){
        return;
    }
    
    webPageLoaded = YES;
    
    // mapBirdsViewLocation or mapSteetViewLocation
    //NSString *selectedValue = [theWebView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"mapSteetViewLocation(%@,%@);", @"37.792744",@"-122.444069"]];
    NSString *selectedValue = [theWebView stringByEvaluatingJavaScriptFromString: webPageCommand];
    
    NSLog(@"%@",selectedValue);
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    // NSLog(@"passed data from web view : %@",[[request URL] query]);
    
    // the host is used to identify the method
    
    NSString *host  = [[request URL] host];
    
    // pov changed
    
    if ([host isEqualToString:@"pov_changed"])
    {
       // NSLog(@"pov_changed : %@",[[request URL] query]);
        
        NSString *pov = [[request URL] query];
        NSString *povMod1 = [pov stringByReplacingOccurrencesOfString:@"heading=" withString:@""];        
        NSArray *povArray = [povMod1 componentsSeparatedByString:@"pitch="];
        float heading = [[povArray objectAtIndex:0] floatValue];
        
        MapButton *button = (MapButton*)mapButton;        
        button.heading = heading;
        [button setNeedsDisplay];        
        
        return NO;
    }
    
    // position changed
    
    else if ([host isEqualToString:@"position_changed"])
    {
     //   NSLog(@"position_changed : %@",[[request URL] query]);

        NSString *pos = [[request URL] query];
        NSString *posMod1 = [pos stringByReplacingOccurrencesOfString:@"%20" withString:@""];
        NSString *posMod2 = [posMod1 stringByReplacingOccurrencesOfString:@"position=(" withString:@""];
        NSString *posMod3 = [posMod2 stringByReplacingOccurrencesOfString:@")" withString:@""];
        
        NSArray *posArray = [posMod3 componentsSeparatedByString:@","];
        
        NSLog(@"lat is  : %@",[posArray objectAtIndex:0]);
        NSLog(@"lon is  : %@",[posArray objectAtIndex:1]);
        
        CLLocationCoordinate2D center;
        center.latitude = [[posArray objectAtIndex:0] floatValue];
        center.longitude = [[posArray objectAtIndex:1] floatValue];
        
        MKCoordinateRegion region = mapView.region;
        
        region.center = center;        
        region.span.latitudeDelta = 0.003;
        region.span.longitudeDelta = 0.003;
        
        region = [mapView regionThatFits:region];
        
        [mapView setRegion:region animated:NO];
        
        return NO;
    }
    
    // position changed
    
    else if ([host isEqualToString:@"map_changed"])
    {
     //   NSLog(@"map_changed : %@",[[request URL] query]);
        
        NSString *pos = [[request URL] query];
        NSString *posMod1 = [pos stringByReplacingOccurrencesOfString:@"%20" withString:@""];
        NSString *posMod2 = [posMod1 stringByReplacingOccurrencesOfString:@"position=(" withString:@""];
        NSString *posMod3 = [posMod2 stringByReplacingOccurrencesOfString:@")" withString:@""];
        
        
        NSArray *paramParts = [posMod3 componentsSeparatedByString:@"&"];
        
        NSArray *posArray = [[paramParts objectAtIndex:0] componentsSeparatedByString:@","];
        
     //   NSLog(@"lat is  : %@",[posArray objectAtIndex:0]);
     //   NSLog(@"lon is  : %@",[posArray objectAtIndex:1]);
            
        CLLocationCoordinate2D center;
        center.latitude = [[posArray objectAtIndex:0] floatValue];
        center.longitude = [[posArray objectAtIndex:1] floatValue];
        
        MKCoordinateRegion region = mapView.region;
        
        region.center = center;
        region.span.latitudeDelta = 0.003;
        region.span.longitudeDelta = 0.003;        
        region = [mapView regionThatFits:region];
        [mapView setRegion:region animated:NO];
        
        float heading = [[[paramParts objectAtIndex:1] stringByReplacingOccurrencesOfString:@"heading=" withString:@""] floatValue];
    
     //   NSLog(@"heading is  : %f",heading);
        
        MapButton *button = (MapButton*)mapButton;
        button.heading = heading;
        [button setNeedsDisplay];
        
        return NO;
    }
    
    // street links changed
    
    else if ([host isEqualToString:@"links_changed"])
    {
        //   NSLog(@"links_changed : %@",[[request URL] query]);
        return NO;
    }
    
    else if([host isEqualToString:@"error_loading"])
    {
        /*
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:NSLocalizedString(@"Not connected to Internet!", @"AlertView")
                                  message:NSLocalizedString(@"Please connect to the Internet.", @"AlertView")
                                  delegate:nil
                                  cancelButtonTitle:NSLocalizedString(@"Ok", @"AlertView")
                                  otherButtonTitles:nil, nil];
        
        [alertView show];
         */
        
        self.reloadButton.hidden = NO;
        //self.reloadButton.titleLabel.text = @"Error loading.\n Tap to retry.";
        //self.reloadButton.titleLabel.numberOfLines = 0;
        //self.reloadButton.titleLabel.textColor = [UIColor whiteColor];
        self.reloadButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        return NO;
    }
    
    if([[[request URL] query] isEqualToString:@"clickedOnLineNo"])
    {
        //Do your action here
        return NO;
    }
    return YES;
}

- (BOOL)changeButton:(UIBarButtonItem *) button {
    BOOL showPOI = NO;

    const CGFloat * tintComponents = CGColorGetComponents([button.tintColor CGColor]);
    
    if (button.tintColor == nil || 1 == *tintComponents) {
        //button.tintColor = [UIColor colorWithRed:(101/255.0) green:(206./255.0) blue:(244./255.0) alpha:1];
        button.tintColor = [UIColor  colorWithRed:(73/255.0) green:(174/255.0) blue:(234/255.0) alpha:1.];
        showPOI = YES;
    }
    else{
        button.tintColor = [UIColor whiteColor];
    }  

    return showPOI;    
}

- (void)showVenueToggle:(id)sender query:(NSString *)query
{
    self.currentQuery = query;
    self.currentButton = sender;
    
    [Foursquare2 setDelegate:self];
    
    BOOL isBusy = [[UIApplication sharedApplication] isNetworkActivityIndicatorVisible];
    
    if (isBusy) {
        return;
    }
    
    UIBarButtonItem *button = (UIBarButtonItem *)sender;
    
    BOOL added = [self changeButton:button];
    
    if(added){
        [self getVenuesForLocation:coordinate query:query];
    }
    else{
        NSArray *annotations = [self.nearbyPlaces objectForKey:query];
        if (annotations != nil) {
            [self.mapView removeAnnotations:annotations];
            [self.nearbyPlaces removeObjectForKey:query];
        }
    }
}

- (IBAction)reloadWebview:(id)sender{
    
    switch (_changeViewSegment.selectedSegmentIndex) {
        case 0:
            [self openWithStreetView];
            break;
            
        case 1:
            [self openWithArialView];
            break;
            
        case 2:
            [self openWithRoadView];
            break;
            
        default:
            [self openWithStreetView];
            break;
    }
}

- (IBAction)CafeToggle:(id)sender
{
    [self showVenueToggle:sender query:@"cafe"];
}

- (IBAction)barToggle:(id)sender{
    
    [self showVenueToggle:sender query:@"bar"];

}

- (IBAction)gasToggle:(id)sender{
    
  [self showVenueToggle:sender query:@"gas"];
    
}

- (IBAction)schoolToggle:(id)sender{
    
    [self showVenueToggle:sender query:@"school"];
}

- (IBAction)shoppingToggle:(id)sender{
    
   [self showVenueToggle:sender query:@"shopping"];
}

- (IBAction)parkToggle:(id)sender{
    
    [self showVenueToggle:sender query:@"park"];
}

- (IBAction)coffeeToggle:(id)sender{
    
    [self showVenueToggle:sender query:@"coffee"];
}

- (void)requester:(FSRequester *)requester didFailWithErrror:(NSError*)error{
    
    [self changeButton:self.currentButton];
}

-(void)getVenuesForLocation:(CLLocationCoordinate2D)location query:(NSString*)query{
    
    NSString *categoryID = nil;
    
    if ([query isEqualToString:@"bar"]) {
        categoryID = @"4bf58dd8d48988d116941735";
    }    
    else if ([query isEqualToString:@"gas"]) {
        categoryID = @"4bf58dd8d48988d113951735";
    }    
    else if ([query isEqualToString:@"school"]) {
        categoryID = @"4bf58dd8d48988d13b941735";
    }
    else if ([query isEqualToString:@"shopping"]) {
        categoryID = @"4bf58dd8d48988d118951735"; // id for grocery store
    }
    else if ([query isEqualToString:@"park"]) {
        categoryID = @"4bf58dd8d48988d163941735";
    }
    else if ([query isEqualToString:@"coffee"]) {
        categoryID = @"4bf58dd8d48988d1e0931735";
    }
    else if ([query isEqualToString:@"cafe"]) {
        categoryID = @"4bf58dd8d48988d16d941735"; // restaurant =  4bf58dd8d48988d1c4941735
    }
    
    // image url format example
    // https://irs3.4sqi.net/img/user/300x300/YI54VKYBE1H0QZ00.jpg
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    [Foursquare2 searchVenuesNearByLatitude:[NSNumber numberWithDouble:location.latitude]
								  longitude:[NSNumber numberWithDouble:location.longitude]
								 accuracyLL:nil
								   altitude:nil
								accuracyAlt:nil
									  query:nil
									  limit:nil
									 intent:intentBrowse
                                     radius:@(5000)
                                 categoryId: categoryID
								   callback:^(BOOL success, id result){
                                       
                                       [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
									   if (success) {
										   NSDictionary *dic = result;
										   NSArray* venues = [dic valueForKeyPath:@"response.venues"];
                                           FSConverter *converter = [[[FSConverter alloc] init] autorelease];
                                           NSMutableArray* annotations = [converter convertToObjects:venues query: query];
                                           [self addNearbyAnnotations:annotations forQuery:query];
									   }
								   }];
}

- (void)addNearbyAnnotations:(NSMutableArray*)annotations forQuery:(NSString*)query
{
    [self.nearbyPlaces setObject:annotations forKey:query];
    
    [mapView addAnnotations:annotations];    
}

- (MKAnnotationView *)mapView:(MKMapView *)theMapView
            viewForAnnotation:(id <MKAnnotation>)annotation
{
    // If it's the user location, just return nil.
    
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    // If it's the property location, just return nil.
    
    if ([annotation isKindOfClass:[StreetViewController class]])
        return nil;

    
    // Handle any custom annotations.
    
    if ([annotation isKindOfClass:[FSVenue class]])
    {
        // Try to dequeue an existing pin view first.
        MKAnnotationView *pinView = (MKAnnotationView*)[theMapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
        
        if (!pinView)
        {
            // If an existing pin view was not available, create one.
            pinView = [[[MKAnnotationView alloc] initWithAnnotation:annotation
                reuseIdentifier:@"CustomPinAnnotationView"] autorelease];
            
        }
        else
            pinView.annotation = annotation;
        
        NSString* query = ((FSVenue*)annotation).query;
        
        if ([query isEqualToString:@"cafe"]) {
            pinView.image = [UIImage imageNamed:@"knife-and-fork-48-3.png"];
        }
        else if ([query isEqualToString:@"gas"]) {
            pinView.image = [UIImage imageNamed:@"gas-48-1.png"];
        }
        else if ([query isEqualToString:@"shopping"]) {
            pinView.image = [UIImage imageNamed:@"shopping-cart-48-1.png"];
        }
        else if ([query isEqualToString:@"school"]) {
            pinView.image = [UIImage imageNamed:@"grad-hat-48-1.png"];
        }
        else if ([query isEqualToString:@"park"]) {
            pinView.image = [UIImage imageNamed:@"tree-48-1.png"];
        }
        else if ([query isEqualToString:@"bar"]) {
            pinView.image = [UIImage imageNamed:@"martini-48-1.png"];
        }
        else if ([query isEqualToString:@"coffee"]) {
            pinView.image = [UIImage imageNamed:@"coffee-48-8.png"];
        }
        
        pinView.canShowCallout = YES;
        pinView.centerOffset = CGPointMake(0, -20.0);
        
        // Add a detail disclosure button to the callout.
        //       UIButton* rightButton = [UIButton buttonWithType:
        //                                UIButtonTypeDetailDisclosure];
        //        [rightButton addTarget:self action:@selector(myShowDetailsMethod:)
        //              forControlEvents:UIControlEventTouchUpInside];
        //       pinView.rightCalloutAccessoryView = rightButton;
        
        return pinView;
    }
    
    return nil;
}

- (void)mapView:(MKMapView *)mv didAddAnnotationViewsHIDE:(NSArray *)views
{
    CGRect visibleRect = [mapView annotationVisibleRect];
    for (MKAnnotationView *view in views) {
        CGRect endFrame = view.frame;
        
        CGRect startFrame = endFrame;
        startFrame.origin.y = visibleRect.origin.y - startFrame.size.height;
        view.frame = startFrame;
        
        [UIView beginAnimations:@"drop" context:NULL];
        [UIView setAnimationDuration:.5];
        
        view.frame = endFrame;
        
        [UIView commitAnimations];
    }
}

- (void)hidePOIAnnotations:(BOOL)hide
{
    NSEnumerator *enumerator = [self.nearbyPlaces keyEnumerator];
    
    for (NSString* key in enumerator) {
        
        NSArray *annos = [nearbyPlaces objectForKey:key];
        
        for (int i=0; i < annos.count; ++i) {
            
            [[mapView viewForAnnotation:[annos objectAtIndex:i]] setHidden:hide];
        }
    }    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
