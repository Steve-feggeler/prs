//
//  Property.h
//  R2
//
//  Created by stephen feggeler on 2/28/13.
//  Copyright 2013 self. All rights reserved.
//

//ML_NUMBER|AREA|LISTING_PRICE|STREET_NUMBER|STREET_NUMBER_MODIFIER|STREET_DIRECTION|STREET_NAME|STREET_SUFFIX|UNIT|CITY|STATE|ZIP|BEDROOMS|BATHROOMS|SQUARE_FOOTAGE|
//LOT_SIZE_SQ_FT|LISTING_DATE|YEAR_BUILT|MARKETING_REMARKS|SHOW_ADDRESS_TO_PUBLIC|COUNTY|PROPERTY_TYPE_DESC|LISTING_AGENT_NAME|LISTING_OFFICE_NAME|PICTURES_NUMBER_OF|
//PARKING|HOA_DUES|OPEN_HOUSE_INFORMATION|OPEN_HOUSE_INFORMATION_TWO|DOM|LATITUDE|LONGITUDE|KNOWN_SHORT_SALE_DESC|REO_DESC|NUMBER_OF_UNITS|EST_GROSS_INCOME|EST_TOTAL_EXPENSES|

//NUMBER_OF_PARKING_SPACES|PROPERTY_SUBTYPE|NUMBER_PARKING_SPACES


#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <QuartzCore/QuartzCore.h>
	

@interface Property : NSObject <MKAnnotation> {
	NSString *mln;
	NSString *area;
	NSString *listingPrice;
	NSString *streetNumber;
	NSString *streetNumberModifier;
	NSString *streetDirection;
	NSString *streetName;
	NSString *streetSuffix;
	
	NSString *unit;
	NSString *city;
	NSString *state;
	NSString *zip;
	NSString *bedrooms;
	NSString *bathrooms;
	NSString *squareFootage;
	NSString *lotSizeSqFt;
	NSString *listingDate;
	NSString *yearBuilt;
	NSString *marketingRemarks;
	NSString *showAddressToPublic;
	NSString *county;
	NSString *propertyTypeDesc;
	NSString *listingAgentName;
	NSString *listingOfficeName;
	NSString *picturesNumberOf;

	NSString *parking;
	NSString *hoaDues;
	NSString *openHouseInformation;
    NSString *openHouseInformationTwo;
    NSString *openHouseDays;
	NSString *dom;
	NSString *latitude;
	NSString *longitude;
	NSString *knownShortSaleDesc;
	NSString *reoDesc;
	NSString *numberOfUnits;
	NSString *estGrossIncome;
	NSString *estTotalExpenses;
	
	NSString *numberOfParkingSpaces;
	NSString *propertySubtype;
	NSString *numberParkingSpaces;

}

@property (nonatomic,retain) NSString *mln;
@property (nonatomic,retain) NSString *area;
@property (nonatomic,retain) NSString *listingPrice;
@property (nonatomic,retain) NSString *streetNumber;
@property (nonatomic,retain) NSString *streetNumberModifier;
@property (nonatomic,retain) NSString *streetDirection;
@property (nonatomic,retain) NSString *streetName;
@property (nonatomic,retain) NSString *streetSuffix;

@property (nonatomic,retain) NSString *unit;
@property (nonatomic,retain) NSString *city;
@property (nonatomic,retain) NSString *state;
@property (nonatomic,retain) NSString *zip;
@property (nonatomic,retain) NSString *bedrooms;
@property (nonatomic,retain) NSString *bathrooms;
@property (nonatomic,retain) NSString *squareFootage;
@property (nonatomic,retain) NSString *lotSizeSqFt;
@property (nonatomic,retain) NSString *listingDate;
@property (nonatomic,retain) NSString *yearBuilt;
@property (nonatomic,retain) NSString *marketingRemarks;
@property (nonatomic,retain) NSString *showAddressToPublic;
@property (nonatomic,retain) NSString *county;
@property (nonatomic,retain) NSString *propertyTypeDesc;
@property (nonatomic,retain) NSString *listingAgentName;
@property (nonatomic,retain) NSString *listingOfficeName;
@property (nonatomic,retain) NSString *picturesNumberOf;

@property (nonatomic,retain) NSString *parking;
@property (nonatomic,retain) NSString *hoaDues;
@property (nonatomic,retain) NSString *openHouseInformation;
@property (nonatomic,retain) NSString *openHouseInformationTwo;
@property (nonatomic,retain) NSString *openHouseDays;
@property (nonatomic,retain) NSString *dom;
@property (nonatomic,retain) NSString *latitude;
@property (nonatomic,retain) NSString *longitude;
@property (nonatomic,retain) NSString *knownShortSaleDesc;
@property (nonatomic,retain) NSString *reoDesc;
@property (nonatomic,retain) NSString *numberOfUnits;
@property (nonatomic,retain) NSString *estGrossIncome;
@property (nonatomic,retain) NSString *estTotalExpenses;

@property (nonatomic,retain) NSString *numberOfParkingSpaces;
@property (nonatomic,retain) NSString *propertySubtype;
@property (nonatomic,retain) NSString *numberParkingSpaces;	

@property (nonatomic, copy) NSString *title;

- (id)initWithRecord:(NSArray *)record;
- (NSString*)mainThumbImage;
- (NSString*)price;
- (NSInteger)priceNumber;
- (NSString*)address1;
- (NSString*)address2;
- (NSString*)detials1;
- (NSString*)propertyTypeLabel;
- (NSString*)neighborhood;
- (NSArray*)bigImages;
- (NSString*)priceMini;

- (void)formatBathrooms;
- (void)formatPrice;
- (void)formatRemarks;
- (void)formatHOA;
- (void)formatListingOffice;
- (NSString*)formatDallors:(NSString*)dollars;
- (NSString*)formatOpenHouse:(NSString*)openHouseInfo;
- (NSString*)formatOpenHouseDays:(NSString*)day1 andDay:(NSString*)day2;
- (NSString*)propertyTableType;


+ (NSString*)mlnFromRecord:(NSMutableArray*)record;

+ (NSString*)bedsLabel;
+ (NSString*)bathsLabel;
+ (NSString*)strutureSizeLabel;
+ (NSString*)lotSizeLabel;
+ (NSString*)priceLabel;
+ (NSString*)yearBuiltLabel;
+ (NSString*)neighborhoodLabel;
+ (NSString*)mlsLabel;
+ (NSString*)remarksLabel;
+ (NSString*)hoaLabel;
+ (NSString*)domLabel;
+ (NSString*)parkingSpaceLabel;
+ (NSString*)numberOfUnitsLabel;
+ (NSString*)estIncomeLabel;
+ (NSString*)estExpenseLabel;

@end
