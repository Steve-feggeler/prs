#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "Property.h"

@interface CalloutMapAnnotation : NSObject <MKAnnotation> {
	
	CLLocationDegrees _latitude;
	CLLocationDegrees _longitude;
	Property *_property;
	
}

@property (nonatomic) CLLocationDegrees latitude;
@property (nonatomic) CLLocationDegrees longitude;
@property (nonatomic, retain) Property* property;

- (id)initWithLatitude:(CLLocationDegrees)latitude
		  andLongitude:(CLLocationDegrees)longitude;

@end
