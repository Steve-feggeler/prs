//
//  PropertyDetailsViewController.m
//  R2
//
//  Created by stephen feggeler on 3/16/13.
//  Copyright 2013 self. All rights reserved.
//

#import <sys/utsname.h>
#import "R2AppDelegate.h"
#import "PropertyDetailsViewController.h"
#import "Property.h"
#import "UIImageView+WebCache.h"
#import "UIButton+WebCache.h"
#import "StreetViewController.h"
#import "BCDirections.h"

#define FONT_SIZE 14.0f
#define CELL_CONTENT_WIDTH 300.0f
#define CELL_CONTENT_MARGIN 15.0f

@implementation PropertyDetailsViewController

@synthesize indexOfProperty, tableHeaderView;
@synthesize imageScroller;
@synthesize labelImageScroller;
@synthesize coordinate;
@synthesize totalProperties;
@synthesize selectedPropertyDelegate;

NSString*
machineName()
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];	

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
	
    // Set this in every view controller so that the back button displays back instead of the root view controller name
       	
	self.tableView.tableHeaderView = tableHeaderView;
	
	// setup the property
		
	[self setupPropertyDetails];
    
    // navigationItem stuff
   	[self addRightBarItemNavigation];
    [self setNavigationItemTitle];
    
    // setup show hide nav bar for landscape mode
    [self initTapGesture];
}

// this is not exact, just an approximation
- (BOOL)isInSanFrancisco:(CLLocationCoordinate2D)location{
    
    CLLocationCoordinate2D san_francisco_ctr = CLLocationCoordinate2DMake(37.758, -122.444);
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(san_francisco_ctr, 14000, 17000);
    
    double lat_distance_from_sf_ctr = fabs(location.latitude - san_francisco_ctr.latitude);
    double lon_distance_from_sf_ctr = fabs(location.longitude - san_francisco_ctr.longitude);
    
    BOOL in_lat_region = region.span.latitudeDelta/2.0 >= lat_distance_from_sf_ctr;
    BOOL in_lon_region = region.span.longitudeDelta/2.0 >= lon_distance_from_sf_ctr;
    
    return (in_lat_region && in_lon_region);
}


-(void)findMissingPropertyLocation:(Property*)property{
    
    NSString *location = [NSString stringWithFormat:@"%@, %@",property.address1, property.address2];
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder geocodeAddressString:location
                 completionHandler:^(NSArray* placemarks, NSError* error){
                     if (placemarks && placemarks.count > 0) {
                         CLPlacemark *topResult = [placemarks objectAtIndex:0];
                         MKPlacemark *placemark = [[MKPlacemark alloc] initWithPlacemark:topResult];
                         
                         // if not in san francisco, don't display property
                         if (![self isInSanFrancisco:placemark.location.coordinate]) {
                             return;
                         }
                         
                         // If this takes a long time, this property could be freed
                         @try {
                             property.latitude = [NSString stringWithFormat:@"%f", placemark.location.coordinate.latitude];
                             property.longitude = [NSString stringWithFormat:@"%f", placemark.location.coordinate.longitude];
                             
                        }
                         @catch (NSException *exception) {
                         }
                         @finally {
                             
                         }
                     }
                 }
     ];
}


- (void)initTapGesture
{
    self.tapGesture = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showHideNavbar:)] autorelease];
    self.tapGesture.enabled = NO;
    [self.view addGestureRecognizer:self.tapGesture];
}

/* 
 *  show and hide nav bar when in landscape mode
 *
 */
-(void) showHideNavbar:(id) sender
{
    __block CGRect frame = self.navigationController.navigationBar.frame;
    __block Boolean moveNavBar = NO;
    
    CGFloat yNavShown = [[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0 ? 0 : 20;
    
    // hide nav bar
    if (frame.origin.y == yNavShown) //20
    {
        frame.origin.y = -44;
        moveNavBar = YES;
    }
    
    // show nav bar
    else if (frame.origin.y == -44)
    {
        frame.origin.y = yNavShown;//20
        moveNavBar = YES;
    }
    
    if (moveNavBar) {
        [UIView animateWithDuration:0.4f
                animations:^{
                    self.navigationController.navigationBar.frame = frame;
                }
                completion:^(BOOL finished){
                }
         ];
    }    
}

- (void)addRightBarItemNavigation
{    
	UISegmentedControl *segment = [[[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"\u25B2",@"\u25BC",nil]] autorelease];
	
    // arrow bigger
    UIFont *font = [UIFont boldSystemFontOfSize:20.0f];
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
	segment.momentary = YES;
    
    // hides borders
    segment.tintColor = [UIColor clearColor];
    
    // set title attributes, font and color
    [segment setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateNormal];
    [segment setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor lightTextColor]} forState:UIControlStateSelected];
    [segment setTitleTextAttributes:attributes forState:UIControlStateNormal];
	
    // add action
	[segment addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventValueChanged];
    
	UIBarButtonItem *segmentedControlBarItem = [[[UIBarButtonItem alloc] initWithCustomView:segment] autorelease];
	
	self.navigationItem.rightBarButtonItem = segmentedControlBarItem;        
}

- (void)removedRightBarItemNavigation
{
    self.navigationItem.rightBarButtonItem = nil;
}

- (void) myCustomBack {
	
	[self.navigationController popViewControllerAnimated:YES];
}


- (void)setupPropertyDetails
{
    [self buildTableDataStructure];
    
	Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    
    // check for missing location
    CGFloat lat = [property.latitude floatValue];
    CGFloat lon = [property.longitude floatValue];
    
    BOOL isInSF = [self isInSanFrancisco:property.coordinate];
    
    if (lat == 0.0 || lon == 0.0 || (isInSF == NO)) {
          [self findMissingPropertyLocation:property];
    }

	self.imagePaths = [property bigImages];
	
	//
	// setup image scrollview
	//
    
	[self setupImageScroller1];    	
}

- (void)setNavigationItemTitle
{
    NSString *results = @"No Properties";
	
	if (self.totalProperties>0) {
		results = [NSString stringWithFormat:@"%ld of %ld", (unsigned long)(indexOfProperty + 1), (long)self.totalProperties];
	}
	
	self.navigationItem.title = results;
}

- (void)changePage:(id)sender{
	
	UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
	
	// goto previous property
	if(segmentedControl.selectedSegmentIndex == 0){
		
		if(self.indexOfProperty>0){
			--self.indexOfProperty;
			[self setupPropertyDetails];
			[self.tableView reloadData];
		}		
	}
	
	// goto next property
	else if(segmentedControl.selectedSegmentIndex == 1){
		
		if(self.indexOfProperty< (self.properties.count - 1)){
			++self.indexOfProperty;
			[self setupPropertyDetails];
			[self.tableView reloadData];
			
			
			if(self.indexOfProperty == (self.properties.count - 1)){
				
                if (!self.blockFetchMoreData)
                {
                    [self fetchMoreData];
                }
				
			}
			
		}
		
	}
    
    [self setNavigationItemTitle];
    
    Property *selectedProperty = [self.properties objectAtIndex:self.indexOfProperty];
    
    [self.selectedPropertyDelegate didChangeSelectedProperty:selectedProperty];
}

- (void)fetchMoreData{
	
	R2AppDelegate *appDelegate = (R2AppDelegate*)[[UIApplication sharedApplication] delegate];
	
	[appDelegate fetchMoreProperties];	
}

- (void)setupImageScroller1
{
  //[imageScroller setBackgroundColor:[UIColor blackColor]];
	[imageScroller setCanCancelContentTouches:NO];
    
	imageScroller.indicatorStyle = UIScrollViewIndicatorStyleWhite;
	imageScroller.clipsToBounds = YES;		// default is NO, we want to restrict drawing within our scrollview
	imageScroller.scrollEnabled = YES;
	imageScroller.pagingEnabled = YES;
    
    if (self.scrollImageDelegate1) {
        [self.scrollImageDelegate1 resetScrollView:imageScroller];
        self.scrollImageDelegate1.imageNames = nil;
        self.scrollImageDelegate1 = nil;
    }
    
	if(self.scrollImageDelegate1 == nil)
    {
		self.scrollImageDelegate1 = [[[ScrollingImageDelegate alloc] init] autorelease];
	}
	[self.scrollImageDelegate1 resetScrollView:imageScroller];	
		
	imageScroller.delegate = self.scrollImageDelegate1;
	
	CGRect scrollerFrame = imageScroller.frame;
	
    CGFloat scrollWidth = scrollerFrame.size.width;
    CGFloat scrollHeight = scrollerFrame.size.height;
	self.scrollImageDelegate1.scrollObjWidth = scrollWidth;
	self.scrollImageDelegate1.scrollObjHeight = scrollHeight;
	self.scrollImageDelegate1.imageNames = _imagePaths;
	self.scrollImageDelegate1.positionLabel = labelImageScroller;
	
	[self.scrollImageDelegate1 scrollViewAddMissingViews:imageScroller];
	
	[imageScroller setContentSize:
    CGSizeMake((_imagePaths.count * scrollWidth), scrollHeight)];
    
    ///////////////
    
    // get screen width
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    CGFloat screenWidth = screenRect.size.width;
    
    // calc header and images frames
    CGFloat headerHeight = (screenWidth - 20) * 2/3 + 20;  // had padding of 10
    CGRect headerFrame = CGRectMake(0, 0, screenWidth, headerHeight);
    // CGRect imageFrame = CGRectMake(10, 10, 300, 200);
    CGFloat imgWidth = screenWidth - 20;
    CGFloat imgHeight = imgWidth * 2.0 / 3.0;
       
    // set tableview header size
    self.tableHeaderView.frame = headerFrame;
    
    self.tableView.tableHeaderView = self.tableHeaderView;
    
    // setup images background and size
    [imageScroller setBackgroundColor:[UIColor whiteColor]];
    [self performSelector:@selector(resizeImageScroller) withObject:nil afterDelay:0.0];
    
    CGRect labelRect = labelImageScroller.frame;
    labelRect.origin.y = headerFrame.origin.y + imgHeight + labelRect.size.height;
    labelRect.origin.x = headerFrame.origin.x + (headerFrame.size.width / 2.0);
    labelRect.origin.x -= (labelRect.size.width / 2.0);
    labelImageScroller.frame = labelRect;
}

- (void) resizeImageScroller
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat imgWidth = screenRect.size.width - 20;
    CGFloat imgHeight = imgWidth * 2.0 / 3.0;
    CGRect imageFrame = CGRectMake(10, 10, imgWidth, imgHeight);
    
    [self.scrollImageDelegate1 resizeScrollView:self.imageScroller withFrame:imageFrame];
}

-(CGFloat)heightForLongText:(NSString*)text
{
    CGFloat cellWidth = [[UIScreen mainScreen] bounds].size.width;
    
	CGSize constraint = CGSizeMake(cellWidth - (CELL_CONTENT_MARGIN * 2), 20000.0f);
    
    CGRect textRect = [text boundingRectWithSize:constraint
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:FONT_SIZE]}
                                                 context:nil];
	
	CGFloat height = MAX(textRect.size.height, 44.0f);
	
	return height + (CELL_CONTENT_MARGIN * 2);	
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self setupNavigationBar];
}

/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/

- (void)showLandscapeView
{        
    self.navigationController.navigationBar.tintColor = nil;
    
    [self removedRightBarItemNavigation];
      
    self.navigationItem.hidesBackButton = YES;
    self.labelImageScroller.hidden = YES;
    
    self.scrollImageDelegate1.navigationItem = self.navigationItem;
    
    [self.scrollImageDelegate1 updatePositionLabel:imageScroller];
    
    // calc frame size for full screen
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    CGRect landscapeFrame = CGRectMake(0, 12, screenHeight, screenWidth);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        if(screenWidth != 414){
            // iphone 4s to iphone 6
            landscapeFrame = CGRectMake(0, 32, screenHeight, screenWidth);
        }
        else {
            // iphone 6 plus
            landscapeFrame = CGRectMake(0, 20, screenHeight, screenWidth);
        }
    }
    
    // set tableview header frame full screen
    self.tableHeaderView.frame = landscapeFrame;
    
    // set image scroller full screen and background color
    [self.scrollImageDelegate1 resizeScrollView:imageScroller withFrame:landscapeFrame];
    [imageScroller setBackgroundColor:[UIColor blackColor]];    
    
    // disable table scroll for landscape so only picture scrolling is allowed
    self.tableView.scrollEnabled = NO;
    
    // scroll table to top to show images
   // [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    [self.tableView setContentOffset: CGPointMake(0, 0) animated:NO];
    
    // position images behind nav bar
    //[self.tableView setContentInset:UIEdgeInsetsMake(-44,0,0,0)];
    [self.tableView setContentInset:UIEdgeInsetsMake(0,0,0,0)];
     
    // setup navbar for landscape
    //self.navigationController.navigationBar.alpha = .6;
    //self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    [self performSelector:@selector(updateNavigationBarForLandscape) withObject:nil afterDelay:0.1];
    
    // enable hiding the nav bar
     self.tapGesture.enabled = YES;
   // [self performSelector:@selector(setImageScrollviewToLandscape) withObject:nil afterDelay:0.1];
 }

- (void)updateNavigationBarForLandscape{
    self.navigationController.navigationBar.alpha = .7;
}

- (void)setImageScrollviewToLandscape{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    CGRect landscapeFrame = CGRectMake(0, 12, screenHeight, screenWidth);
    
    self.imageScroller.frame = landscapeFrame;
    
    [self performSelector:@selector(setLandscapeInset) withObject:nil afterDelay:0.1];
}

- (void)setLandscapeInset{
    [self.tableView setContentInset:UIEdgeInsetsMake(-12,0,0,0)];
}


- (void)showPortraitView
{
    [self addRightBarItemNavigation];
    [self setNavigationItemTitle];
    
    self.labelImageScroller.hidden = NO;
    
    self.navigationItem.hidesBackButton = NO;
    
    self.scrollImageDelegate1.navigationItem = nil;
    
    // nav bar setup
    /*
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:24/255.0f green:60/255.0f blue:97/255.0f alpha:1];
    self.navigationController.navigationBar.hidden = NO;
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    self.navigationController.navigationBar.alpha = 1.;
     */
    
    // get screen width
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    // tableview setup
    // position tableview, enable scrolling, position tableview
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        if (screenRect.size.height == 414)
        {
            [self.tableView setContentInset:UIEdgeInsetsMake(45,0,0,0)]; // iphone 6 plus
        }
        else
        {
            [self.tableView setContentInset:UIEdgeInsetsMake(32,0,0,0)]; // iphone 4s to 6
        }
        
    }
    else{
        [self.tableView setContentInset:UIEdgeInsetsMake(50,0,0,0)];
    }
    
    //NSString *machine = machineName();
    //float scale = [[UIScreen mainScreen] scale];
    
    self.tableView.scrollEnabled = YES;
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
      
    
    CGFloat screenWidth = screenRect.size.width;
    
    // for ios 8
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
         screenWidth = screenRect.size.height;
    }
    else{
         screenWidth = screenRect.size.width;
    }
    
    // calc header and images frames
    CGFloat headerHeight = (screenWidth - 20) * 2/3 + 20;  // had padding of 10
    CGRect headerFrame = CGRectMake(0, 0, screenWidth, headerHeight);
   // CGRect imageFrame = CGRectMake(10, 10, 300, 200);
    CGFloat imgWidth = screenWidth - 20;
    CGFloat imgHeight = imgWidth * 2.0 / 3.0;
    CGRect imageFrame = CGRectMake(10, 10, imgWidth, imgHeight);
    
    // set tableview header size
    self.tableHeaderView.frame = headerFrame;
    
    // setup images background and size
    [imageScroller setBackgroundColor:[UIColor whiteColor]];    
    [self.scrollImageDelegate1 resizeScrollView:imageScroller withFrame:imageFrame];
    
    // disable hiding nav bar
    self.tapGesture.enabled = NO;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (   toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft
        || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight)
    {
        [self showLandscapeView];
    
    }
    else if(toInterfaceOrientation == UIInterfaceOrientationPortrait) {
        
        [self showPortraitView];
    }
    [self setupNavigationBar];
 }

- (void)setupNavigationBar{
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0){
        self.automaticallyAdjustsScrollViewInsets = YES;
        self.edgesForExtendedLayout = UIRectEdgeAll;
        self.navigationController.navigationBar.translucent = YES;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
        [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    }
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSUInteger)supportedInterfaceOrientationsHIDE
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
}

-(BOOL)shouldAutorotate
{
    return YES;
}

- (NSDictionary*)rowForIndexPath:(NSIndexPath *)indexPath
{
    NSArray *sections = [self sectionsForTable];
    NSDictionary* theSection = [sections objectAtIndex:indexPath.section];
    NSArray *rows = [theSection objectForKey:@"rows"];
    NSDictionary* theRow = [rows objectAtIndex:indexPath.row];
    return theRow;
}

- (NSArray*)rowsForSection:(NSInteger)section
{
    NSArray *sections = [self sectionsForTable];
    NSDictionary* theSection = [sections objectAtIndex:section];
    NSArray *rows = [theSection objectForKey:@"rows"];
    return rows;
}

- (NSString*)titleForSection:(NSInteger)section
{
    NSArray *sections = [self sectionsForTable];
    NSDictionary* theSection = [sections objectAtIndex:section];
    return [theSection objectForKey:@"title"];
}

- (NSArray*)sectionsForTable
{
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    NSString* sectionName = [property propertyTableType];
    NSArray *sections = [self.propertyTypeData objectForKey:sectionName];
    return sections;
}


#pragma mark -
#pragma mark Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary* theRow = [self rowForIndexPath:indexPath];
    
    // get height selector
    SEL heightMethod = [[theRow objectForKey:@"height"] pointerValue];
       
    // need NSInvocation to call selector with return type
    NSInvocation *invocation =
        [NSInvocation invocationWithMethodSignature:
            [[PropertyDetailsViewController class] instanceMethodSignatureForSelector:heightMethod]];
    
    [invocation setSelector:heightMethod];
    [invocation setTarget:self];
    [invocation invoke];
    
    CGFloat height;
    [invocation getReturnValue:&height];
    
    return height;    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return [self sectionsForTable].count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self rowsForSection:section].count;    
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {

    return [self titleForSection:section];    
}

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSectionHIDE:(NSInteger)section
{
    if(section == 0)
        return 70;
    return 22.0;
}

- (NSIndexPath *)tableView:(UITableView *)tv willSelectRowAtIndexPath:(NSIndexPath *)path
{
    // Determine if row is selectable based on the NSIndexPath.
	
	/*
    if (rowIsSelectable)
    {
        return path;
    }
	*/
	
    return nil;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{	
	UITableViewCell *cell = nil;
    
    NSDictionary* theRow = [self rowForIndexPath:indexPath];
    
    // get height selector
    SEL cellMethod = [[theRow objectForKey:@"cell"] pointerValue];
    
    // need NSInvocation to call selector with return type
    NSInvocation *invocation =
    [NSInvocation invocationWithMethodSignature:
    [[PropertyDetailsViewController class] instanceMethodSignatureForSelector:cellMethod]];
    
    [invocation setSelector:cellMethod];
    [invocation setTarget:self];
    [invocation invoke];
    [invocation getReturnValue:&cell];
    
    return cell;
}

- (UITableViewCell *)directionsTableViewCell:(UITableView*)tableView
{
    static NSString *CellIdentifier = @"directions";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
	}
	
	//cell.userInteractionEnabled = NO;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
 	
    //	cell.textLabel.font = [UIFont boldSystemFontOfSize:17.0];
	cell.detailTextLabel.textColor = [UIColor darkGrayColor];
	
	return cell;
}

- (UITableViewCell *)emailAgentTableViewCell:(UITableView*)tableView
{
    static NSString *CellIdentifier = @"email agent";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
	}
	
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
 	
    //	cell.textLabel.font = [UIFont boldSystemFontOfSize:17.0];
	cell.detailTextLabel.textColor = [UIColor blueColor];
	
	return cell;
}


- (UITableViewCell *)styleValue1TableViewCell:(UITableView*)tableView
{
	static NSString *CellIdentifier = @"styleValue1";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];		
	}
	
	//cell.userInteractionEnabled = NO;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
 	
//	cell.textLabel.font = [UIFont boldSystemFontOfSize:17.0];
	cell.detailTextLabel.textColor = [UIColor darkGrayColor];
	
	return cell;	
}

- (UITableViewCell *)styleSubtitleTableViewCell:(UITableView*)tableView
{
	static NSString *CellIdentifier = @"styleSubtitle";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] autorelease];		
	}
	
	cell.selectionStyle = UITableViewCellSelectionStyleNone;	
	cell.detailTextLabel.textColor = [UIColor darkGrayColor];    
		
    return cell;	
}


- (UITableViewCell *)longTextTableViewCell:(UITableView *)tableView
{	
	static NSString *CellIdentifier = @"long Text Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	UILabel *label = nil;
	
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
		
		label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
		[label setLineBreakMode:NSLineBreakByWordWrapping];
		[label setMinimumScaleFactor:FONT_SIZE];
		[label setNumberOfLines:0];
		[label setFont:[UIFont systemFontOfSize:FONT_SIZE]];
		[label setTag:1];
		
		[[cell contentView] addSubview:label];
    }
	
	return cell;
}

- (UITableViewCell *)mapTableViewCell:(UITableView *)tableView
{
    static NSString *CellIdentifier = @"mapCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
    }
    
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    
	cell.userInteractionEnabled = YES;
    cell.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    CGRect screenFrame = [[UIScreen mainScreen] bounds];
    int widthMap = screenFrame.size.width - 20;
    int heightMap = (widthMap * 2.0 ) / 3.0;
  
    NSString *url = [NSString stringWithFormat:@"http://maps.google.com/maps/api/staticmap?size=%dx%d&center=%@,%@&zoom=13&markers=color:blue%%7Clabel:P%%7C%@,%@&sensor=false",
                      widthMap,
                      heightMap,
                      property.latitude,
                      property.longitude,
                      property.latitude,
                      property.longitude];
        
    UIButton *imageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    int widthButton = widthMap - 2;
    int heightButton = heightMap - 2;
    imageButton.frame = CGRectMake(11, 1, widthButton, heightButton);
    
    [imageButton setImageWithURL:[NSURL URLWithString:url]
    success:^(UIImage *image, BOOL cached)
    {
        
    }
    failure:^(NSError *error)
    {
        [imageButton setImage:[UIImage imageNamed:@"san-francisco-300x200.png"] forState:(UIControlStateNormal)];
    }];
    
    imageButton.adjustsImageWhenHighlighted = YES;
    [imageButton addTarget:self action:@selector(mapButtonHit:) forControlEvents:UIControlEventTouchUpInside];
    
    CALayer *layer = imageButton.layer;
	layer.masksToBounds = YES;
	layer.cornerRadius = 10.0;
    
    [cell.contentView addSubview:imageButton];
	[cell setEditing:YES];

    return cell;
}


- (UITableViewCell *)mapTableViewCell_1:(UITableView *)tableView
{	
	static NSString *CellIdentifier = @"mapCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
    }
	
	Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
	
	cell.userInteractionEnabled = YES;
	
	MKMapView *mapView = [[[MKMapView alloc] initWithFrame:CGRectMake(1, 1, 298, 198)] autorelease];
	
	mapView.mapType=MKMapTypeStandard;
	mapView.showsUserLocation=NO;
	[mapView setContentMode:UIViewContentModeBottomRight];
	
	CLLocationCoordinate2D center;
	center.latitude = [property.latitude floatValue];
	center.longitude = [property.longitude floatValue];
	
	MKCoordinateRegion region;
	region.center = center;
	
	MKCoordinateSpan span;
	span.latitudeDelta = 0.005;
	span.longitudeDelta = 0.005;
	region.span=span;
	
	[mapView setRegion:region animated:NO];
	self.coordinate = center;
	
	[mapView addAnnotation:self];
	[mapView setScrollEnabled:NO];
	[mapView setUserInteractionEnabled:YES];
    mapView.userInteractionEnabled = NO;
    mapView.exclusiveTouch = NO;
    mapView.zoomEnabled = NO;
	
	//	UIButton *mapButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 70, 70)];
	UIButton *mapButton = [[[UIButton alloc] initWithFrame:CGRectMake(10, 0, 300, 200)] autorelease];
	[mapButton setBackgroundColor:[UIColor grayColor]];
    
    [mapButton addTarget:self action:@selector(mapButtonHit:)
     forControlEvents:UIControlEventTouchUpInside];
	
	CALayer *mapLayer = mapView.layer;
	mapLayer.masksToBounds = YES;
	mapLayer.cornerRadius = 10.0;
    
	[mapButton setContentEdgeInsets:UIEdgeInsetsMake(1, 1, 1, 1)];
		
	CALayer *layer = mapButton.layer;
	layer.masksToBounds = YES;
	layer.cornerRadius = 10.0;
    
    [mapButton addSubview:mapView];
	
	[cell addSubview:mapButton];
	[cell setEditing:YES];
	
    return cell;		
}

- (void)mapButtonHit:(id)sender
{
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    
    StreetViewController *streetViewController = [[[StreetViewController alloc] initWithNibName:@"StreetView" bundle:nil] autorelease];
    
    streetViewController.lat = property.latitude;
    streetViewController.lon = property.longitude;
    streetViewController.title = property.detials1;
    streetViewController.subtitle = [NSString stringWithFormat:@"%@, %@", property.address1, property.address2];
    streetViewController.hidesBottomBarWhenPushed = YES;
    streetViewController.selectedMapIndex = 2;
    
    self.navigationItem.backBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil] autorelease];
    
    [self.navigationController pushViewController:streetViewController animated:YES];
}

- (void)getDirections:(id)sender
{
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    CLLocationCoordinate2D location;
    location.latitude = [property.latitude doubleValue];
    location.longitude = [property.longitude doubleValue];
    [BCDirections GetDirectionsLocation:location];
}

- (void)emailAgent:(id)sender
{
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    
    MFMailComposeViewController *mailVC = [[MFMailComposeViewController alloc] init];
    mailVC.mailComposeDelegate = self;
    
    NSString *messageBody = [NSString stringWithFormat:@"I am interested in a property located at %@, %@",
                             [property address1],
                             [property address2]];
    
    BOOL dont_show_address = [property.showAddressToPublic isEqualToString:@"0"];
    if (dont_show_address) {
        messageBody = [NSString stringWithFormat:@"I am interested in a property, with MLN=%@, near %@",
                       [property mln],
                       [property address2]];
    }
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7){
        
         mailVC.navigationBar.barTintColor = [UIColor colorWithHue:0.555f saturation:0.99f brightness:0.855f alpha:1.f];
         mailVC.navigationBar.barTintColor = [UIColor colorWithRed:63.0/255.0 green:131.0/255.0 blue:143.0/255.0 alpha:1.0f];
        // mailVC.navigationBar.translucent = YES;
         mailVC.navigationBar.tintColor = [UIColor whiteColor];
         [mailVC.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
         [mailVC.navigationBar setBarStyle:UIBarStyleBlack];
    }
    
    [mailVC setSubject:@"Real Estate Question"];
    [mailVC setMessageBody:messageBody isHTML:NO];
    [mailVC setToRecipients:[NSArray arrayWithObjects:@"agent@pacificRealtyService.com",nil]];
    
    [self.navigationController presentViewController:mailVC animated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }];
}

// The mail compose view controller delegate method
- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (ImagesTableViewCell *)imagesTableViewCell:(UITableView *)tableView
{
	
	static NSString *CellIdentifier = @"imageGroup";
    
    ImagesTableViewCell *cell = (ImagesTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ImagesTableViewCell" owner:self options:nil] objectAtIndex:0];
    }
	
	Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
	
	cell.imagePaths = [property bigImages];
    
    // Configure the cell...
    
    return cell;		
}

# pragma mark -
# pragma build table data structure

- (void)buildTableDataStructure
{
    self.propertyTypeData = [[[NSMutableDictionary alloc] initWithCapacity:7] autorelease];
    
    [self.propertyTypeData setObject:[self businessSections] forKey:@"BUSO"];
    [self.propertyTypeData setObject:[self commercialSections] forKey:@"COMI"];
    [self.propertyTypeData setObject:[self condoCoopTicLoftSections] forKey:@"COND"];
    [self.propertyTypeData setObject:[self lotsSections] forKey:@"LOTL"];
    [self.propertyTypeData setObject:[self twoToFourSections] forKey:@"MFM2"];
    [self.propertyTypeData setObject:[self fivePlusSections] forKey:@"MFM5"];
    [self.propertyTypeData setObject:[self singleFamilySections] forKey:@"RESI"];
}

- (NSArray*)businessSections
{
    NSMutableArray * sections = [[[NSMutableArray alloc] init] autorelease];
    
    // open house section
    [sections addObject:[self openHouseSection]];
    
    // property info section
    [sections addObject:[self infoSectionBUSO]];
    
    // description section
    [sections addObject:[self descriptionSection]];
    
    // map section
    [sections addObject:[self mapSection]];
    
    // email agent section
    [sections addObject:[self emailAgentSection]];
    
    // lister section
    [sections addObject:[self listerSection]];
    
    // disclaimer section
    [sections addObject:[self disclaimerSection]];
    
    return sections;    
}

- (NSArray*)commercialSections
{
    NSMutableArray * sections = [[[NSMutableArray alloc] init] autorelease];
    
    // open house section
    [sections addObject:[self openHouseSection]];
    
    // property info section
    [sections addObject:[self infoSectionCOMI]];
    
    // description section
    [sections addObject:[self descriptionSection]];
    
    // map section
    [sections addObject:[self mapSection]];
    
    // email agent section
    [sections addObject:[self emailAgentSection]];
    
    // lister section
    [sections addObject:[self listerSection]];
    
    // income section
    [sections addObject:[self incomeSection]];
    
    // disclaimer section
    [sections addObject:[self disclaimerSection]];
    
    return sections;
}

- (NSArray*)condoCoopTicLoftSections
{
    NSMutableArray * sections = [[[NSMutableArray alloc] init] autorelease];
    
    // open house section
    [sections addObject:[self openHouseSection]];
    
    // property info section
    [sections addObject:[self infoSection]];
    
    // description section
    [sections addObject:[self descriptionSection]];
    
    // map section
    [sections addObject:[self mapSection]];
    
    // email agent section
    [sections addObject:[self emailAgentSection]];
    
    // lister section
    [sections addObject:[self listerSection]];
    
    // disclaimer section
    [sections addObject:[self disclaimerSection]];
    
    return sections;
}

- (NSArray*)lotsSections
{
    NSMutableArray * sections = [[[NSMutableArray alloc] init] autorelease];
    
    // open house section
    [sections addObject:[self openHouseSection]];
    
    // property info section
    [sections addObject:[self infoSectionLOTL]];
    
    // description section
    [sections addObject:[self descriptionSection]];
    
    // map section
    [sections addObject:[self mapSection]];
    
    // email agent section
    [sections addObject:[self emailAgentSection]];
    
    // lister section
    [sections addObject:[self listerSection]];
    
    // disclaimer section
    [sections addObject:[self disclaimerSection]];
    
    return sections;
}

- (NSArray*)twoToFourSections
{
    NSMutableArray * sections = [[[NSMutableArray alloc] init] autorelease];
    
    // open house section
    [sections addObject:[self openHouseSection]];
    
    // property info section
    [sections addObject:[self infoSectionMFM2]];
    
    // description section
    [sections addObject:[self descriptionSection]];
    
    // map section
    [sections addObject:[self mapSection]];
    
    // email agent section
    [sections addObject:[self emailAgentSection]];
    
    // lister section
    [sections addObject:[self listerSection]];
    
    // disclaimer section
    [sections addObject:[self disclaimerSection]];
    
    return sections;
}

- (NSArray*)fivePlusSections
{
    NSMutableArray * sections = [[[NSMutableArray alloc] init] autorelease];
    
    // open house section
    [sections addObject:[self openHouseSection]];
    
    // property info section
    [sections addObject:[self infoSectionMFM5]];
    
    // description section
    [sections addObject:[self descriptionSection]];
    
    // map section
    [sections addObject:[self mapSection]];
    
    // email agent section
    [sections addObject:[self emailAgentSection]];
    
    // lister section
    [sections addObject:[self listerSection]];
    
    // disclaimer section
    [sections addObject:[self disclaimerSection]];
    
    return sections;
}

- (NSArray*)singleFamilySections
{
    NSMutableArray * sections = [[[NSMutableArray alloc] init] autorelease];
    
    // open house section
    [sections addObject:[self openHouseSection]];
    
    // property info section
    [sections addObject:[self infoSectionRESI]];
    
    // description section
    [sections addObject:[self descriptionSection]];
    
    // map section
    [sections addObject:[self mapSection]];
    
    // email agent section
    [sections addObject:[self emailAgentSection]];
    
    // lister section
    [sections addObject:[self listerSection]];
    
    // disclaimer section
    [sections addObject:[self disclaimerSection]];
    
    return sections;
}

- (NSDictionary*)dictionaryForCell:(SEL)cellSelector andHeight:(SEL)cellHeightSelector
{
    NSArray *keys = [NSArray arrayWithObjects:@"cell", @"height", nil];
    NSArray *objects =
        [NSArray arrayWithObjects:
            [NSValue valueWithPointer:cellSelector],
            [NSValue valueWithPointer:cellHeightSelector],
         nil];
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    return dictionary;
}

- (NSMutableDictionary*)infoSection
{
    NSMutableDictionary * section = [[[NSMutableDictionary alloc] init]  autorelease];
    NSMutableArray *rows = [[[NSMutableArray alloc] init] autorelease];
    
    [rows addObject: [self dictionaryForCell:@selector(cellForAddress) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForArea) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForPrice) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForType) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForBeds) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForBaths) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForHOA) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForDOM) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForStructureSize) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForLotSize) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForYearBuilt) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForParkingSpaces) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForUnitsInBuilding) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForMLN) andHeight:@selector(heightForStandardCell)]];
    
    [section setObject:rows forKey:@"rows"];
    
    return section; 
}

- (NSMutableDictionary*)infoSectionBUSO
{
    NSMutableDictionary * section = [[[NSMutableDictionary alloc] init]  autorelease];
    NSMutableArray *rows = [[[NSMutableArray alloc] init] autorelease];
    
    [rows addObject: [self dictionaryForCell:@selector(cellForAddress) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForArea) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForPrice) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForType) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForDOM) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForStructureSize) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForLotSize) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForYearBuilt) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForParkingSpaces) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForMLN) andHeight:@selector(heightForStandardCell)]];
    
    [section setObject:rows forKey:@"rows"];
    
    return section;
}

- (NSMutableDictionary*)infoSectionCOMI
{
    NSMutableDictionary * section = [[[NSMutableDictionary alloc] init]  autorelease];
    NSMutableArray *rows = [[[NSMutableArray alloc] init] autorelease];
    
    [rows addObject: [self dictionaryForCell:@selector(cellForAddress) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForArea) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForPrice) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForType) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForDOM) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForStructureSize) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForLotSize) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForYearBuilt) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForParkingSpaces) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForUnitsInBuilding) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForMLN) andHeight:@selector(heightForStandardCell)]];
    
    [section setObject:rows forKey:@"rows"];
    
    return section;
}

- (NSMutableDictionary*)infoSectionLOTL
{
    NSMutableDictionary * section = [[[NSMutableDictionary alloc] init]  autorelease];
    NSMutableArray *rows = [[[NSMutableArray alloc] init] autorelease];
    
    [rows addObject: [self dictionaryForCell:@selector(cellForAddress) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForArea) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForPrice) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForType) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForDOM) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForLotSize) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForMLN) andHeight:@selector(heightForStandardCell)]];
    
    [section setObject:rows forKey:@"rows"];
    
    return section;
}

- (NSMutableDictionary*)infoSectionMFM2
{
    NSMutableDictionary * section = [[[NSMutableDictionary alloc] init]  autorelease];
    NSMutableArray *rows = [[[NSMutableArray alloc] init] autorelease];
    
    [rows addObject: [self dictionaryForCell:@selector(cellForAddress) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForArea) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForPrice) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForType) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForDOM) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForStructureSize) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForLotSize) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForYearBuilt) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForUnitsInBuilding) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForMLN) andHeight:@selector(heightForStandardCell)]];
    
    [section setObject:rows forKey:@"rows"];
    
    return section;
}

- (NSMutableDictionary*)infoSectionMFM5
{
    NSMutableDictionary * section = [[[NSMutableDictionary alloc] init]  autorelease];
    NSMutableArray *rows = [[[NSMutableArray alloc] init] autorelease];
    
    [rows addObject: [self dictionaryForCell:@selector(cellForAddress) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForArea) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForPrice) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForEstIncome) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForEstExpense) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForType) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForDOM) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForStructureSize) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForLotSize) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForYearBuilt) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForUnitsInBuilding) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForMLN) andHeight:@selector(heightForStandardCell)]];
    
    [section setObject:rows forKey:@"rows"];
    
    return section;
}

- (NSMutableDictionary*)infoSectionRESI
{
    NSMutableDictionary * section = [[[NSMutableDictionary alloc] init]  autorelease];
    NSMutableArray *rows = [[[NSMutableArray alloc] init] autorelease];
    
    [rows addObject: [self dictionaryForCell:@selector(cellForAddress) andHeight:@selector(heightForStandardCell)]];
    
    [rows addObject: [self dictionaryForCell:@selector(cellForArea) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForPrice) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForType) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForBeds) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForBaths) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForHOA) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForDOM) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForStructureSize) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForLotSize) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForYearBuilt) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForParkingSpaces) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForMLN) andHeight:@selector(heightForStandardCell)]];
    
    [section setObject:rows forKey:@"rows"];
    
    return section;
}

- (NSMutableDictionary*)disclaimerSection
{
    NSMutableDictionary * section = [[[NSMutableDictionary alloc] init]  autorelease];
    NSMutableArray *rows = [[[NSMutableArray alloc] init] autorelease];
    
    [rows addObject: [self dictionaryForCell:@selector(cellForDisclaimer) andHeight:@selector(HeightForDisclaimerCell)]];
    
    [section setObject:rows forKey:@"rows"];
    [section setObject:[self titleForDisclaimerSection] forKey:@"title"];
    
    return section;
}


- (NSMutableDictionary*)descriptionSection
{
    NSMutableDictionary * section = [[[NSMutableDictionary alloc] init]  autorelease];
    NSMutableArray *rows = [[[NSMutableArray alloc] init] autorelease];
    
    [rows addObject: [self dictionaryForCell:@selector(cellForDescription) andHeight:@selector(HeightForDescriptionCell)]];
    
    [section setObject:rows forKey:@"rows"];
    [section setObject:[self titleForDescriptionSection] forKey:@"title"];
    
    return section;
}

- (NSMutableDictionary*)mapSection
{
    NSMutableDictionary * section = [[[NSMutableDictionary alloc] init]  autorelease];
    
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    
    BOOL dont_show_address = [property.showAddressToPublic isEqualToString:@"0"];
    
    // dont show map in address hidden
    if (dont_show_address) {
        return section;
    }
    
    NSMutableArray *rows = [[[NSMutableArray alloc] init] autorelease];
        
    [rows addObject: [self dictionaryForCell:@selector(cellForMap) andHeight:@selector(heightForMapCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForDirections) andHeight:@selector(heightForStandardCell)]];
    
    [section setObject:rows forKey:@"rows"];
    [section setObject:[self titleForLocationSection] forKey:@"title"];
    
    return section;
}

- (NSMutableDictionary*)emailAgentSection
{
    NSMutableDictionary * section = [[[NSMutableDictionary alloc] init]  autorelease];
    NSMutableArray *rows = [[[NSMutableArray alloc] init] autorelease];
    
    [rows addObject: [self dictionaryForCell:@selector(cellForEmailAgent) andHeight:@selector(heightForStandardCell)]];
    
    [section setObject:rows forKey:@"rows"];
    [section setObject:[self titleForEmailAgentSection] forKey:@"title"];
    
    return section;
}

- (NSMutableDictionary*)listerSection
{
    NSMutableDictionary * section = [[[NSMutableDictionary alloc] init]  autorelease];
    NSMutableArray *rows = [[[NSMutableArray alloc] init] autorelease];
    
    [rows addObject: [self dictionaryForCell:@selector(cellForListingAgent) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForListingOffice) andHeight:@selector(heightForStandardCell)]];
    
    [section setObject:rows forKey:@"rows"];
    [section setObject:[self titleForListerSection] forKey:@"title"];
    
    return section;
}

// income section

- (NSMutableDictionary*)incomeSection
{
    NSMutableDictionary * section = [[[NSMutableDictionary alloc] init]  autorelease];
    NSMutableArray *rows = [[[NSMutableArray alloc] init] autorelease];
    
    [rows addObject: [self dictionaryForCell:@selector(cellForEstIncome) andHeight:@selector(heightForStandardCell)]];
    [rows addObject: [self dictionaryForCell:@selector(cellForEstExpense) andHeight:@selector(heightForStandardCell)]];
    
    [section setObject:rows forKey:@"rows"];
    [section setObject:[self titleForIncomeSection] forKey:@"title"];
        
    return section;
}

// open house section

- (NSMutableDictionary*)openHouseSection
{
    NSMutableDictionary * section = [[[NSMutableDictionary alloc] init] autorelease];
    NSMutableArray *rows = [[[NSMutableArray alloc] init] autorelease];
    
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    
    BOOL hasOpenHouse = NO;
    
    if ((property.openHouseInformation != nil) && (property.openHouseInformation.length > 0)) {
        [rows addObject: [self dictionaryForCell:@selector(cellForOpenHouse) andHeight:@selector(heightForStandardCell)]];
        hasOpenHouse = YES;
    }
    
    if ((property.openHouseInformationTwo != nil) && (property.openHouseInformationTwo.length > 0)) {
        [rows addObject: [self dictionaryForCell:@selector(cellForOpenHouseTwo) andHeight:@selector(heightForStandardCell)]];
        hasOpenHouse = YES;
    }
    
    if (hasOpenHouse) {
        [section setObject:rows forKey:@"rows"];
        [section setObject:[self titleForOpenHouseSection] forKey:@"title"];        
    }

    return section;
}

# pragma mark -
# pragma table cell heights

- (CGFloat)heightForStandardCell
{
    return 44;
}

- (CGFloat)heightForMapCell
{
    return 200;
}

- (CGFloat)HeightForDescriptionCell
{
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    
    return [self heightForLongText:property.marketingRemarks];
}

static NSString *disclaimer = @"© 2013 San Francisco Association of Realtors. All rights reserved. Listings on this page identified as belonging to another listing firm are based upon data obtained from the SFAR MLS, which data is copyrighted by the San Francisco Association of REALTORS©, but is not warranted. IDX information is provided exclusively for consumers' personal, non-commercial use and may not be used for any purpose other than to identify prospective properties consumers may be interested in purchasing. Listing Broker has attempted to offer accurate data, but buyers are advised to confirm all data provided. DRE 01408938.";

- (CGFloat)HeightForDisclaimerCell
{
    return [self heightForLongText:[self fullDisclaimerText]];
}

# pragma mark -
# pragma titles for sections

- (NSString*)titleForDisclaimerSection
{
    return @"Disclaimer";
}

- (NSString*)titleForDescriptionSection
{
    return @"Property Description";
}

- (NSString*)titleForLocationSection
{
    return @"Property Location";
}

- (NSString*)titleForEmailAgentSection
{
    return @"Contact Us";
}

- (NSString*)titleForListerSection
{
    return @"Listing By";
}

- (NSString*)titleForIncomeSection
{
    return @"Income Information";
}

- (NSString*)titleForOpenHouseSection
{
    return @"Open House Information";
}


# pragma mark -
# pragma table cell selectors

- (UITableViewCell*)cellForAddress
{
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    UITableViewCell *cell = cell = [self styleSubtitleTableViewCell:self.tableView];
    
    cell.textLabel.text = [property address1];
    cell.detailTextLabel.text = [property address2];
    
    BOOL dont_show_address = [property.showAddressToPublic isEqualToString:@"0"];
    if (dont_show_address) {
        cell.textLabel.text = @"Address intentionally hidden";
    }
    
    return cell;
}

- (UITableViewCell*)cellForArea
{
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];    
    UITableViewCell *cell = cell = [self styleValue1TableViewCell:self.tableView];
    
    cell.textLabel.text = [Property neighborhoodLabel];
    cell.detailTextLabel.text = [property neighborhood];
    
    return cell;
}

- (UITableViewCell*)cellForPrice
{
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];    
    UITableViewCell *cell = cell = [self styleValue1TableViewCell:self.tableView];
    
    cell.textLabel.text = [Property priceLabel];
    cell.detailTextLabel.text = property.listingPrice;
    
    return cell;
}

- (UITableViewCell*)cellForType
{
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    UITableViewCell *cell = cell = [self styleValue1TableViewCell:self.tableView];
    
    cell.textLabel.text = @"type";
    cell.detailTextLabel.text = property.propertyTypeDesc;

    return cell;
}

- (UITableViewCell*)cellForBeds
{
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    UITableViewCell *cell = cell = [self styleValue1TableViewCell:self.tableView];
    
    cell.textLabel.text = [Property bedsLabel];
    cell.detailTextLabel.text = [property bedrooms];
    
    return cell;
}

- (UITableViewCell*)cellForBaths
{
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    UITableViewCell *cell = cell = [self styleValue1TableViewCell:self.tableView];
    
    cell.textLabel.text = [Property bathsLabel];
    cell.detailTextLabel.text = property.bathrooms;
    
    return cell;
}

- (UITableViewCell*)cellForHOA
{
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    UITableViewCell *cell = cell = [self styleValue1TableViewCell:self.tableView];
    
    cell.textLabel.text = [Property hoaLabel];
    cell.detailTextLabel.text = property.hoaDues;
    
    return cell;
}

- (UITableViewCell*)cellForDOM
{
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    UITableViewCell *cell = cell = [self styleValue1TableViewCell:self.tableView];
    
    cell.textLabel.text = [Property domLabel];
    cell.detailTextLabel.text = property.dom;
    
    return cell;
}

- (UITableViewCell*)cellForStructureSize
{
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    UITableViewCell *cell = cell = [self styleValue1TableViewCell:self.tableView];
    
    cell.textLabel.text = [Property strutureSizeLabel];
    cell.detailTextLabel.text = property.squareFootage;
    
    NSString * land_size = property.squareFootage;
    if (land_size.length>0) {
        float size = [land_size floatValue];
        NSString *formated_size;
        formated_size = [NSString localizedStringWithFormat:@"%.0F sq ft", size];
        cell.detailTextLabel.text = formated_size;
    }
    
    return cell;
}

- (UITableViewCell*)cellForLotSize
{
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    UITableViewCell *cell = cell = [self styleValue1TableViewCell:self.tableView];
    
    cell.textLabel.text = [Property lotSizeLabel];
    cell.detailTextLabel.text = property.lotSizeSqFt;
    
    NSString * land_size = property.lotSizeSqFt;
    if (land_size.length>0) {
        float size = [land_size floatValue];
        NSString *formated_size;
        if (size <= 100) {
            formated_size = [NSString localizedStringWithFormat:@"%.2F acres", size];
        }
        else{
            formated_size = [NSString localizedStringWithFormat:@"%.0F sq ft", size ];
        }
        
        cell.detailTextLabel.text = formated_size;
    }
    
    return cell;
}

- (UITableViewCell*)cellForYearBuilt
{
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    UITableViewCell *cell = cell = [self styleValue1TableViewCell:self.tableView];
    
    cell.textLabel.text = [Property yearBuiltLabel];
    cell.detailTextLabel.text = [property yearBuilt];
    
    return cell;
}

- (UITableViewCell*)cellForParkingSpaces
{
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    UITableViewCell *cell = cell = [self styleValue1TableViewCell:self.tableView];
    
    cell.textLabel.text = [Property parkingSpaceLabel];
    cell.detailTextLabel.text = [property numberOfParkingSpaces];
    
    return cell;
}

- (UITableViewCell*)cellForUnitsInBuilding
{
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    UITableViewCell *cell = cell = [self styleValue1TableViewCell:self.tableView];
    
    cell.textLabel.text = [Property numberOfUnitsLabel];
    cell.detailTextLabel.text = property.numberOfUnits;
    
    return cell;
}

- (UITableViewCell*)cellForMLN
{
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    UITableViewCell *cell = [self styleValue1TableViewCell:self.tableView];
    
    cell.textLabel.text = @"MLS ID";
    cell.detailTextLabel.text = property.mln;
    
    return cell;
}

- (NSString*)fullDisclaimerText{
    
    // keep year current
    NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setDateFormat:@"yyyy"];
    NSString *yearString = [formatter stringFromDate:[NSDate date]];
    
    // text disclaimer has current year
    NSString *text = [disclaimer stringByReplacingOccurrencesOfString:@"2013" withString:yearString];
    
    // current date
    NSDate *date = [NSDate date];
    
    // get hour and minutes
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:date];
    NSInteger minute = [components minute];
    
    // server updates every 15 minutes, 01,16,31, and 46
    minute = minute % 15;
    minute -= 1;
    if (minute < 0) {
        minute = 14;
    }
    
    NSDateFormatter *dateFormat = [[[NSDateFormatter alloc] init] autorelease];
    [dateFormat setDateFormat:@"MMM dd, yyyy hh:mm a"];
    
    NSDate *oldDate = [date dateByAddingTimeInterval:-60*minute];
    
    NSString *dateString = [dateFormat stringFromDate:oldDate];
    
    NSString *totalText = [NSString stringWithFormat:@"%@\nLast updated: %@", text, dateString];
    
    return totalText;
}

- (UITableViewCell*)cellForDisclaimer
{
    UITableViewCell *cell = [self longTextTableViewCell:self.tableView];
    
    cell.userInteractionEnabled = NO;
    
    CGFloat cellWidth = [[UIScreen mainScreen] bounds].size.width;
    
    CGSize constraint = CGSizeMake(cellWidth - (CELL_CONTENT_MARGIN * 2), 20000.0f);
    
    NSString *totalText = [self fullDisclaimerText];
    
    CGRect textRect = [totalText boundingRectWithSize:constraint
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:FONT_SIZE]}
                                              context:nil];
    
    UILabel *label = (UILabel*)[cell viewWithTag:1];
    
    [label setText:totalText];
    [label setFrame:CGRectMake(CELL_CONTENT_MARGIN, CELL_CONTENT_MARGIN, cellWidth - (CELL_CONTENT_MARGIN * 2), MAX(textRect.size.height, 44.0f))];
    
    return cell;
}

- (UITableViewCell*)cellForDescription
{
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    UITableViewCell *cell = [self longTextTableViewCell:self.tableView];
    
    UILabel *label = nil;
    
    cell.userInteractionEnabled = NO;
    
    NSString *text = property.marketingRemarks;
    
    CGFloat cellWidth = [[UIScreen mainScreen] bounds].size.width;
    
    CGSize constraint = CGSizeMake(cellWidth - (CELL_CONTENT_MARGIN * 2), 20000.0f);
    
    CGRect textRect = [text boundingRectWithSize:constraint
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:FONT_SIZE]}
                                              context:nil];
    if (!label)
        label = (UILabel*)[cell viewWithTag:1];
    
    [label setText:text];
    [label setFrame:CGRectMake(CELL_CONTENT_MARGIN, CELL_CONTENT_MARGIN, cellWidth - (CELL_CONTENT_MARGIN * 2), MAX(textRect.size.height, 44.0f))];

    return cell;
}

- (UITableViewCell*)cellForMap
{
    UITableViewCell* cell = [self mapTableViewCell:self.tableView];
    return cell;
 }

- (UITableViewCell*)cellForDirections
{
    UITableViewCell *cell = [self directionsTableViewCell:self.tableView];
    cell.textLabel.text = @"";
    cell.detailTextLabel.text = @"";
   // cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    cell.backgroundColor = [UIColor clearColor];
    cell.backgroundView.backgroundColor = [UIColor clearColor];
    cell.backgroundView = [[[UIView alloc] initWithFrame:CGRectZero] autorelease];
    
    UIButton *direction_btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    //direction_btn.frame = CGRectMake(0, 0, 293, 36);  // only size matters
    direction_btn.frame = CGRectMake(170, 0, 160, 36);  // only size matters

    [direction_btn setTitle:@"Get Directions" forState:UIControlStateNormal];
    [direction_btn addTarget:self action:@selector(getDirections:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *nearby_btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    nearby_btn.frame = CGRectMake(0, 0, 140, 36);  // only size matters
    nearby_btn.titleLabel.textAlignment = NSTextAlignmentRight;
    [nearby_btn setTitle:@"See Location" forState:UIControlStateNormal];
    [nearby_btn addTarget:self action:@selector(mapButtonHit:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.contentView addSubview:direction_btn];
    [cell.contentView addSubview:nearby_btn];
    
    //cell.accessoryView = direction_btn;
    
    return cell;
}

- (UITableViewCell*)cellForEmailAgent
{
    UITableViewCell *cell = [self emailAgentTableViewCell:self.tableView];
    cell.textLabel.text = @"";
    cell.detailTextLabel.text = @"";
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    cell.backgroundColor = [UIColor clearColor];
    cell.backgroundView.backgroundColor = [UIColor clearColor];
    cell.backgroundView = [[[UIView alloc] initWithFrame:CGRectZero] autorelease];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
    //UIColor *color = [UIColor colorWithRed:36/255.0 green:71/255.0 blue:113/255.0 alpha:1.0];
    UIColor *color = [UIColor colorWithRed:46/255.0 green:81/255.0 blue:136/255.0 alpha:1.0];
    //UIColor *color = [UIColor colorWithRed:61/255.0 green:111/255.0 blue:251/255.0 alpha:1.0];
    btn.layer.backgroundColor = [color CGColor];
    btn.layer.cornerRadius = 5;
    //btn.layer.borderColor = [[UIColor colorWithRed:36/255.0 green:71/255.0 blue:113/255.0 alpha:1.0] CGColor];
    //btn.layer.borderWidth = 2.0f;
    
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    btn.titleLabel.font = [UIFont boldSystemFontOfSize:14.0];
    
    CGFloat widthBtn = [[UIScreen mainScreen] bounds].size.width - 33;//27
   
    btn.frame = CGRectMake(0, 0, widthBtn, 36);  // only size matters
    [btn setTitle:@"Contact Agent" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(emailAgent:) forControlEvents:UIControlEventTouchUpInside];
    cell.accessoryView = btn;
    
    return cell;
}

- (UITableViewCell*)cellForListingAgent
{
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    UITableViewCell *cell = cell = [self styleValue1TableViewCell:self.tableView];

    cell.textLabel.text = @"Agent";
    cell.detailTextLabel.text = property.listingAgentName;
   
    return cell;
}

- (UITableViewCell*)cellForListingOffice
{
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    UITableViewCell *cell = cell = [self styleValue1TableViewCell:self.tableView];

    cell.textLabel.text = @"Office";
    cell.detailTextLabel.text = property.listingOfficeName;

    return cell;
}

- (UITableViewCell*)cellForEstIncome
{
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    UITableViewCell *cell = cell = [self styleValue1TableViewCell:self.tableView];

    cell.textLabel.text = [Property estIncomeLabel];
    if ([property.estGrossIncome isEqualToString:@"$0"])
    {
        cell.detailTextLabel.text = @"";
    }
    else{
        cell.detailTextLabel.text = property.estGrossIncome;

    }

    return cell;
}

- (UITableViewCell*)cellForEstExpense
{
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    UITableViewCell *cell = cell = [self styleValue1TableViewCell:self.tableView];

    cell.textLabel.text = [Property estExpenseLabel];
       
    if ([property.estTotalExpenses isEqualToString:@"$0"])
    {
        cell.detailTextLabel.text = @"";
    }
    else{
        cell.detailTextLabel.text = property.estTotalExpenses;
        
    }

    return cell;
}

- (UITableViewCell*)cellForOpenHouse
{
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    UITableViewCell *cell = cell = [self styleValue1TableViewCell:self.tableView];

    cell.textLabel.text = property.openHouseInformation;
    cell.detailTextLabel.text = @"";
    
    return cell;
}

- (UITableViewCell*)cellForOpenHouseTwo
{
    Property *property = (Property*)[self.properties objectAtIndex:self.indexOfProperty];
    UITableViewCell *cell = cell = [self styleValue1TableViewCell:self.tableView];

    cell.textLabel.text = property.openHouseInformationTwo;
    cell.detailTextLabel.text = @"";

    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
    /*
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
    // ...
    // Pass the selected object to the new view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];
    */
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}

- (void)dealloc {
    [_imagePaths release];
    [_properties release];
    [_tapGesture release];
    [_propertyTypeData release];
    [_scrollImageDelegate1 release];
    
    [super dealloc];
}


@end

