//
//  Property.m
//  R2
//
//  Created by stephen feggeler on 2/28/13.
//  Copyright 2013 self. All rights reserved.
//

#import "Property.h"
#import "DataController.h"


@implementation Property

@synthesize title;

@synthesize mln;
@synthesize area;
@synthesize listingPrice;
@synthesize streetNumber;
@synthesize streetNumberModifier;
@synthesize streetDirection;
@synthesize streetName;
@synthesize streetSuffix;

@synthesize unit;
@synthesize city;
@synthesize state;
@synthesize zip;
@synthesize bedrooms;
@synthesize bathrooms;
@synthesize squareFootage;
@synthesize lotSizeSqFt;
@synthesize listingDate;
@synthesize yearBuilt;
@synthesize marketingRemarks;
@synthesize showAddressToPublic;
@synthesize county;
@synthesize propertyTypeDesc;
@synthesize listingAgentName;
@synthesize listingOfficeName;
@synthesize picturesNumberOf;

@synthesize parking;
@synthesize hoaDues;
@synthesize openHouseInformation;
@synthesize openHouseInformationTwo;
@synthesize openHouseDays;
@synthesize dom;
@synthesize latitude;
@synthesize longitude;
@synthesize knownShortSaleDesc;
@synthesize reoDesc;
@synthesize numberOfUnits;
@synthesize estGrossIncome;
@synthesize estTotalExpenses;

@synthesize numberOfParkingSpaces;
@synthesize propertySubtype;
@synthesize numberParkingSpaces;

- (id) initWithRecord:(NSArray *)record
{
	if ((self = [super init]))
    {
		self.mln = [record objectAtIndex:0];		
		self.listingPrice = [record objectAtIndex:1];
		self.area = [record objectAtIndex:2];
		
		// skip "lastlistingPrice" since it is not used. add one
		
		self.streetNumber = [record objectAtIndex:4];
		self.streetNumberModifier = [record objectAtIndex:5];
		self.streetDirection = [record objectAtIndex:6];
		self.streetName = [record objectAtIndex:7];
		self.streetSuffix = [record objectAtIndex:8];
		
		self.unit = [record objectAtIndex:9];
		self.city = [record objectAtIndex:10];
		self.state = [record objectAtIndex:11];
		self.zip = [record objectAtIndex:12];
		self.bedrooms = [record objectAtIndex:13];
		self.bathrooms = [record objectAtIndex:14];
		self.squareFootage = [record objectAtIndex:15];
		self.lotSizeSqFt = [record objectAtIndex:16];
		self.listingDate = [record objectAtIndex:17];
		self.yearBuilt = [record objectAtIndex:18];
		self.marketingRemarks = [record objectAtIndex:19];
		self.showAddressToPublic = [record objectAtIndex:20];
		self.county = [record objectAtIndex:21];
		self.propertyTypeDesc = [record objectAtIndex:22];
		self.listingAgentName = [record objectAtIndex:23];
		self.listingOfficeName = [record objectAtIndex:24];
		self.picturesNumberOf = [record objectAtIndex:25];
		
		self.parking = [record objectAtIndex:26];
		self.hoaDues = [record objectAtIndex:27];
		self.openHouseInformation = [record objectAtIndex:28];
        self.openHouseInformationTwo = [record objectAtIndex:29];
		self.dom = [record objectAtIndex:30];
		self.latitude = [record objectAtIndex:31];
		self.longitude = [record objectAtIndex:32];
		self.knownShortSaleDesc = [record objectAtIndex:33];
		self.reoDesc = [record objectAtIndex:34];
		self.numberOfUnits = [record objectAtIndex:35];
		self.estGrossIncome = [record objectAtIndex:36];
		self.estTotalExpenses = [record objectAtIndex:37];
		
		self.numberOfParkingSpaces = [record objectAtIndex:38];
		self.propertySubtype = [record objectAtIndex:39];
		self.numberParkingSpaces = [record objectAtIndex:40];
		
		[self formatBathrooms];
		[self formatPrice];
		[self formatRemarks];
		[self formatHOA];
		[self formatListingOffice];
        
        self.openHouseDays = [self formatOpenHouseDays:self.openHouseInformation andDay:self.openHouseInformationTwo];        
		self.openHouseInformation = [self formatOpenHouse: self.openHouseInformation];
        self.openHouseInformationTwo = [self formatOpenHouse: self.openHouseInformationTwo];
		
		self.estTotalExpenses = [self formatDallors:self.estTotalExpenses];
		self.estGrossIncome = [self formatDallors:self.estGrossIncome];
		
		self.title = [self neighborhood];
	}
	
    return self;
}

- (CLLocationCoordinate2D)coordinate {
	
	CLLocationCoordinate2D coordinate;
	coordinate.latitude = [self.latitude floatValue];
	coordinate.longitude = [self.longitude floatValue];
	
	//NSLog(@"street %@",[self address1]);
	
	return coordinate;
}

// this method takes the unformated openHouse strings from the server

- (NSString*)formatOpenHouseDays:(NSString*)day1 andDay:(NSString*)day2
{
    NSString * days = @"";
    NSString * firstDay = @"";
    NSString * secondDay = @"";
    
    NSArray *parts = [day1 componentsSeparatedByString:@"|"];
    
    if (parts.count >= 4 ) {
		
		// get date parts
        NSArray *numberParts = [[parts objectAtIndex:0] componentsSeparatedByString:@"/"];
        
		firstDay = [NSString stringWithFormat:@"open %@/%@",[numberParts objectAtIndex:0],  [numberParts objectAtIndex:1]];
    }             
    
    parts = [day2 componentsSeparatedByString:@"|"];

    if (parts.count >= 4 ) {
		
		// get date parts
        NSArray *numberParts = [[parts objectAtIndex:0] componentsSeparatedByString:@"/"];
        
		secondDay = [NSString stringWithFormat:@"%@/%@",[numberParts objectAtIndex:0],  [numberParts objectAtIndex:1]];
        
        // open two days to format here
        
        if (day1.length >0)
        {
            days = [NSString stringWithFormat:@"%@ %@", firstDay, secondDay];
        }
        else
        {
            days = [NSString stringWithFormat:@"open %@", day2];
        }               
    }
    else
    {
        // open one day.  If not open at all, Thats OK.
        
        days = firstDay;
    }        
    
    return days;
}


- (NSString*)formatOpenHouse:(NSString*)openHouseInfo
{
	NSArray *parts = [openHouseInfo componentsSeparatedByString:@"|"];
	
	if (parts.count >= 4 ) {
		
		// get date parts
		NSString *date1 = [parts objectAtIndex:0];
		NSString *time1 = [parts objectAtIndex:1];
		NSString *date2 = [parts objectAtIndex:2];
		NSString *time2 = [parts objectAtIndex:3];	
		
		// first date
		//
		NSString *aDateString = [NSString stringWithFormat:@"%@ %@",date1,time1];
		
		// make date 1
		NSDateFormatter* formatter = [[[NSDateFormatter alloc] init] autorelease];
		[formatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];		
		NSDate* date = [formatter dateFromString:aDateString];
		
		// date 1
		[formatter setDateFormat:@"EEE, MMM d"];
		NSString *d1 = [formatter stringFromDate:date];
		
		// time 1
		[formatter setDateFormat:@"h:mm"];
		NSString *t1 = [formatter stringFromDate:date];
		
		// second date
		//
		aDateString = [NSString stringWithFormat:@"%@ %@",date2,time2];
		
		// make date 2
		[formatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
		date = [formatter dateFromString:aDateString];
				
		// time 2
		[formatter setDateFormat:@"h:mm aaa"];
		NSString *t2 = [formatter stringFromDate:date];

		NSString *totalDate = [NSString stringWithFormat:@"%@ from %@ to %@",d1,t1,t2];
		
		//self.openHouseInformation = totalDate;
        
        return totalDate;
	}
    return @"";
}

- (void)formatBathrooms
{
	if(self.bathrooms.length){
		
		
		NSRange range = [self.bathrooms rangeOfString:@"."];
		CFIndex index = range.location;
		
		CFIndex index1 = range.location + 1;
		unichar ch1 = [self.bathrooms characterAtIndex:index1];
		
		CFIndex index2 = range.location + 2;
		unichar ch2= [self.bathrooms characterAtIndex:index2];
		
		
		if (ch2 != '0') {
			index += 3;
		}
		else if(ch1 != '0'){
			index += 2;
		}
		
		NSString *subString = [self.bathrooms substringToIndex: index];
		
		NSString *formated = [NSString stringWithFormat:@"%@", subString];
		
		self.bathrooms = formated;
	}	
}

- (NSString*)priceMini
{
	NSString *ps = [self.listingPrice stringByReplacingOccurrencesOfString:@"$" withString:@""];
	ps =[ps stringByReplacingOccurrencesOfString:@"," withString:@""];
	int price = [ps intValue];
	
	if (price<1000) {
		return [NSString stringWithFormat:@"%d", price];
	}
	
	else if(price<1000000){ // 1 million
		price /= 1000;
		return [NSString stringWithFormat:@"%dK", price];
	}
	
	else if(price<1000000000){ // 1 billion
		float p = [ps floatValue];
		p /= 1000000;
		NSString *s = [NSString stringWithFormat:@"%.02fM", p];
		return s;
	}
	else{
		price /= 1000000000;
		return [NSString stringWithFormat:@"%dB", price];
	}
}

- (void)formatPrice
{
	NSString *sPrice = self.listingPrice;
	
	NSMutableString *formatedPrice = [[[NSMutableString alloc] init] autorelease];
	
	int cnt=0;
	for(NSInteger i=sPrice.length-1; i>=0; --i){
		
		unichar chr = [sPrice characterAtIndex:i];
		NSString *s_chr = [NSString stringWithCharacters:&chr length:1];
		
		if ( cnt >0 && cnt%3==0) {
			[formatedPrice insertString:@"," atIndex:0];
		}
		++cnt;
		
		[formatedPrice insertString:s_chr atIndex:0];
	}
	
	[formatedPrice insertString:@"$" atIndex:0];
	
	self.listingPrice = formatedPrice;
}

- (void)formatHOA
{
	CGFloat nHOA = [self.hoaDues floatValue];
	
	NSString *hoa = [NSString stringWithFormat:@"$%.2f",nHOA];
	
	self.hoaDues = hoa;
	
}

- (NSString*)formatDallors:(NSString*)dollars
{	
	int money = [dollars intValue];
	NSString *sPrice = [NSString stringWithFormat:@"%d",money];
	
	NSMutableString *formatedPrice = [[[NSMutableString alloc] init] autorelease];
	
	int cnt=0;
	for(NSInteger i=sPrice.length-1; i>=0; --i){
		
		unichar chr = [sPrice characterAtIndex:i];
		NSString *s_chr = [NSString stringWithCharacters:&chr length:1];
		
		if ( cnt >0 && cnt%3==0) {
			[formatedPrice insertString:@"," atIndex:0];
		}
		++cnt;
		
		[formatedPrice insertString:s_chr atIndex:0];
	}
	
	[formatedPrice insertString:@"$" atIndex:0];
	
	return formatedPrice;
}


- (void)formatRemarks
{
	self.marketingRemarks = [self.marketingRemarks stringByReplacingOccurrencesOfString:@"^" withString:@","];		
}

- (void)formatListingOffice
{
	self.listingOfficeName = [self.listingOfficeName stringByReplacingOccurrencesOfString:@"^" withString:@","];		
}


- (NSString*)price{
		
	return self.listingPrice;
}

- (NSInteger)priceNumber{
    
    NSString *number = [self.listingPrice stringByReplacingOccurrencesOfString:@"$" withString:@""];
    number = [number stringByReplacingOccurrencesOfString:@"," withString:@""];
    return number.integerValue;
}

- (NSString*)address1
{
	NSMutableString *ms = [[[NSMutableString alloc] init] autorelease];
	
	if(self.streetNumber.length){
		
		[ms appendFormat:@"%@",self.streetNumber];
	}
	
	if(self.streetNumberModifier.length){
		
		[ms appendFormat:@"-%@",self.streetNumberModifier];
	}
	
	if(self.streetDirection.length){
		
		[ms appendFormat:@" %@",self.streetDirection];
	}
	
	if(self.streetName.length){
		
		[ms appendFormat:@" %@",self.streetName];
	}
	
	if(self.streetSuffix.length){
		
		[ms appendFormat:@" %@",self.streetSuffix];
	}
	
	if(self.unit.length){
		
		[ms appendFormat:@", #%@",self.unit];
	}
	
	NSString *adr = [[ms copy] autorelease];

	return adr;
}

- (NSString*)address2
{
	NSMutableString *ms = [[[NSMutableString alloc] init] autorelease];
	
	if(self.city.length){
		
		[ms appendFormat:@"%@",self.city];
	}
	
	if(self.state.length){
		
		[ms appendFormat:@", %@",self.state];
	}
	
	if(self.zip.length){
		
		[ms appendFormat:@" %@",self.zip];
	}	
	
	NSString *adr = [[ms copy] autorelease];
	
	return adr;
}

- (NSString*)detials1
{
	NSMutableString *ms = [[[NSMutableString alloc] init] autorelease];
	
	[ms appendFormat:@"%@, ", self.propertyTypeLabel];
	
	if(self.bedrooms.length){
		
		[ms appendFormat:@"%@ Bd",self.bedrooms];
	}
	
	if(self.bathrooms.length){
		
		[ms appendFormat:@", %@ Ba,", bathrooms];
	}
	
	if(self.dom.length){
		
		[ms appendFormat:@" %@ DOM",self.dom];
	}
	
	NSString *adr = [[ms copy] autorelease];
	
	return adr;
}

- (NSString*)propertyTableType
{
	NSString *type = self.propertyTypeDesc;
	
	if ([type isEqualToString:@"Single-Family Homes"]) {
		
		type = @"RESI";
	}
	
	else if([type isEqualToString:@"Business Op"])
	{
		type = @"BUSO";
	}
	
	else if([type isEqualToString:@"Commercial For Sale"])
	{
		type = @"COMI";
	}
	
	else if([type isEqualToString:@"Condominium"])
	{
		type = @"COND";
	}
	
	else if([type isEqualToString:@"Stock Cooperative"])
	{
		type = @"COND";
	}
	
	else if([type isEqualToString:@"Loft Condominium"])
	{
		type = @"COND";
	}
	
	else if([type isEqualToString:@"Tenancy In Common"])
	{
		type = @"COND";
	}
	
	else if([type isEqualToString:@"Lots & Acreage"])
	{
		type = @"LOTL";
	}
    else if([type isEqualToString:@"5 - 15 Units"])
	{
		type = @"MFM5";
	}
	
	else if(numberOfUnits.length>0 && ![numberOfUnits isEqualToString:@"0"])
	{
        if([numberOfUnits intValue]<5)
        {
            type = @"MFM2";
        }
        else
        {
             type = @"MFM5";
        }
	}
    else{
        // should not get here
        type = @"RESI";
    }
	
	
	return type;
}


- (NSString*)propertyTypeLabel
{
	NSString *type = self.propertyTypeDesc;
	
	if ([type isEqualToString:@"Single-Family Homes"]) {
		
		type = @"SFR";
	}
	
	else if([type isEqualToString:@"Business Op"])
	{
		type = @"Business";		 
	}
	
	else if([type isEqualToString:@"Commercial For Sale"])
	{
		type = @"Commercial";		 
	}
	
	else if([type isEqualToString:@"Condominium"])
	{
		type = @"CON";		 
	}
	
	else if([type isEqualToString:@"Stock Cooperative"])
	{
		type = @"SC";		 
	}
	
	else if([type isEqualToString:@"Loft Condominium"])
	{
		type = @"Loft";		 
	}
	
	else if([type isEqualToString:@"Tenancy In Common"])
	{
		type = @"TIC";		 
	}
	
	else if([type isEqualToString:@"Lots & Acreage"])
	{
		type = @"LOTS";		 
	}
	
	else if(numberOfUnits.length>0 && ![numberOfUnits isEqualToString:@"0"])
	{
		type = [NSString stringWithFormat:@"%@ Units", numberOfUnits];
	}
	
	
	return type;
}

- (NSString*)neighborhood
{
	NSString *hood = [DataController neighborhoodFromArea:self.area];	
	
	return hood;
}




- (NSString*)mainThumbImage
{
	//http://www.pacificrealtyservice.com/Content/Img/70/405170_t.jpg
	
	NSInteger i = self.mln.length - 2;
	
	if(i<0){
		return nil;		
	}
	
	NSString *dir = [self.mln substringWithRange:NSMakeRange(i, 2)];
	
	NSString *path = [NSString stringWithFormat:@"http://www.pacificrealtyservice.com/Content/Img/%@/%@_t.jpg", dir, self.mln];
	
	return path;	
}


- (NSArray*)bigImages
{
	NSInteger i = self.mln.length - 2;
	
	if(i<0){
		return nil;		
	}
	
	NSString *dir = [self.mln substringWithRange:NSMakeRange(i, 2)];
	
	int numberOfPictures = [picturesNumberOf intValue];
	
	NSMutableArray *paths = [[[NSMutableArray alloc] initWithCapacity:numberOfPictures] autorelease];
	
	for(int i=0; i<numberOfPictures; ++i){
		
		NSString *path = nil;
		
		if (i>0 && i<10) {
			
			path = [NSString stringWithFormat:@"http://www.pacificrealtyservice.com/Content/Img/%@/%@_0%d.jpg", dir, self.mln, i];
		}
		else if(i>=10){
		
			path = [NSString stringWithFormat:@"http://www.pacificrealtyservice.com/Content/Img/%@/%@_%d.jpg", dir, self.mln, i];
		}
		else if(i==0){
			path = [NSString stringWithFormat:@"http://www.pacificrealtyservice.com/Content/Img/%@/%@.jpg", dir, self.mln];			
		}
		
		if (path != nil) {
			
			[paths addObject:path];
		}
		
	}
	
	return paths;			
}


+ (NSString*)mlnFromRecord:(NSMutableArray*)record
{
	
	return @"mln";
}

+ (NSString*)bedsLabel
{
	return @"Beds";
}

+ (NSString*)bathsLabel
{
	return @"Baths";
}

+ (NSString*)strutureSizeLabel
{
	return @"Stucture Size";
}

+ (NSString*)lotSizeLabel
{
	return @"Lot Size";
	
}

+ (NSString*)priceLabel
{
	return @"Price";
}

+ (NSString*)yearBuiltLabel
{
	return @"Year Built";
}
+ (NSString*)neighborhoodLabel
{
	return @"Area";
}

+ (NSString*)mlsLabel
{
	return @"MLS #";
}

+ (NSString*)remarksLabel
{
	return @"Property Description";
}

+ (NSString*)hoaLabel
{
	return @"HOA";
}

+ (NSString*)domLabel
{
	return @"Days On Market";
}

+ (NSString*)parkingSpaceLabel
{
	return @"Parking Spaces";
}

+ (NSString*)numberOfUnitsLabel
{
	return @"Units in Building";
}

+ (NSString*)estIncomeLabel
{
	return @"Est. Income";
}

+ (NSString*)estExpenseLabel
{
	return @"Est. Expense";
}


- (void) dealloc {
	
	[mln release];
	[area release];
	[listingPrice release];
	[streetNumber release];
	[streetNumberModifier release];
	[streetDirection release];
	[streetName release];
	[streetSuffix release];
	
	[unit release];
	[city release];
	[state release];
	[zip release];
	[bedrooms release];
	[bathrooms release];
	[squareFootage release];
	[lotSizeSqFt release];
	[listingDate release];
	[yearBuilt release];
	[ marketingRemarks release];
	[showAddressToPublic release];
	[county release];
	[propertyTypeDesc release];
	[listingAgentName release];
	[listingOfficeName release];
	[ picturesNumberOf release];
	
	[parking release];
	[hoaDues release];
	[openHouseInformation release];
    [openHouseInformationTwo release];
    [openHouseDays release];
	[dom release];
	[latitude release];
	[longitude release];
	[knownShortSaleDesc release];
	[reoDesc release];
	[numberOfUnits release];
	[estGrossIncome release];
	[estTotalExpenses release];
	
	[numberOfParkingSpaces release];
	[propertySubtype release];
	[numberParkingSpaces release];	
		
	[super dealloc]; 
}

@end
