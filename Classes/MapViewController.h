//
//  MapViewController.h
//  R2
//
//  Created by stephen feggeler on 3/26/13.
//  Copyright 2013 self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <QuartzCore/QuartzCore.h>

#import "R2AppDelegate.h"
#import "PropertyFinder.h"
#import "SFPropertiesPinView.h"
#import "CalloutMapAnnotation.h"
#import "PropertyDetailsViewController.h"

@interface MapViewController : UIViewController
    <MKMapViewDelegate, PropertyFinderDelegate,
    CLLocationManagerDelegate, PropertySelectionChanged, SFPropertyPinDelegate,UIGestureRecognizerDelegate>
{
	NSMutableArray *callouts;
	MKCoordinateRegion theRegion;    
    MKCoordinateRegion prevRegion;
    BOOL blockSetRegion;
}

@property (nonatomic, retain) NSMutableArray *callouts;
@property (nonatomic, retain) IBOutlet MKMapView *mapView;
@property (nonatomic, assign) NSInteger totalProperties;
@property (nonatomic, assign) bool resetMap;
@property (nonatomic, retain) CalloutMapAnnotation *calloutMapAnnotation;
@property (nonatomic, retain) MKAnnotationView *selectedAnnotationView;
@property (nonatomic, assign) CLLocationCoordinate2D prevCenter;
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, assign) NSInteger selectedPropertyIndex;
@property (nonatomic, assign) Property *selectedProperty;
@property (nonatomic, retain) NSMutableArray * properties;

- (void)setPropertyRegion;
- (void)updateTitle;
- (void)updateMap;
- (void)removeCallout:(id)object;
- (void)unblockSetRegion:(id)object;
- (void)showCalloutForPropertyIndex:(Property*)property;

@end
