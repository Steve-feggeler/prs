//
//  RemoteConnection.m
//  R2
//
//  Created by stephen feggeler on 2/28/13.
//  Copyright 2013 self. All rights reserved.
//

#import "PRSConnection.h"


@implementation PRSConnection

@synthesize delegate;


- (void)performSearch:(NSString *)queryString // already utf 8 encoded
{
	NSURL *searchURL = [NSURL URLWithString:@"http://www.pacificrealtyservice.com/home/CSVPropertyList"];

	// create an NSURLRequest from the created NSURL
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:searchURL];
    
    request.timeoutInterval = 45;

	//NSString *queryString = @"price=Any&dom=Any&pageSize=50&sqfeet=Any&beds=Any&baths=Any&property_subtype=RESI&county=San+Francisco&city=Any&area=4-A+Balboa+Terrace&currentPage=0&lastPage=-1";	
	NSData *postData = [queryString dataUsingEncoding:NSISOLatin1StringEncoding];	

	[request setHTTPMethod:@"POST"];
	[request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)postData.length] forHTTPHeaderField:@"Content-Length"];
	[request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
	[request setHTTPBody:postData];


	// create an NSURLConnection object with the created NSURLRequest
	NSURLConnection *connection =[[NSURLConnection alloc] initWithRequest:request delegate:self];

	[request release]; // release the request NSURLRequest

	// if the NSURLConnection was successfully created
	if (connection)
	{
		// create recieved data
		receivedData = [[NSMutableData data] retain];
		
		// display the standard network activity indicator in the status bar
		[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
		
		[connection start];
		
	} // end if
	else
		NSLog(@"search \"%@\" could not be performed", queryString);
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{	
	NSLog(@"didReceiveResponse");	
	
	[receivedData setLength:0];
}

// called when the NSURLConnection fails
- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    NSLog(@"Connection Error is: %@", [error localizedDescription]);
    
    [delegate prsConnection:self didFailWithError:error];
    
	[receivedData release];
	[connection release];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data { 
	
	NSLog(@"didReceiveData");
	
	[receivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	
	// hide the network activity indicator in the status bar
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	NSLog(@"Succeeded! Received %lu bytes of data", (unsigned long)[receivedData length]);
		
	// pass the received data to the delegate
	[delegate prsConnection:self didReceiveData:receivedData];
	
	[receivedData release]; 
	[connection release];
	
}


@end
