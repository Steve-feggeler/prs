//
//  PropertyCalloutView.m
//  R2
//
//  Created by stephen feggeler on 4/23/13.
//  Copyright 2013 self. All rights reserved.
//

#import "PropertyCalloutView.h"


@implementation PropertyCalloutView

@synthesize label1;
@synthesize label2;
@synthesize label3;
@synthesize label4;
@synthesize label5;
@synthesize mainImage;
@synthesize accessory;


- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
		
    }
    return self;
}

- (void)didMoveToSuperview {
	
	[super didMoveToSuperview];
	
	[self.accessory addTarget: self 
					   action: @selector(showDetails) 
			 forControlEvents: UIControlEventTouchUpInside | UIControlEventTouchCancel];	
}


-(void) showDetails
{
	NSLog(@"details");
}

-(void)calloutAccessoryTapped
{
	
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code.
}
*/

- (void)dealloc {
    [super dealloc];
}


@end
