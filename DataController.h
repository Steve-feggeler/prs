//
//  DataController.h
//  R1
//
//  Created by stephen feggeler on 1/29/13.
//  Copyright 2013 self. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "ListItem.h"

@interface DataController : NSObject <NSCoding> {
	
	NSInteger beds;
	NSInteger baths;
	NSInteger dom;
	NSInteger structureSize;
	NSInteger price;
	NSInteger resultsPerPage;
    
    BOOL openHousesOnly;
    BOOL shortSaleOnly;
    BOOL reoSaleOnly;
    BOOL priceReducedOnly;
	
	NSInteger selectedLocationList;
}

@property (nonatomic, assign) NSInteger beds;
@property (nonatomic, assign) NSInteger baths;
@property (nonatomic, assign) NSInteger dom;
@property (nonatomic, assign) NSInteger price;
@property (nonatomic, assign) NSInteger structureSize;
@property (nonatomic, assign) NSInteger resultsPerPage;

@property (nonatomic, assign) BOOL openHousesOnly;
@property (nonatomic, assign) BOOL shortSaleOnly;
@property (nonatomic, assign) BOOL reoSaleOnly;
@property (nonatomic, assign) BOOL priceReducedOnly;

@property (nonatomic, retain) NSMutableArray *propertyTypes;
@property (nonatomic, retain) NSMutableArray *zipcodes;
@property (nonatomic, retain) NSMutableArray *neighborhoods;
@property (nonatomic, retain) NSMutableArray *area;

@property (nonatomic, assign) NSInteger selectedLocationList;

- (void)MutableArray:(NSMutableArray *)theArray addItemName:(NSString*)name withKey:(NSString*)key isSelected:(BOOL)isSelected;
- (NSString*)selectedPropertyTypes;
- (NSString*)selectedLocations;
- (NSString*)getPostCMDForPage:(NSInteger)page;
- (NSString*)getPostCMDForPage:(NSInteger)page region:(MKCoordinateRegion)region;
- (NSString*)CMDFromListItemArray:(NSMutableArray*)items named:(NSString*)name;

+ (NSMutableArray*) bathsArray;
+ (NSMutableArray*) bedsArray;
+ (NSMutableArray*) domArray;
+ (NSMutableArray*) pricesArray;
+ (NSMutableArray*) structureSizeArray;
+ (NSMutableArray*) resultsPerPageArray;
+ (NSMutableArray*) locationTypeNames;
+ (NSMutableDictionary*) keyedDictionaryFromArray:(NSArray*)array;
+ (NSString*) neighborhoodFromArea:(NSString *)area;

@end
