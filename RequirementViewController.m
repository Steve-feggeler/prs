//
//  FlipsideViewController.m
//  R1
//
//  Created by stephen feggeler on 12/20/12.
//  Copyright 2012 self. All rights reserved.
//

#import "RequirementViewController.h"

static const int rowHeight = 50; // the height of each row in the table

#define FIRST_SECTION_SIZE 5
#define SECOND_SECTION_SIZE 3

@implementation RequirementViewController

@synthesize delegate;
@synthesize table;
@synthesize dataController;

// called by the AddViewController when finished adding a person to the contacts	 
- (void)checkListControllerDidFinishSelecting:(CheckListController *)controller
{		
	[self dismissViewControllerAnimated:YES completion:nil];
		
	[table reloadData];
}

- (void)viewDidLoad {
	
    [super viewDidLoad];
	
	R2AppDelegate *appDelegate = (R2AppDelegate *)[[UIApplication sharedApplication] delegate];
		
	self.dataController = appDelegate.dataController;
	
	self.table.rowHeight = rowHeight; // set the table's row height
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7){
        self.automaticallyAdjustsScrollViewInsets = YES;
        self.edgesForExtendedLayout = UIRectEdgeAll;
        self.edgesForExtendedLayout = UIRectEdgeTop | UIRectEdgeLeft | UIRectEdgeRight;
        
        
        
        /*
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithHue:0.555f saturation:0.99f brightness:0.855f alpha:1.f];
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:46.0/255.0 green:68.0/255.0 blue:132.0/255.0 alpha:0.9f];
        self.navigationController.navigationBar.translucent = YES;
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
        [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
        */
    }    
}

-(void)viewWillDisappear:(BOOL)animated
{
	R2AppDelegate *appDelegate = (R2AppDelegate *)[[UIApplication sharedApplication] delegate];
	
	[appDelegate saveDataController];	
}

-(void)savePropertyTypes{
	
	if (propertyTypes==nil) {
		return;
	}
	
	// creates list of valid directories for saving a file
	NSArray *paths = NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES);
	
	// get the first directory because we care about only one
	NSString *directory = [paths objectAtIndex:0];
	
	// concatenate the file name "data.propertyTypes" to the end of the path
	NSString *filePath = [[NSString alloc] initWithString:
						  [directory stringByAppendingPathComponent:@"data.propertyTypes"]];	
		
	// archive the slideshows to the file data.slideshows
	[NSKeyedArchiver archiveRootObject:propertyTypes toFile:filePath];		
	
	[filePath release];
}

-(NSString*)getFilePath:(NSString*)filename{
	
	// find this app's documents directory
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	
	// get the first directory
	NSString *directory = [paths objectAtIndex:0];
	
	// concatenate the file name "data.propertyTypes" to the end of the path
	NSString *filePath = [[[NSString alloc] initWithString:
						  [directory stringByAppendingPathComponent:filename]] autorelease];
	return filePath;	
}

- (IBAction)done:(id)sender {
		
	[self.delegate flipsideViewControllerDidFinish:self];	
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;		
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void) MultiCheckListController:(MultiCheckListController *)controller doneWithCheckList:(NSMutableArray*)list selectedListIndex:(NSInteger)index
{	
	dataController.selectedLocationList = index;
	
	[self dismissViewControllerAnimated:YES completion:nil];
	
	[table reloadData];
}

-(void)showPropertyTypes
{
	// initialize the ContactViewController
	CheckListController *controller = [[CheckListController alloc] initWithNibName:@"CheckListController" bundle:nil];	
	
	controller.delegate = self;
	controller.itemsArray = dataController.propertyTypes;		
		
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
	
    [self presentViewController:navigationController animated:YES completion:nil];
    
    [navigationController release];
    [controller release];
}

-(void)showLocationsView
{
	// initialize the ContactViewController
	MultiCheckListController *locationController = [[MultiCheckListController alloc] initWithNibName:@"MultiCheckListView" bundle:nil];
	
	CheckListItem *zips = [[CheckListItem alloc] init];
	zips.name = @"Zip";
	zips.list = dataController.zipcodes;
  	
	CheckListItem *neighborhoods = [[CheckListItem alloc] init];
	neighborhoods.name = @"NBHD";
	neighborhoods.list = dataController.neighborhoods;
	
	CheckListItem *areas = [[CheckListItem alloc] init];
	areas.name = @"Area";
	areas.list = dataController.area;
	
	NSMutableArray *locationLists = [[NSMutableArray alloc] initWithObjects:neighborhoods,zips, areas, nil];

	locationController.checkLists = locationLists;
	locationController.selectedList = dataController.selectedLocationList;
	
	[zips release];
	[neighborhoods release];
	[areas release];
	[locationLists release];
						
	locationController.delegate = self;	
	
	UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:locationController];
	
    [self presentViewController:navigationController animated:YES completion:nil];
    
    [navigationController release];
    [locationController release];		
}

-(ListItem*)listItemWithName:(NSString*)name key:(NSString*)key isSelected:(BOOL)isSelected{
    
    ListItem *item = [[[ListItem alloc] init] autorelease];
	item.name = name;
	item.key = key;
	item.isSelected = isSelected;
    
    return item;
}

-(NSMutableArray*)listItemList
{
    NSMutableArray * list = [[[NSMutableArray alloc] init] autorelease];
    
    ListItem *item = nil;
    
    item = [self listItemWithName:@"Any" key:@"Any" isSelected:NO];
    [list addObject:item];
    
    item = [self listItemWithName:@"red" key:@"red" isSelected:NO];
    [list addObject:item];
    
    item = [self listItemWithName:@"green" key:@"green" isSelected:NO];
    [list addObject:item];
    
    item = [self listItemWithName:@"blue" key:@"blue" isSelected:NO];
    [list addObject:item];
    
    return list;
}


- (void)pickerViewDemo
{
	CGRect screenRect = [[UIScreen mainScreen] bounds];
	
	//CGFloat screenWidth = screenRect.size.width;
	//CGFloat screenHeight = screenRect.size.height;
	
	mask = [[UIView alloc] initWithFrame: screenRect];
	[mask setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.0]];//0.78
	[self.view addSubview:mask];
	
	
	pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 480, 320, 200)];	
	pickerView.delegate = self;
	pickerView.showsSelectionIndicator = YES;
	
	[self.view addSubview:pickerView];
	[pickerView release];
	
	pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 480, 320, 44)];
		
	//Create a button 
    UIBarButtonItem *infoButton = [[UIBarButtonItem alloc] 
								   initWithTitle:@"done"
								   style:UIBarButtonItemStyleBordered 
								   target:self 
								   action:@selector(pickerDone:)];
	
    [pickerToolbar setItems:[NSArray arrayWithObjects:infoButton,nil]];	
	
	[infoButton release];
	
	[self.view addSubview:pickerToolbar];
	[pickerToolbar release];
	
	[UIView beginAnimations:nil context:pickerView];
	
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDuration:0.5];
	
	[pickerView setFrame:CGRectMake(0, 244, 320, 200)];
	[pickerToolbar setFrame:CGRectMake(0, 200, 320, 44)];
	[mask setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.57]];	
	
	[UIView commitAnimations];	
}

// called when the user touches one of the rows in the table
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
	if (indexPath.section == 0 && indexPath.row == 0) {
		
		[self showPropertyTypes];
		
	}
	else if(indexPath.section == 0 && indexPath.row == 1){
		
		
		[self showLocationsView];
	}
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
} 

-(void)pickerDone:(id)sender {
	
	[UIView beginAnimations:nil context:pickerView];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDuration:0.5];
	[UIView setAnimationDidStopSelector:@selector(finishedHidingPicker)];
	
	[pickerView setFrame:CGRectMake(0, 480, 320, 200)];
	[pickerToolbar setFrame:CGRectMake(0, 480, 320, 44)];
	[mask setBackgroundColor:[UIColor colorWithWhite:0.0 alpha:0.0]];	
	
	[UIView commitAnimations];		
}

-(void)finishedHidingPicker
{
	[pickerView removeFromSuperview];
	[pickerToolbar removeFromSuperview];
	[mask removeFromSuperview];
}

static NSString *instructions = @"When searching for businesses, commercial real estate, lots and land, or multi-unit properties, the beds and baths requirements must be set to 'Any'.";

static NSString *notes = @"This Application only shows real estate for San Francisco.  Sometimes the listing agent chooses to hide the property address from public view.  In the map view, this is labeled as 'blocked'.  The location of these properties is somewhere on the map.";

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat height = 44;
    
    CGFloat cellWidth = [[UIScreen mainScreen] bounds].size.width;
	
	if (indexPath.row == 0 && indexPath.section == 1) {
		height =  [SliderRequirementsCell height];
	}
    else if(indexPath.row == 0 && indexPath.section == 3){
        
        #define FONT_SIZE 13.0f
        #define CELL_CONTENT_WIDTH 300.0f
        #define CELL_CONTENT_MARGIN 15.0f
        
        CGSize constraint = CGSizeMake(cellWidth - (CELL_CONTENT_MARGIN * 2), 20000.0f);
        CGRect textRect = [instructions boundingRectWithSize:constraint
                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:FONT_SIZE]}
                                                     context:nil];
        
        CGFloat height = MAX(textRect.size.height, 44.0f);
        return height + (CELL_CONTENT_MARGIN * 2);
    }
    else if(indexPath.row == 0 && indexPath.section == 4){
        
        CGSize constraint = CGSizeMake(cellWidth - (CELL_CONTENT_MARGIN * 2), 20000.0f);
        CGRect textRect = [notes boundingRectWithSize:constraint
                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:FONT_SIZE]}
                                                     context:nil];
        
        CGFloat height = MAX(textRect.size.height, 44.0f);
        return height + (CELL_CONTENT_MARGIN * 2);
    }
	
	return height;
}


// returns the number of sections in table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 5; // the number of sections in the table
}

// returns the number of rows in the given table section
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{		
	NSInteger numberRows = 0;
	
	switch (section) {
		case 0:
			numberRows = 2;
			break;
		case 1:
			numberRows = 1;
			break;
        case 2:
            numberRows = 4;
            break;
        case 3:
            numberRows = 1;
            break;
            
        case 4:
            numberRows = 1;
            break;

		default:
			break;
	}
	
	return numberRows;
} 

// returns the title for the given section
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 3) {
        return @"Instructions";
    }
    else if(section == 4){
        return @"Notes";
    }
	return nil;
}

// returns the cell at the given index path
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{	
	UITableViewCell *returnedCell = nil;
    
    CGFloat cellWidth = [[UIScreen mainScreen] bounds].size.width;
		
	if(indexPath.section == 0){
		
		returnedCell = [self tableView:tableView itemCellForRowAtIndexPath:indexPath];						
	}
	else if(indexPath.section == 1){
		
		returnedCell = [self tableView:tableView sliderCellForRowAtIndexPath:indexPath];		
	}
    else if(indexPath.section == 2)
    {           
		returnedCell = [self tableView:tableView switchCellForIndexPath:indexPath];
	}
    else if(indexPath.section == 3)
    {
        returnedCell = [self longTextTableViewCell:self.tableView];
        returnedCell.userInteractionEnabled = NO;
        
        CGSize constraint = CGSizeMake(cellWidth - (CELL_CONTENT_MARGIN * 2), 20000.0f);
        
        CGRect textRect = [instructions boundingRectWithSize:constraint
                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:FONT_SIZE]}
                                                     context:nil];
        
        UILabel *label = (UILabel*)[returnedCell viewWithTag:1];
        
        [label setText:instructions];
        [label setFrame:CGRectMake(CELL_CONTENT_MARGIN, CELL_CONTENT_MARGIN, cellWidth - (CELL_CONTENT_MARGIN * 2), MAX(textRect.size.height, 44.0f))];
	}
    else if(indexPath.section == 4)
    {
        returnedCell = [self longTextTableViewCell:self.tableView];
        returnedCell.userInteractionEnabled = NO;
        
        CGSize constraint = CGSizeMake(cellWidth - (CELL_CONTENT_MARGIN * 2), 20000.0f);
        
        CGRect textRect = [notes boundingRectWithSize:constraint
                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:FONT_SIZE]}
                                                     context:nil];
        
        UILabel *label = (UILabel*)[returnedCell viewWithTag:1];
        
        [label setText:notes];
        [label setFrame:CGRectMake(CELL_CONTENT_MARGIN, CELL_CONTENT_MARGIN, cellWidth - (CELL_CONTENT_MARGIN * 2), MAX(textRect.size.height, 44.0f))];
	}

	
	return returnedCell;	
}

- (UITableViewCell *)longTextTableViewCell:(UITableView *)tableView
{
	static NSString *CellIdentifier = @"long Text Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	UILabel *label = nil;
	
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
		
		label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
		[label setLineBreakMode:NSLineBreakByWordWrapping];
		[label setMinimumScaleFactor:FONT_SIZE];
		[label setNumberOfLines:0];
		[label setFont:[UIFont systemFontOfSize:FONT_SIZE]];
		[label setTag:1];
		
		[[cell contentView] addSubview:label];
    }
	
	return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView switchCellForIndexPath:(NSIndexPath *)indexPath{
	
	static NSString *idItemCell = @"switchPath";
	
	// get a reusable cell
	UITableViewCell * cell = [table dequeueReusableCellWithIdentifier:idItemCell];
	
	// if no reusable cell exists
	if (cell == nil)
	{
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault	reuseIdentifier:idItemCell] autorelease];
		//cell.selectionStyle = UITableViewCellSelectionStyleNone;
	}
    
   	if (indexPath.section == 2)
    {		
		if (indexPath.row == 0) {
			
			cell.textLabel.text = @"Open Houses Only";
            UISwitch *switchView = [[[UISwitch alloc] init] autorelease];            
            cell.accessoryView = switchView;
            
            [switchView setOn:dataController.openHousesOnly animated:NO];
            [switchView addTarget:self action:@selector(switchForOpenHouse:) forControlEvents:UIControlEventValueChanged];
		}
        if (indexPath.row == 1) {
			
			cell.textLabel.text = @"Short Sale Only";
            UISwitch *switchView = [[[UISwitch alloc] init] autorelease];
            cell.accessoryView = switchView;
            
            [switchView setOn:dataController.shortSaleOnly animated:NO];
            [switchView addTarget:self action:@selector(switchForShortSale:) forControlEvents:UIControlEventValueChanged];
		}
        if (indexPath.row == 2) {
			
			cell.textLabel.text = @"Reo Sale Only";
            UISwitch *switchView = [[[UISwitch alloc] init] autorelease];
            cell.accessoryView = switchView;
            
            [switchView setOn:dataController.reoSaleOnly animated:NO];
            [switchView addTarget:self action:@selector(switchForReoSale:) forControlEvents:UIControlEventValueChanged];
		}
        if (indexPath.row == 3) {
			
			cell.textLabel.text = @"Price Reduced Only";
            UISwitch *switchView = [[[UISwitch alloc] init] autorelease];
            cell.accessoryView = switchView;
            
            [switchView setOn:dataController.priceReducedOnly animated:NO];
            [switchView addTarget:self action:@selector(switchForPriceReduced:) forControlEvents:UIControlEventValueChanged];
		}		
	}
	
    //	cell.labelValue.textColor = [UIColor grayColor];
	
	return cell;
}

- (void) switchForOpenHouse:(id)sender {
    UISwitch* switchControl = sender;
    dataController.openHousesOnly = switchControl.on;
    NSLog( @"The switchForOpenHouse is %@", switchControl.on ? @"ON" : @"OFF" );
}

- (void) switchForShortSale:(id)sender {
    UISwitch* switchControl = sender;
    dataController.shortSaleOnly = switchControl.on;
    NSLog( @"The switchForShortSale is %@", switchControl.on ? @"ON" : @"OFF" );
}

- (void) switchForReoSale:(id)sender {
    UISwitch* switchControl = sender;
    dataController.reoSaleOnly = switchControl.on;
    NSLog( @"The switchForREOSale is %@", switchControl.on ? @"ON" : @"OFF" );
}

- (void) switchForPriceReduced:(id)sender {
    UISwitch* switchControl = sender;
    dataController.priceReducedOnly = switchControl.on;
    NSLog( @"The switchForPriceReduced is %@", switchControl.on ? @"ON" : @"OFF" );
}

- (UITableViewCell *)tableView:(UITableView *)tableView itemCellForRowAtIndexPath:(NSIndexPath *)indexPath{
	
	static NSString *idItemCell = @"ItemCell";
	
	// get a reusable cell
	ItemCell *cell = (ItemCell *)[table dequeueReusableCellWithIdentifier:idItemCell];
	
	// if no reusable cell exists
	if (cell == nil)
	{		
		cell = [[[ItemCell alloc] initWithStyle:UITableViewCellStyleDefault	reuseIdentifier:idItemCell] autorelease];		
		//cell.selectionStyle = UITableViewCellSelectionStyleNone;
	}
    
    UIColor *infoBlue = [UIColor colorWithRed:37.0/255.0 green: 87.0/255.0 blue: 180.0/255.0 alpha: 1.0];
	
	if (indexPath.section == 0) {
		
		if (indexPath.row == 0) {
			
			cell.labelKey.text = @"Property Type";
			cell.labelValue.text = [dataController selectedPropertyTypes];
            cell.labelValue.textColor = infoBlue;
            cell.labelValue.backgroundColor = [UIColor clearColor];
            cell.labelKey.backgroundColor = [UIColor clearColor];
		}
		else if(indexPath.row == 1){
			
			cell.labelKey.text = @"Location";
            cell.labelKey.backgroundColor = [UIColor clearColor];
			cell.labelValue.text = [dataController selectedLocations];
            cell.labelValue.textColor = infoBlue;
            cell.labelValue.backgroundColor = [UIColor clearColor];
		}
	}
	
//	cell.labelValue.textColor = [UIColor grayColor];
	
	return cell; 		
}

- (UITableViewCell *)tableView:(UITableView *)tableView sliderCellForRowAtIndexPath:(NSIndexPath *)indexPath{
	
	static NSString *idSliderCell = @"SliderCell";
	
	
	// get a reusable cell
	SliderRequirementsCell *cell = (SliderRequirementsCell *)[table dequeueReusableCellWithIdentifier:idSliderCell];
	
	// if no reusable cell exists
	if (cell == nil)
	{
		// create a new ItemCell
		cell = [[[SliderRequirementsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idSliderCell] autorelease];
		
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
	}	
	
	cell.priceValue.text = [[DataController pricesArray] objectAtIndex:dataController.price];	
	cell.priceSlider.maximumValue = [[DataController pricesArray] count] - 1;
	cell.priceSlider.value = dataController.price;		
	[cell.priceSlider addTarget:self action:@selector(priceValueChanged:) 
	   forControlEvents:UIControlEventValueChanged];
	
	// beds
	cell.bedsSlider.minimumValue = 0;
	cell.bedsSlider.maximumValue = [[DataController bedsArray] count] - 1;
	cell.bedsSlider.value = dataController.beds;
	cell.bedsValue.text = [[DataController bedsArray] objectAtIndex:dataController.beds];
	[cell.bedsSlider addTarget:self action:@selector(bedsValueChanged:) 
			  forControlEvents:UIControlEventValueChanged];
	
	// baths
	cell.bathsSlider.minimumValue = 0;
	cell.bathsSlider.maximumValue = [[DataController bathsArray] count] - 1;
	cell.bathsSlider.value = dataController.baths;
	cell.bathsValue.text = [[DataController bathsArray] objectAtIndex:dataController.baths];
	[cell.bathsSlider addTarget:self action:@selector(bathsValueChanged:) 
			  forControlEvents:UIControlEventValueChanged];
	
	// structure size
	cell.structureSizeSlider.minimumValue = 0;
	cell.structureSizeSlider.maximumValue = [[DataController structureSizeArray] count] - 1;
	cell.structureSizeSlider.value = dataController.structureSize;
	cell.structureSizeValue.text = [[DataController structureSizeArray] objectAtIndex:dataController.structureSize];
	[cell.structureSizeSlider addTarget:self action:@selector(structureSizeValueChanged:) 
			   forControlEvents:UIControlEventValueChanged];
	
		
    // dom
	cell.domSlider.minimumValue = 0;
	cell.domSlider.maximumValue = [[DataController domArray] count] - 1;
	cell.domSlider.value = dataController.dom;
	cell.domValue.text = [[DataController domArray] objectAtIndex:dataController.dom];
	[cell.domSlider addTarget:self action:@selector(domValueChanged:)
			  forControlEvents:UIControlEventValueChanged];
    
    // cache here so changing slider works
	priceValue = cell.priceValue;
	bedsValue = cell.bedsValue;
	bathsValue = cell.bathsValue;
	structureSizeValue = cell.structureSizeValue;
    domValue = cell.domValue;
		
	return cell; 		
}

-(IBAction)structureSizeValueChanged:(UISlider *)sender
{	
	NSInteger i = (NSInteger) round(sender.value);	
	dataController.structureSize = i;	
	structureSizeValue.text = [[DataController structureSizeArray] objectAtIndex:i];
}

-(IBAction)priceValueChanged:(UISlider *)sender
{	
	NSInteger i = (NSInteger) round(sender.value);	
	dataController.price = i;	
	priceValue.text = [[DataController pricesArray] objectAtIndex:i];
}

-(IBAction)bedsValueChanged:(UISlider *)sender
{	
	NSInteger i = (NSInteger) round(sender.value);	
	dataController.beds = i;	
	bedsValue.text = [[DataController bedsArray] objectAtIndex:i];
}

-(IBAction)domValueChanged:(UISlider *)sender
{
	NSInteger i = (NSInteger) round(sender.value);
	dataController.dom = i;
	domValue.text = [[DataController domArray] objectAtIndex:i];
}


-(IBAction)bathsValueChanged:(UISlider *)sender
{	
	NSInteger i = (NSInteger) round(sender.value);	
	dataController.baths = i;	
	bathsValue.text = [[DataController bathsArray] objectAtIndex:i];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    // Handle the selection
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
	
    return [arrayPrices count];
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	
	return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	
    return [arrayPrices objectAtIndex:row];
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
	int sectionWidth = 300;
	
	return sectionWidth;
}

- (void)dealloc {
    [super dealloc];
}


@end
