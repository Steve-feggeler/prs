//
//  SliderRequirementsCell.h
//  R1
//
//  Created by stephen feggeler on 2/8/13.
//  Copyright 2013 self. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface SliderRequirementsCell : UITableViewCell {
	
	UISlider *priceSlider;
	UILabel  *priceName;
	UILabel  *priceValue;
	
	UISlider *bedsSlider;
	UILabel  *bedsName;
	UILabel  *bedsValue;
	
	UISlider *bathsSlider;
	UILabel  *bathsName;
	UILabel  *bathsValue;
	
	UISlider *structureSizeSlider;
	UILabel  *structureSizeName;
	UILabel  *structureSizeValue;
	
	UISlider *domSlider;
	UILabel  *domName;
	UILabel  *domValue;
	
	UISlider *pageSizeSlider;
	UILabel  *pageName;
	UILabel  *pageValue;		
	
}

@property (nonatomic, retain) UISlider *priceSlider;
@property (nonatomic, retain) UILabel  *priceName;
@property (nonatomic, retain) UILabel  *priceValue;

@property (nonatomic, retain) UISlider *bedsSlider;
@property (nonatomic, retain) UILabel  *bedsName;
@property (nonatomic, retain) UILabel  *bedsValue;

@property (nonatomic, retain) UISlider *bathsSlider;
@property (nonatomic, retain) UILabel  *bathsName;
@property (nonatomic, retain) UILabel  *bathsValue;

@property (nonatomic, retain) UISlider *structureSizeSlider;
@property (nonatomic, retain) UILabel  *structureSizeName;
@property (nonatomic, retain) UILabel  *structureSizeValue;

@property (nonatomic, retain) UISlider *domSlider;
@property (nonatomic, retain) UILabel  *domName;
@property (nonatomic, retain) UILabel  *domValue;

@property (nonatomic, retain) UISlider *pageSizeSlider;
@property (nonatomic, retain) UILabel  *pageName;
@property (nonatomic, retain) UILabel  *pageValue;


+ (CGFloat)height;

@end
